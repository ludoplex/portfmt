PORTNAME=llvm90
.   for option a in CLANG COMPILER_RT EXTRAS LLD LLDB OPENMP
WRKSRC_${option:tl}_$a=	${WRKDIR}/${${option}_DISTFILES}
. endfor
FOO=meh

. include <bsd.port.mk>
<<<<<<<<<
(ast
 (variable
  (modifier #:assign)
  (words
   (word
    (offset 9 15)
    (value "llvm90")))
  (children
   (variable-name
    (words
     (word
      (offset 0 8)
      (value "PORTNAME"))))))
 (for
  (indent "   ")
  (children
   (for-bindings
    (bindings
     (word-whitespace
      (offset 23 24)
      (value " "))
     (word
      (offset 24 30)
      (value "option"))
     (word-whitespace
      (offset 30 31)
      (value " "))
     (word
      (offset 31 32)
      (value "a"))
     (word-whitespace
      (offset 32 33)
      (value " "))))
   (for-words
    (words
     (word-whitespace
      (offset 35 36)
      (value " "))
     (word
      (offset 36 41)
      (value "CLANG"))
     (word-whitespace
      (offset 41 42)
      (value " "))
     (word
      (offset 42 53)
      (value "COMPILER_RT"))
     (word-whitespace
      (offset 53 54)
      (value " "))
     (word
      (offset 54 60)
      (value "EXTRAS"))
     (word-whitespace
      (offset 60 61)
      (value " "))
     (word
      (offset 61 64)
      (value "LLD"))
     (word-whitespace
      (offset 64 65)
      (value " "))
     (word
      (offset 65 69)
      (value "LLDB"))
     (word-whitespace
      (offset 69 70)
      (value " "))
     (word
      (offset 70 76)
      (value "OPENMP"))))
   (for-body
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (offset 100 101)
        (value "\t"))
       (word-append
        (offset 101 133)
        (word-expand
         (offset 101 110)
         (word
          (offset 103 109)
          (value "WRKDIR")))
        (word
         (offset 110 111)
         (value "/"))
        (word-expand
         (offset 111 133)
         (word-expand
          (offset 113 122)
          (word
           (offset 115 121)
           (value "option")))
         (word
          (offset 122 132)
          (value "_DISTFILES")))))
      (children
       (variable-name
        (words
         (word-append
          (offset 77 99)
          (word
           (offset 77 84)
           (value "WRKSRC_"))
          (word-expand
           (offset 84 96)
           (word
            (offset 86 92)
            (value "option"))
           (modifiers
            (modifier
             (type #:tl))))
          (word
           (offset 96 97)
           (value "_"))
          (word-expand
           (offset 97 99)
           (word
            (offset 98 99)
            (value "a"))))))))))
   (for-end
    (indent " "))))
 (variable
  (modifier #:assign)
  (words
   (word
    (offset 147 150)
    (value "meh")))
  (children
   (variable-name
    (words
     (word
      (offset 143 146)
      (value "FOO"))))))
 (comment
  (words
   (word-whitespace
    (offset 151 151)
    (value ""))))
 (include
  (type #:bmake)
  (indent " ")
  (sys? #t)
  (loaded? #f)
  (words
   (word-whitespace
    (offset 161 162)
    (value " "))
   (word
    (offset 162 175)
    (value "<bsd.port.mk>")))))
