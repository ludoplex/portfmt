.if 1 # a
.elif 2 # b
. elif 3
.else # c
.  endif # d
<<<<<<<<<
(ast
 (if
  (end-indent "  ")
  (words
   (word-whitespace
    (offset 49 50)
    (value " "))
   (word-comment
    (offset 51 53)
    (value " d")))
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 3 4)
      (value " "))
     (word
      (offset 4 5)
      (value "1"))
     (word-whitespace
      (offset 5 6)
      (value " "))
     (word-comment
      (offset 7 9)
      (value " a"))))
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 15 16)
      (value " "))
     (word
      (offset 16 17)
      (value "2"))
     (word-whitespace
      (offset 17 18)
      (value " "))
     (word-comment
      (offset 19 21)
      (value " b"))))
   (branch
    (type #:if)
    (indent " ")
    (test
     (word-whitespace
      (offset 28 29)
      (value " "))
     (word
      (offset 29 30)
      (value "3"))))
   (branch
    (type #:else)
    (indent "")
    (test
     (word-whitespace
      (offset 36 37)
      (value " "))
     (word-comment
      (offset 38 40)
      (value " c")))))))
