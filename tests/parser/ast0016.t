do-install: do-install-${D:S/./_/g}
<<<<<<<<<
(ast
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word
      (offset 0 10)
      (value "do-install"))))
   (target-rule-dependencies
    (words
     (word-whitespace
      (offset 11 12)
      (value " "))
     (word-append
      (offset 12 35)
      (word
       (offset 12 23)
       (value "do-install-"))
      (word-expand
       (offset 23 35)
       (word
        (offset 25 26)
        (value "D"))
       (modifiers
        (modifier
         (type #:S)
         (delimiter "/")
         (pattern
          (word
           (offset 29 30)
           (value ".")))
         (replacement
          (word
           (offset 31 32)
           (value "_")))
         (chained? #f)
         (flags "g")))))))
   (target-rule-body))))
