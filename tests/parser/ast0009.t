.if 1
.else
FOO=fo
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 3 4)
      (value " "))
     (word
      (offset 4 5)
      (value "1"))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word
        (offset 16 18)
        (value "fo")))
      (children
       (variable-name
        (words
         (word
          (offset 12 15)
          (value "FOO")))))))))))
