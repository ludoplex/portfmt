do-install-${D:S/./_/g}:
<<<<<<<<<
(ast
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word-append
      (offset 0 23)
      (word
       (offset 0 11)
       (value "do-install-"))
      (word-expand
       (offset 11 23)
       (word
        (offset 13 14)
        (value "D"))
       (modifiers
        (modifier
         (type #:S)
         (delimiter "/")
         (pattern
          (word
           (offset 17 18)
           (value ".")))
         (replacement
          (word
           (offset 19 20)
           (value "_")))
         (chained? #f)
         (flags "g")))))))
   (target-rule-dependencies)
   (target-rule-body))))
