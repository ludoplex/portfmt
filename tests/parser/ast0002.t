.if 1
PORTSCOUT=	foo
.if 4
.endif
.   elifdef 2
PORTSCOUT=	bar
.else
YEP=	contraband
.endif

#foo
foo:: meh meh
	asldfjalsdfj
.ifdef FOO
bar:
	slkdjfalsdkf
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 3 4)
      (value " "))
     (word
      (offset 4 5)
      (value "1")))
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (offset 16 17)
        (value "\t"))
       (word
        (offset 17 20)
        (value "foo")))
      (children
       (variable-name
        (words
         (word
          (offset 6 15)
          (value "PORTSCOUT"))))))
     (if
      (end-indent "")
      (children
       (branch
        (type #:if)
        (indent "")
        (test
         (word-whitespace
          (offset 24 25)
          (value " "))
         (word
          (offset 25 26)
          (value "4"))))))))
   (branch
    (type #:ifdef)
    (indent "   ")
    (test
     (word-whitespace
      (offset 45 46)
      (value " "))
     (word
      (offset 46 47)
      (value "2")))
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (offset 58 59)
        (value "\t"))
       (word
        (offset 59 62)
        (value "bar")))
      (children
       (variable-name
        (words
         (word
          (offset 48 57)
          (value "PORTSCOUT"))))))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (offset 73 74)
        (value "\t"))
       (word
        (offset 74 84)
        (value "contraband")))
      (children
       (variable-name
        (words
         (word
          (offset 69 72)
          (value "YEP"))))))))))
 (comment
  (words
   (word-whitespace
    (offset 92 92)
    (value ""))
   (word
    (offset 93 97)
    (value "#foo"))))
 (target-rule
  (operator "::")
  (children
   (target-rule-targets
    (words
     (word
      (offset 98 101)
      (value "foo"))))
   (target-rule-dependencies
    (words
     (word-whitespace
      (offset 103 104)
      (value " "))
     (word
      (offset 104 107)
      (value "meh"))
     (word-whitespace
      (offset 107 108)
      (value " "))
     (word
      (offset 108 111)
      (value "meh"))))
   (target-rule-body
    (children
     (target-command
      (words
       (word
        (offset 113 125)
        (value "asldfjalsdfj"))))
     (if
      (end-indent "")
      (children
       (branch
        (type #:ifdef)
        (indent "")
        (test
         (word-whitespace
          (offset 132 133)
          (value " "))
         (word
          (offset 133 136)
          (value "FOO")))
        (children
         (target-rule
          (operator ":")
          (children
           (target-rule-targets
            (words
             (word
              (offset 137 140)
              (value "bar"))))
           (target-rule-dependencies)
           (target-rule-body
            (children
             (target-command
              (words
               (word
                (offset 143 155)
                (value "slkdjfalsdkf")))))))))))))))))
