quiet-flags:
	@a
	-b
	+c
	@-d
	@+i
	-@e
	+@f
	-+
	@@
	++i
	--j
	@+-
	@-+g
	-@+
	-+@i
	+-@
	+@-k
<<<<<<<<<
(ast
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word
      (offset 0 11)
      (value "quiet-flags"))))
   (target-rule-dependencies)
   (target-rule-body
    (children
     (target-command
      (words
       (word
        (offset 15 16)
        (value "a")))
      (flags "@"))
     (target-command
      (words
       (word
        (offset 19 20)
        (value "b")))
      (flags "-"))
     (target-command
      (words
       (word
        (offset 23 24)
        (value "c")))
      (flags "+"))
     (target-command
      (words
       (word
        (offset 28 29)
        (value "d")))
      (flags "@-"))
     (target-command
      (words
       (word
        (offset 33 34)
        (value "i")))
      (flags "@+"))
     (target-command
      (words
       (word
        (offset 38 39)
        (value "e")))
      (flags "-@"))
     (target-command
      (words
       (word
        (offset 43 44)
        (value "f")))
      (flags "+@"))
     (target-command
      (flags "-+"))
     (target-command
      (flags "@@"))
     (target-command
      (words
       (word
        (offset 56 57)
        (value "i")))
      (flags "++"))
     (target-command
      (words
       (word
        (offset 61 62)
        (value "j")))
      (flags "--"))
     (target-command
      (flags "@+-"))
     (target-command
      (words
       (word
        (offset 72 73)
        (value "g")))
      (flags "@-+"))
     (target-command
      (flags "-@+"))
     (target-command
      (words
       (word
        (offset 83 84)
        (value "i")))
      (flags "-+@"))
     (target-command
      (flags "+-@"))
     (target-command
      (words
       (word
        (offset 94 95)
        (value "k")))
      (flags "+@-")))))))
