.if 1
fo=
.elifndef 2
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 3 4)
      (value " "))
     (word
      (offset 4 5)
      (value "1")))
    (children
     (variable
      (modifier #:assign)
      (children
       (variable-name
        (words
         (word
          (offset 6 8)
          (value "fo"))))))))
   (branch
    (type #:ifndef)
    (indent "")
    (test
     (word-whitespace
      (offset 19 20)
      (value " "))
     (word
      (offset 20 21)
      (value "2")))))))
