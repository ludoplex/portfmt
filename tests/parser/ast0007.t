# You can force skipping these test by defining IGNORE_PATH_CHECKS
.if !defined(IGNORE_PATH_CHECKS)
.if ! ${PREFIX:M/*}
.BEGIN:
	@${ECHO_MSG} "PREFIX must be defined as an absolute path so that when 'make'"
	@${ECHO_MSG} "is invoked in the work area PREFIX points to the right place."
	@${FALSE}
.endif
.endif

DATADIR?=		${PREFIX}/share/${PORTNAME}
DOCSDIR?=		${PREFIX}/share/doc/${PORTNAME}
ETCDIR?=		${PREFIX}/etc/${PORTNAME}
EXAMPLESDIR?=	${PREFIX}/share/examples/${PORTNAME}
WWWDIR?=		${PREFIX}/www/${PORTNAME}
<<<<<<<<<
(ast
 (comment
  (words
   (word
    (offset 0 66)
    (value "# You can force skipping these test by defining IGNORE_PATH_CHECKS"))))
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 70 71)
      (value " "))
     (word-bool
      (offset 71 72)
      (value "!"))
     (word
      (offset 72 99)
      (value "defined(IGNORE_PATH_CHECKS)")))
    (children
     (if
      (end-indent "")
      (children
       (branch
        (type #:if)
        (indent "")
        (test
         (word-whitespace
          (offset 103 104)
          (value " "))
         (word-bool
          (offset 104 105)
          (value "!"))
         (word-whitespace
          (offset 105 106)
          (value " "))
         (word-expand
          (offset 106 119)
          (word
           (offset 108 114)
           (value "PREFIX"))
          (modifiers
           (modifier
            (type #:M)
            (arg
             (word
              (offset 116 118)
              (value "/*")))))))
        (children
         (target-rule
          (operator ":")
          (children
           (target-rule-targets
            (words
             (word
              (offset 120 126)
              (value ".BEGIN"))))
           (target-rule-dependencies)
           (target-rule-body
            (children
             (target-command
              (words
               (word-expand
                (offset 130 141)
                (word
                 (offset 132 140)
                 (value "ECHO_MSG")))
               (word-whitespace
                (offset 141 142)
                (value " "))
               (word
                (offset 142 206)
                (value "\"PREFIX must be defined as an absolute path so that when 'make'\"")))
              (flags "@"))
             (target-command
              (words
               (word-expand
                (offset 209 220)
                (word
                 (offset 211 219)
                 (value "ECHO_MSG")))
               (word-whitespace
                (offset 220 221)
                (value " "))
               (word
                (offset 221 284)
                (value "\"is invoked in the work area PREFIX points to the right place.\"")))
              (flags "@"))
             (target-command
              (words
               (word-expand
                (offset 287 295)
                (word
                 (offset 289 294)
                 (value "FALSE"))))
              (flags "@"))))))))))))))
 (comment
  (words
   (word-whitespace
    (offset 310 310)
    (value ""))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (offset 320 322)
    (value "\t\t"))
   (word-append
    (offset 322 349)
    (word-expand
     (offset 322 331)
     (word
      (offset 324 330)
      (value "PREFIX")))
    (word
     (offset 331 338)
     (value "/share/"))
    (word-expand
     (offset 338 349)
     (word
      (offset 340 348)
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (offset 311 318)
      (value "DATADIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (offset 359 361)
    (value "\t\t"))
   (word-append
    (offset 361 392)
    (word-expand
     (offset 361 370)
     (word
      (offset 363 369)
      (value "PREFIX")))
    (word
     (offset 370 381)
     (value "/share/doc/"))
    (word-expand
     (offset 381 392)
     (word
      (offset 383 391)
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (offset 350 357)
      (value "DOCSDIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (offset 401 403)
    (value "\t\t"))
   (word-append
    (offset 403 428)
    (word-expand
     (offset 403 412)
     (word
      (offset 405 411)
      (value "PREFIX")))
    (word
     (offset 412 417)
     (value "/etc/"))
    (word-expand
     (offset 417 428)
     (word
      (offset 419 427)
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (offset 393 399)
      (value "ETCDIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (offset 442 443)
    (value "\t"))
   (word-append
    (offset 443 479)
    (word-expand
     (offset 443 452)
     (word
      (offset 445 451)
      (value "PREFIX")))
    (word
     (offset 452 468)
     (value "/share/examples/"))
    (word-expand
     (offset 468 479)
     (word
      (offset 470 478)
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (offset 429 440)
      (value "EXAMPLESDIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (offset 488 490)
    (value "\t\t"))
   (word-append
    (offset 490 515)
    (word-expand
     (offset 490 499)
     (word
      (offset 492 498)
      (value "PREFIX")))
    (word
     (offset 499 504)
     (value "/www/"))
    (word-expand
     (offset 504 515)
     (word
      (offset 506 514)
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (offset 480 486)
      (value "WWWDIR")))))))
