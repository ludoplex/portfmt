.if 1
.for a in 1 2
FOO=bar
.endfor
.for a in 1 2
FOO=bar
.endfor
.for a in 1 2
FOO=bar
.endfor
.else
MUH=muh
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 3 4)
      (value " "))
     (word
      (offset 4 5)
      (value "1")))
    (children
     (for
      (indent "")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (offset 10 11)
          (value " "))
         (word
          (offset 11 12)
          (value "a"))
         (word-whitespace
          (offset 12 13)
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (offset 15 16)
          (value " "))
         (word
          (offset 16 17)
          (value "1"))
         (word-whitespace
          (offset 17 18)
          (value " "))
         (word
          (offset 18 19)
          (value "2"))))
       (for-body
        (children
         (variable
          (modifier #:assign)
          (words
           (word
            (offset 24 27)
            (value "bar")))
          (children
           (variable-name
            (words
             (word
              (offset 20 23)
              (value "FOO"))))))))
       (for-end
        (indent ""))))
     (for
      (indent "")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (offset 40 41)
          (value " "))
         (word
          (offset 41 42)
          (value "a"))
         (word-whitespace
          (offset 42 43)
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (offset 45 46)
          (value " "))
         (word
          (offset 46 47)
          (value "1"))
         (word-whitespace
          (offset 47 48)
          (value " "))
         (word
          (offset 48 49)
          (value "2"))))
       (for-body
        (children
         (variable
          (modifier #:assign)
          (words
           (word
            (offset 54 57)
            (value "bar")))
          (children
           (variable-name
            (words
             (word
              (offset 50 53)
              (value "FOO"))))))))
       (for-end
        (indent ""))))
     (for
      (indent "")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (offset 70 71)
          (value " "))
         (word
          (offset 71 72)
          (value "a"))
         (word-whitespace
          (offset 72 73)
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (offset 75 76)
          (value " "))
         (word
          (offset 76 77)
          (value "1"))
         (word-whitespace
          (offset 77 78)
          (value " "))
         (word
          (offset 78 79)
          (value "2"))))
       (for-body
        (children
         (variable
          (modifier #:assign)
          (words
           (word
            (offset 84 87)
            (value "bar")))
          (children
           (variable-name
            (words
             (word
              (offset 80 83)
              (value "FOO"))))))))
       (for-end
        (indent ""))))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word
        (offset 106 109)
        (value "muh")))
      (children
       (variable-name
        (words
         (word
          (offset 102 105)
          (value "MUH")))))))))))
