A=${${a} b:? :}
.MAKEFLAGS:  WITH=""
RUN_DEPENDS= foo:devel/foo
HELLO:=foo
<<<<<<<<<
(ast
 (variable
  (modifier #:assign)
  (words
   (word-expand
    (offset 2 15)
    (word-expand
     (offset 4 8)
     (word
      (offset 6 7)
      (value "a")))
    (word
     (offset 8 10)
     (value " b"))
    (modifiers
     (modifier
      (type #:?)
      (then
       (word
        (offset 12 13)
        (value " ")))
      (orelse
       (word-append
        (offset 13 13)))))))
  (children
   (variable-name
    (words
     (word
      (offset 0 1)
      (value "A"))))))
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word
      (offset 16 26)
      (value ".MAKEFLAGS"))))
   (target-rule-dependencies
    (words
     (word-whitespace
      (offset 27 29)
      (value "  "))
     (word
      (offset 29 36)
      (value "WITH=\"\""))))
   (target-rule-body
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (offset 49 50)
        (value " "))
       (word
        (offset 50 63)
        (value "foo:devel/foo")))
      (children
       (variable-name
        (words
         (word
          (offset 37 48)
          (value "RUN_DEPENDS"))))))
     (variable
      (modifier #:expand)
      (words
       (word
        (offset 71 74)
        (value "foo")))
      (children
       (variable-name
        (words
         (word
          (offset 64 69)
          (value "HELLO")))))))))))
