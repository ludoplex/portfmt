.if 1
.if 2
foo:
.if 3
	adflasdf
.endif
.endif
bar:
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 3 4)
      (value " "))
     (word
      (offset 4 5)
      (value "1")))
    (children
     (if
      (end-indent "")
      (children
       (branch
        (type #:if)
        (indent "")
        (test
         (word-whitespace
          (offset 9 10)
          (value " "))
         (word
          (offset 10 11)
          (value "2")))
        (children
         (target-rule
          (operator ":")
          (children
           (target-rule-targets
            (words
             (word
              (offset 12 15)
              (value "foo"))))
           (target-rule-dependencies)
           (target-rule-body
            (children
             (if
              (end-indent "")
              (children
               (branch
                (type #:if)
                (indent "")
                (test
                 (word-whitespace
                  (offset 20 21)
                  (value " "))
                 (word
                  (offset 21 22)
                  (value "3")))
                (children
                 (target-command
                  (words
                   (word
                    (offset 24 32)
                    (value "adflasdf"))))))))))))))))
     (target-rule
      (operator ":")
      (children
       (target-rule-targets
        (words
         (word
          (offset 47 50)
          (value "bar"))))
       (target-rule-dependencies)
       (target-rule-body))))))))
