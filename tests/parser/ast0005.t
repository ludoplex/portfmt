.if 0
.    for deptype in ${_OPTIONS_DEPENDS}
.      if defined(${opt}_${deptype}_DEPENDS)
${deptype}_DEPENDS+=	${${opt}_${deptype}_DEPENDS}
.      endif
.endfor
.else
FOO=asd
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (offset 3 4)
      (value " "))
     (word
      (offset 4 5)
      (value "0")))
    (children
     (for
      (indent "    ")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (offset 14 15)
          (value " "))
         (word
          (offset 15 22)
          (value "deptype"))
         (word-whitespace
          (offset 22 23)
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (offset 25 26)
          (value " "))
         (word-expand
          (offset 26 45)
          (word
           (offset 28 44)
           (value "_OPTIONS_DEPENDS")))))
       (for-body
        (children
         (if
          (end-indent "      ")
          (children
           (branch
            (type #:if)
            (indent "      ")
            (test
             (word-whitespace
              (offset 55 56)
              (value " "))
             (word-append
              (offset 56 90)
              (word
               (offset 56 64)
               (value "defined("))
              (word-expand
               (offset 64 70)
               (word
                (offset 66 69)
                (value "opt")))
              (word
               (offset 70 71)
               (value "_"))
              (word-expand
               (offset 71 81)
               (word
                (offset 73 80)
                (value "deptype")))
              (word
               (offset 81 90)
               (value "_DEPENDS)"))))
            (children
             (variable
              (modifier #:append)
              (words
               (word-whitespace
                (offset 111 112)
                (value "\t"))
               (word-expand
                (offset 112 140)
                (word-expand
                 (offset 114 120)
                 (word
                  (offset 116 119)
                  (value "opt")))
                (word
                 (offset 120 121)
                 (value "_"))
                (word-expand
                 (offset 121 131)
                 (word
                  (offset 123 130)
                  (value "deptype")))
                (word
                 (offset 131 139)
                 (value "_DEPENDS"))))
              (children
               (variable-name
                (words
                 (word-append
                  (offset 91 109)
                  (word-expand
                   (offset 91 101)
                   (word
                    (offset 93 100)
                    (value "deptype")))
                  (word
                   (offset 101 109)
                   (value "_DEPENDS")))))))))))))
       (for-end
        (indent ""))))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word
        (offset 172 175)
        (value "asd")))
      (children
       (variable-name
        (words
         (word
          (offset 168 171)
          (value "FOO")))))))))))
