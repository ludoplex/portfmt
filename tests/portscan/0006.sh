# Make sure it does not crash with an empty directory
mkdir -p "${logdir}/ports"
${PORTFMT} scan -p "${logdir}/ports" -l "${logdir}/log" 2>&1 >/dev/null || exit 1
# or a ports directory with nonexistent ports listed in categories
${PORTFMT} scan -p 0005 -l "${logdir}/log" 2>&1 >/dev/null || exit 1
