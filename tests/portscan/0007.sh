${PORTFMT} scan --variable-values -p 0002 -l "${logdir}/log"
[ "$(readlink "${logdir}/log/portscan-latest.log")" != "$(readlink "${logdir}/log/portscan-previous.log")" ]
cat <<EOF | diff -u - "${logdir}/log/portscan-latest.log"
Vv      archivers/arj                            IGNORE_PATCHES                	002_no_remove_static_const.patch
Vv      archivers/arj                            IGNORE_PATCHES                	doc_refer_robert_k_jung.patch
Vv      archivers/arj                            IGNORE_PATCHES                	gnu_build_cross.patch
Vv      archivers/arj                            IGNORE_PATCHES                	gnu_build_fix.patch
Vv      archivers/arj                            IGNORE_PATCHES                	gnu_build_flags.patch
Vv      archivers/arj                            IGNORE_PATCHES                	gnu_build_pie.patch
Vv      archivers/arj                            IGNORE_PATCHES                	gnu_build_strip.patch
Vv      archivers/arj                            IGNORE_PATCHES                	hurd_no_fcntl_getlk.patch
Vv      archivers/arj                            IGNORE_PATCHES                	reproducible_help_archive.patch
Vv      archivers/arj                            PORTNAME                      	arj
Vv      archivers/arj                            PORTVERSION                   	3.10.22
EOF
