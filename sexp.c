// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/color.h>
#include <libias/flow.h>
#include <libias/io.h>
#include <libias/mem.h>
#include <libias/mempool.h>
#include <libias/stack.h>
#include <libias/str.h>

#include "libias/array.h"
#include "sexp.h"

struct SexpWriterParenthesisPair {
	const char *open;
	const char *close;
};

struct SexpWriter {
	struct File *f;

	struct SexpWriterParenthesisPair *parenthesis_pairs;
	size_t parenthesis_pairs_len;
	size_t parenthesis_pairs_pos;
	struct Stack *parenthesis_stack;

	int32_t level;
};

// Constants
static const size_t sexp_writer_indent_step = 1;
static struct SexpWriterParenthesisPair parenthesis_pairs_monochrome[] = {
	{ "(", ")" },
};
static struct SexpWriterParenthesisPair parenthesis_pairs_color[] = {
	{ "(", ")" },
	{ ANSI_COLOR_RED "(" ANSI_COLOR_RESET, ANSI_COLOR_RED ")" ANSI_COLOR_RESET },
	{ ANSI_COLOR_GREEN "(" ANSI_COLOR_RESET, ANSI_COLOR_GREEN ")" ANSI_COLOR_RESET },
	{ ANSI_COLOR_YELLOW "(" ANSI_COLOR_RESET, ANSI_COLOR_YELLOW ")" ANSI_COLOR_RESET },
	{ ANSI_COLOR_BLUE "(" ANSI_COLOR_RESET, ANSI_COLOR_BLUE ")" ANSI_COLOR_RESET },
	{ ANSI_COLOR_MAGENTA "(" ANSI_COLOR_RESET, ANSI_COLOR_MAGENTA ")" ANSI_COLOR_RESET },
	{ ANSI_COLOR_CYAN "(" ANSI_COLOR_RESET, ANSI_COLOR_CYAN ")" ANSI_COLOR_RESET },
};

struct SexpWriter *
sexp_writer_new(struct Mempool *pool, bool rainbow)
{
	struct SexpWriter *w = xmalloc(sizeof(struct SexpWriter));

	if (rainbow) {
		w->parenthesis_pairs = parenthesis_pairs_color;
		w->parenthesis_pairs_len = nitems(parenthesis_pairs_color);
	} else {
		w->parenthesis_pairs = parenthesis_pairs_monochrome;
		w->parenthesis_pairs_len = nitems(parenthesis_pairs_monochrome);
	}
	w->parenthesis_stack = stack_new();

	w->f = file_open_memstream(NULL);
	panic_unless(w->f, "file_open_memstream");
	return mempool_add(pool, w, sexp_writer_free);
}

void
sexp_writer_free(struct SexpWriter *w)
{
	if (w) {
		stack_free(w->parenthesis_stack);
		file_free(w->f);
		free(w);
	}
}

void
sexp_writer_open_tree(struct SexpWriter *w, const char *name)
{
	SCOPE_MEMPOOL(pool);
	if (w->level > 0) {
		file_puts(w->f, "\n");
	}
	size_t indent = w->level * sexp_writer_indent_step;
	char *buf = str_repeat(pool, " ", indent);
	file_puts(w->f, buf);

	const struct SexpWriterParenthesisPair pair = w->parenthesis_pairs[w->parenthesis_pairs_pos++ % w->parenthesis_pairs_len];
	file_puts(w->f, pair.open);
	stack_push(w->parenthesis_stack, pair.close);

	file_puts(w->f, name);
	w->level++;
}

void
sexp_writer_close_tree(struct SexpWriter *w)
{
	panic_unless(stack_peek(w->parenthesis_stack), "no open parentheses");
	const char *s = stack_pop(w->parenthesis_stack);
	file_puts(w->f, s);
	w->level--;
}

void
sexp_writer_bool(struct SexpWriter *w, const char *name, bool value)
{
	const char *s = "#f";
	if (value) {
		s = "#t";
	}
	sexp_writer_open_tree(w, name);
	file_printf(w->f, " %s", s);
	sexp_writer_close_tree(w);
}

void
sexp_writer_int64_array(struct SexpWriter *w, const char *name, int64_t values[], size_t len)
{
	if (len > 0) {
		sexp_writer_open_tree(w, name);
		for (size_t i = 0; i < len; i++) {
			file_printf(w->f, " %ld", values[i]);
		}
		sexp_writer_close_tree(w);
	}
}

void
sexp_writer_keyword(struct SexpWriter *w, const char *name, const char *value)
{
	if (value) {
		sexp_writer_open_tree(w, name);
		file_printf(w->f, " #:%s", value);
		sexp_writer_close_tree(w);
	}
}

void
sexp_writer_keywords(struct SexpWriter *w, const char *name, struct Array *values)
{
	if (array_len(values) > 0) {
		sexp_writer_open_tree(w, name);
		ARRAY_FOREACH(values, const char *, value) {
			// TODO: Check if valid symbol!!! This goes for all key names!!!
			file_printf(w->f, " #:%s", value);
		}
		sexp_writer_close_tree(w);
	}
}

void
sexp_writer_string(struct SexpWriter *w, const char *name, const char *value)
{
	SCOPE_MEMPOOL(pool);

	unless (value) {
		return;
	}

	sexp_writer_open_tree(w, name);

	struct File *buf = file_open_memstream(pool);
	file_puts(buf, " \"");
	size_t valuelen = strlen(value);
	for (size_t i = 0; i < valuelen; i++) {
		switch (value[i]) {
		case '\\':
			file_puts(buf, "\\\\");
			break;
		case '"':
			file_puts(buf, "\\\"");
			break;
		case '\n':
			file_puts(buf, "\\n");
			break;
		case '\t':
			file_puts(buf, "\\t");
			break;
		default:
			file_puts(buf, (char[]){ value[i], 0 });
			break;
		}
	}
	file_puts(buf, "\"");
	file_puts(w->f, file_slurp(buf, pool, NULL));

	sexp_writer_close_tree(w);
}

void
sexp_writer_symbol(struct SexpWriter *w, const char *name, const char *value)
{
	if (value) {
		// TODO: Check if valid symbol!!! This goes for all key names!!!
		sexp_writer_open_tree(w, name);
		file_printf(w->f, " %s", value);
		sexp_writer_close_tree(w);
	}
}

char *
sexp_writer_finish(struct SexpWriter *w, struct Mempool *extpool)
{
	const char *p;
	while ((p = stack_pop(w->parenthesis_stack))) {
		file_puts(w->f, p);
	}
	w->level = 0;
	file_puts(w->f, "\n");

	return file_slurp(w->f, extpool, NULL);
}
