// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/io.h>
#include <libias/list.h>
#include <libias/map.h>
#include <libias/mem.h>
#include <libias/mempool.h>
#include <libias/queue.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/compare.h>
#include <libias/util.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/word.h"
#include "rules.h"
#include "sexp.h"

enum ASTVisitState {
	AST_VISIT_STATE_CONTINUE,
	AST_VISIT_STATE_ABORT,
	AST_VISIT_STATE_STOP,
};

struct ASTEdit {
	enum ASTEditFlags flags;
	struct AST *root;
	struct Queue *queue;
	struct Mempool *pool;
};

struct ASTWordsEdit {
	enum ASTWordsEditFlags flags;
	struct AST *node;
	struct Queue *queue;
	struct Mempool *pool;
};

struct ASTChildrenIterator {
	struct AST *node;
	struct ListEntry *entry;
	size_t i;
	size_t len;
};

struct ASTWordsIterator {
	struct AST *node;
	struct ListEntry *entry;
	size_t i;
	size_t len;
};

struct ASTSortWordsData {
	struct CompareTrait *compare;
	struct ASTWord **words;
};

struct ASTVisit {
	struct ASTVisitTrait trait;
	void *context;
	struct {
		struct Set *pre;
		struct Set *post;
	} visited_nodes;
	enum ASTVisitState state;
	struct AST *root;
	struct ASTEdit *root_ast_edit;

	struct {
		struct Queue *queue;
		struct ASTFormatSettings *settings;
		struct Rules *rules;
	} format;
};

struct AST {
	const enum ASTType type;

	struct AST *parent;
	struct {
		struct List *list; // [struct AST *]
		struct Map *child_to_list_entry_map; // Map<AST,ListEntry>
	} children;

	// We uphold the invariant that the word list has only one
	// AST_WORD_WHITESPACE (_) in between the other word types (W) like
	// _W_W_W_W_W_W_, _W_W_W_W, or W_W_W_W_W. We want to avoid something
	// like WWWWWW, W___W, or __W__. This means that when we add new
	// words we make sure there is a " " after it unless it's the last
	// word, or that we merge __ to a single _.
	struct {
		struct List *list; // [struct ASTWord *]
		struct Map *word_to_list_entry_map; // Map<ASTWord,ListEntry>
	} words;

	char *indent;

	struct AST *(*new)(void);
	void (*free)(struct AST *);
	void (*clone)(struct AST *, struct AST *);
	void (*render)(struct AST *, struct File *);
	void (*sexp)(struct AST *, struct SexpWriter *);
	void (*words_validate)(struct AST *);

	struct {
		enum ASTExprType type;
	} expr;

	struct {
		enum ASTIfType type;
	} if_branch;

	struct {
		enum ASTIncludeType type;
		bool loaded;
	} include;

	struct {
		char *flags;
	} target_command;

	struct {
		enum ASTTargetRuleOperator operator;
	} target_rule;

	struct {
		enum ASTVariableModifier modifier;
	} variable;
};

#define MAKE_AST_NODE(type, fun) \
	struct AST *this = xmalloc(sizeof(struct AST)); \
	this->indent = str_dup(NULL, ""); \
	this->clone = ast_clone_##fun; \
	this->new = ast_new_##fun; \
	this->free = ast_free_##fun; \
	this->render = ast_render_##fun; \
	this->sexp = ast_sexp_##fun; \
	this->words_validate = ast_words_validate_##fun; \
	this->parent = this; \
	SET_AST_TYPE(this, type); \
	ast_word_list_init(this);

#define SET_AST_TYPE(this, ast_type) \
	do { \
		enum ASTType type = (ast_type); \
		memcpy((void *)&(this)->type, &type, sizeof(enum ASTType)); \
	} while (false);

#define AST_CHILD_LIST_FOREACH(node, child) \
	LIST_FOREACH((node)->children.list, struct AST *, child)

#define AST_WORD_LIST_FOREACH(node, word) \
	LIST_FOREACH((node)->words.list, struct ASTWord *, word)

// Prototypes
static struct ListEntry *ast_child_list_append(struct AST *node, struct AST *new_child);
static struct ListEntry *ast_child_list_entry_for_node(struct AST *node, struct AST *child);
static struct ListEntry *ast_child_list_entry_remove(struct AST *node, struct ListEntry *entry);
static void ast_child_list_init(struct AST *this);
static struct ListEntry *ast_child_list_insert_after(struct AST *node, struct ListEntry *list_entry, struct AST *new_child);
static struct ListEntry *ast_child_list_insert_before(struct AST *node, struct ListEntry *list_entry, struct AST *new_child);
static void ast_clone_children(struct AST *node, struct AST *template);
static void ast_clone_comment(struct AST *node, struct AST *template);
static void ast_clone_expr(struct AST *node, struct AST *template);
static void ast_clone_for(struct AST *node, struct AST *template);
static void ast_clone_for_bindings(struct AST *node, struct AST *template);
static void ast_clone_for_body(struct AST *node, struct AST *template);
static void ast_clone_for_end(struct AST *node, struct AST *template);
static void ast_clone_for_words(struct AST *node, struct AST *template);
static struct AST *ast_clone_helper(struct AST *template, struct AST *parent);
static void ast_clone_if(struct AST *node, struct AST *template);
static void ast_clone_if_branch(struct AST *node, struct AST *template);
static void ast_clone_include(struct AST *node, struct AST *template);
static void ast_clone_root(struct AST *node, struct AST *template);
static void ast_clone_target_command(struct AST *node, struct AST *template);
static void ast_clone_target_rule(struct AST *node, struct AST *template);
static void ast_clone_target_rule_body(struct AST *node, struct AST *template);
static void ast_clone_target_rule_dependencies(struct AST *node, struct AST *template);
static void ast_clone_target_rule_targets(struct AST *node, struct AST *template);
static void ast_clone_variable(struct AST *node, struct AST *template);
static void ast_clone_variable_name(struct AST *node, struct AST *template);
static void ast_clone_words(struct AST *node, struct AST *template);
static void ast_edit_add_child_before_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_add_child_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_append_child_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_apply_words_edit_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_chain_apply_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_remove_all_children_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_remove_child_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_replace_child_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_set_target_rule_operator_impl(struct ASTEdit *edit, struct Queue *args);
static void ast_edit_set_variable_modifier_impl(struct ASTEdit *edit, struct Queue *args);
static struct ListEntry *ast_find_child(struct AST *root, struct AST *child, struct AST **parent);
static void ast_free_comment(struct AST *node);
static void ast_free_expr(struct AST *node);
static void ast_free_for(struct AST *node);
static void ast_free_for_bindings(struct AST *node);
static void ast_free_for_body(struct AST *node);
static void ast_free_for_end(struct AST *node);
static void ast_free_for_words(struct AST *node);
static void ast_free_if(struct AST *node);
static void ast_free_if_branch(struct AST *node);
static void ast_free_include(struct AST *node);
static void ast_free_root(struct AST *node);
static void ast_free_target_command(struct AST *node);
static void ast_free_target_rule(struct AST *node);
static void ast_free_target_rule_body(struct AST *node);
static void ast_free_target_rule_dependencies(struct AST *node);
static void ast_free_target_rule_targets(struct AST *node);
static void ast_free_variable(struct AST *node);
static void ast_free_variable_name(struct AST *node);
static struct AST *ast_new_for_bindings(void);
static struct AST *ast_new_for_body(void);
static struct AST *ast_new_for_end(void);
static struct AST *ast_new_for_words(void);
static struct AST *ast_new_target_rule_body(void);
static struct AST *ast_new_target_rule_dependencies(void);
static struct AST *ast_new_target_rule_targets(void);
static struct AST *ast_new_variable_name(void);
static void ast_remove_from_parent(struct AST *node);
static void ast_render_comment(struct AST *node, struct File *f);
static void ast_render_expr(struct AST *node, struct File *f);
static void ast_render_for(struct AST *node, struct File *f);
static void ast_render_for_bindings(struct AST *node, struct File *f);
static void ast_render_for_body(struct AST *node, struct File *f);
static void ast_render_for_end(struct AST *node, struct File *f);
static void ast_render_for_words(struct AST *node, struct File *f);
static void ast_render_if(struct AST *node, struct File *f);
static void ast_render_if_branch(struct AST *node, struct File *f);
static void ast_render_include(struct AST *node, struct File *f);
static void ast_render_root(struct AST *node, struct File *f);
static void ast_render_target_command(struct AST *node, struct File *f);
static void ast_render_target_rule(struct AST *node, struct File *f);
static void ast_render_target_rule_body(struct AST *node, struct File *f);
static void ast_render_target_rule_dependencies(struct AST *node, struct File *f);
static void ast_render_target_rule_targets(struct AST *node, struct File *f);
static void ast_render_variable(struct AST *node, struct File *f);
static void ast_render_variable_name(struct AST *node, struct File *f);
static void ast_sexp_comment(struct AST *node, struct SexpWriter *w);
static void ast_sexp_expr(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_bindings(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_body(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_end(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_words(struct AST *node, struct SexpWriter *w);
static void ast_sexp_helper(struct AST *node, struct SexpWriter *w);
static void ast_sexp_if(struct AST *node, struct SexpWriter *w);
static void ast_sexp_if_branch(struct AST *node, struct SexpWriter *w);
static void ast_sexp_include(struct AST *node, struct SexpWriter *w);
static void ast_sexp_root(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_command(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule_body(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule_dependencies(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule_targets(struct AST *node, struct SexpWriter *w);
static void ast_sexp_variable(struct AST *node, struct SexpWriter *w);
static void ast_sexp_variable_name(struct AST *node, struct SexpWriter *w);
static void ast_sexp_words(struct AST *node, const char *name, struct SexpWriter *w);
static void ast_visit_dispatch(struct ASTVisit *visit, struct AST *node);
static void ast_visit_done(struct ASTVisit *visit);
static void ast_visit_helper(struct ASTVisit *visit, struct AST *node);
static void ast_visit_pre_dispatch(struct ASTVisit *visit, struct AST *node);
static void ast_visit_reinit(struct ASTVisit *visit);
static ASTVisitFn ast_visit_trait_get_pre_visit_fn(struct ASTVisitTrait *trait, enum ASTType type);
static ASTVisitFn ast_visit_trait_get_visit_fn(struct ASTVisitTrait *trait, enum ASTType type);
static struct ListEntry *ast_word_list_append(struct AST *node, struct ASTWord *word);
static struct ListEntry *ast_word_list_entry_for_word(struct AST *node, struct ASTWord *word);
static struct ListEntry *ast_word_list_entry_remove(struct AST *node, struct ListEntry *list_entry);
static void ast_word_list_init(struct AST *this);
static struct ListEntry *ast_word_list_insert_after(struct AST *node, struct ListEntry *head, struct ASTWord *word, bool replace);
static struct ListEntry *ast_word_list_insert_before(struct AST *node, struct ListEntry *head, struct ASTWord *word, bool replace);
static void ast_words_edit_add_word_before_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_add_word_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_append_word_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_remove_all_words_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_remove_beginning_whitespace_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_remove_trailing_whitespace_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_remove_whitespace_after_word_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_remove_whitespace_before_word_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_remove_word_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_set_whitespace_after_word_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_edit_set_whitespace_before_word_impl(struct ASTWordsEdit *edit, struct Queue *args);
static void ast_words_validate(struct AST *node);
static void ast_words_validate_comment(struct AST *node);
static void ast_words_validate_expr(struct AST *node);
static void ast_words_validate_for(struct AST *node);
static void ast_words_validate_for_bindings(struct AST *node);
static void ast_words_validate_for_body(struct AST *node);
static void ast_words_validate_for_end(struct AST *node);
static void ast_words_validate_for_words(struct AST *node);
static void ast_words_validate_if(struct AST *node);
static void ast_words_validate_if_branch(struct AST *node);
static void ast_words_validate_include(struct AST *node);
static void ast_words_validate_root(struct AST *node);
static void ast_words_validate_target_command(struct AST *node);
static void ast_words_validate_target_rule(struct AST *node);
static void ast_words_validate_target_rule_body(struct AST *node);
static void ast_words_validate_target_rule_dependencies(struct AST *node);
static void ast_words_validate_target_rule_targets(struct AST *node);
static void ast_words_validate_variable(struct AST *node);
static void ast_words_validate_variable_name(struct AST *node);
static void ast_words_validate_whitespace(struct AST *node);
static int compare_ast_sort_words(const void *ap, const void *bp, void *userdata);

void
ast_child_list_init(struct AST *this)
{
	this->children.list = list_new();
	this->children.child_to_list_entry_map = map_new(id_compare);
}

void
ast_word_list_init(struct AST *this)
{
	this->words.list = list_new();
	this->words.word_to_list_entry_map = map_new(id_compare);
}

struct AST *
ast_new_comment(void)
{
	MAKE_AST_NODE(AST_COMMENT, comment);

	return this;
}

void
ast_free_comment(struct AST *node)
{
}

struct AST *
ast_new_expr(void)
{
	MAKE_AST_NODE(AST_EXPR, expr);

	this->expr.type = AST_EXPR_ERROR;

	return this;
}

void
ast_free_expr(struct AST *node)
{
}

struct AST *
ast_new_for(void)
{
	MAKE_AST_NODE(AST_FOR, for);

	ast_child_list_init(this);

	struct AST *bindings = ast_new_for_bindings();
	bindings->parent = this;
	ast_child_list_append(this, bindings);

	struct AST *words = ast_new_for_words();
	words->parent = this;
	ast_child_list_append(this, words);

	struct AST *body = ast_new_for_body();
	body->parent = this;
	ast_child_list_append(this, body);

	struct AST *end = ast_new_for_end();
	end->parent = this;
	ast_child_list_append(this, end);

	return this;
}

void
ast_free_for(struct AST *node)
{
}

struct AST *
ast_new_for_bindings(void)
{
	MAKE_AST_NODE(AST_FOR_BINDINGS, for_bindings);

	return this;
}

void
ast_free_for_bindings(struct AST *node)
{
}

struct AST *
ast_new_for_body(void)
{
	MAKE_AST_NODE(AST_FOR_BODY, for_body);

	ast_child_list_init(this);

	return this;
}

void
ast_free_for_body(struct AST *node)
{
}

struct AST *
ast_new_for_end(void)
{
	MAKE_AST_NODE(AST_FOR_END, for_end);

	return this;
}

void
ast_free_for_end(struct AST *node)
{
}

struct AST *
ast_new_for_words(void)
{
	MAKE_AST_NODE(AST_FOR_WORDS, for_words);

	return this;
}

void
ast_free_for_words(struct AST *node)
{
}

struct AST *
ast_new_if(void)
{
	MAKE_AST_NODE(AST_IF, if);

	ast_child_list_init(this);

	return this;
}

void
ast_free_if(struct AST *node)
{
}

struct AST *
ast_new_if_branch(void)
{
	MAKE_AST_NODE(AST_IF_BRANCH, if_branch);

	this->if_branch.type = AST_IF_IF;
	ast_child_list_init(this);

	return this;
}

void
ast_free_if_branch(struct AST *node)
{
}

struct AST *
ast_new_include(void)
{
	MAKE_AST_NODE(AST_INCLUDE, include);

	this->include.type = AST_INCLUDE_BMAKE;
	ast_child_list_init(this);

	return this;
}

void
ast_free_include(struct AST *node)
{
}

struct AST *
ast_new_root(void)
{
	MAKE_AST_NODE(AST_ROOT, root);

	ast_child_list_init(this);

	return this;
}

void
ast_free_root(struct AST *node)
{
}

struct AST *
ast_new_target_command(void)
{
	MAKE_AST_NODE(AST_TARGET_COMMAND, target_command);

	this->target_command.flags = str_dup(NULL, "");

	return this;
}

void
ast_free_target_command(struct AST *node)
{
	free(node->target_command.flags);
}

struct AST *
ast_new_target_rule(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE, target_rule);

	this->target_rule.operator = AST_TARGET_RULE_OPERATOR_1;
	ast_child_list_init(this);

	struct AST *targets = ast_new_target_rule_targets();
	targets->parent = this;
	ast_child_list_append(this, targets);

	struct AST *dependencies = ast_new_target_rule_dependencies();
	dependencies->parent = this;
	ast_child_list_append(this, dependencies);

	struct AST *body = ast_new_target_rule_body();
	body->parent = this;
	ast_child_list_append(this, body);

	return this;
}

void
ast_free_target_rule(struct AST *node)
{
}

struct AST *
ast_new_target_rule_targets(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE_TARGETS, target_rule_targets);

	return this;
}

void
ast_free_target_rule_targets(struct AST *node)
{
}

struct AST *
ast_new_target_rule_dependencies(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE_DEPENDENCIES, target_rule_dependencies);

	return this;
}

void
ast_free_target_rule_dependencies(struct AST *node)
{
}

struct AST *
ast_new_target_rule_body(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE_BODY, target_rule_body);

	ast_child_list_init(this);

	return this;
}

void
ast_free_target_rule_body(struct AST *node)
{
}

struct AST *
ast_new_variable(void)
{
	MAKE_AST_NODE(AST_VARIABLE, variable);

	this->variable.modifier = AST_VARIABLE_MODIFIER_ASSIGN;
	ast_child_list_init(this);

	struct AST *name = ast_new_variable_name();
	name->parent = this;
	ast_child_list_append(this, name);

	return this;
}

void
ast_free_variable(struct AST *node)
{
}

struct AST *
ast_new_variable_name(void)
{
	MAKE_AST_NODE(AST_VARIABLE_NAME, variable_name);

	return this;
}

void
ast_free_variable_name(struct AST *node)
{
}

void
ast_clone_comment(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_expr(struct AST *node, struct AST *template)
{
	ast_set_expr_type(node, ast_expr_type(template));
	ast_clone_words(node, template);
}

void
ast_clone_for(struct AST *node, struct AST *template)
{
	SCOPE_MEMPOOL(pool);
	AST_CHILD_LIST_FOREACH(node, child) {
		mempool_add(pool, child, ast_free);
	}
	ast_clone_children(node, template);
}

void
ast_clone_for_bindings(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_for_body(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
}

void
ast_clone_for_words(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_for_end(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_if(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_if_branch(struct AST *node, struct AST *template)
{
	ast_set_if_branch_type(node, ast_if_branch_type(template));
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_include(struct AST *node, struct AST *template)
{
	node->include.loaded = ast_include_loaded_p(template);
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_root(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
}

void
ast_clone_target_command(struct AST *node, struct AST *template)
{
	ast_set_target_command_flags(node, ast_target_command_flags(template));
	ast_clone_words(node, template);
}

void
ast_clone_target_rule(struct AST *node, struct AST *template)
{
	SCOPE_MEMPOOL(pool);

	node->target_rule.operator = template->target_rule.operator;

	AST_CHILD_LIST_FOREACH(node, child) {
		mempool_add(pool, child, ast_free);
	}
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_target_rule_targets(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_target_rule_dependencies(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_target_rule_body(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
}

void
ast_clone_variable(struct AST *node, struct AST *template)
{
	SCOPE_MEMPOOL(pool);

	node->variable.modifier = template->variable.modifier;
	ast_clone_words(node, template);

	AST_CHILD_LIST_FOREACH(node, child) {
		mempool_add(pool, child, ast_free);
	}
	ast_clone_children(node, template);
}

void
ast_clone_variable_name(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_children(struct AST *node, struct AST *template)
{
	AST_CHILD_LIST_FOREACH(template, child) {
		struct AST *clone = ast_clone_helper(child, node);
		ast_child_list_append(node, clone);
	}
}

void
ast_clone_words(struct AST *node, struct AST *template)
{
	AST_WORD_LIST_FOREACH(template, template_word) {
		ast_word_list_append(node, ast_word_clone(template_word));
	}
}

struct AST *
ast_clone_helper(struct AST *template, struct AST *parent)
{
	struct AST *node = template->new();
	node->parent = parent;

	ast_set_expr_indent(node, ast_expr_indent(template));

	node->clone(node, template);

	return node;
}

struct AST *
ast_clone(struct AST *template)
{
	struct AST *node = ast_clone_helper(template, NULL);
	node->parent = node;
	return node;
}

void
ast_render_comment(struct AST *node, struct File *f)
{
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
		file_puts(f, "\n");
	}
}

void
ast_render_expr(struct AST *node, struct File *f)
{
	const char *name = ASTExprType_identifier(ast_expr_type(node));
	file_printf(
		f,
		".%s%s",
		ast_expr_indent(node),
		name + 1);
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
	file_puts(f, "\n");
}

void
ast_render_for(struct AST *node, struct File *f)
{
	file_puts(f, ".");
	file_puts(f, ast_expr_indent(node));
	file_puts(f, "for");

	AST_CHILD_LIST_FOREACH(node, child) {
		child->render(child, f);
	}
}

void
ast_render_for_bindings(struct AST *node, struct File *f)
{
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}

	file_puts(f, "in");
}

void
ast_render_for_words(struct AST *node, struct File *f)
{
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}

	file_puts(f, "\n");
}

void
ast_render_for_body(struct AST *node, struct File *f)
{
	AST_CHILD_LIST_FOREACH(node, child) {
		child->render(child, f);
	}
}

void
ast_render_for_end(struct AST *node, struct File *f)
{
	file_puts(f, ".");
	file_puts(f, ast_expr_indent(node));
	file_puts(f, "endfor");

	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
	file_puts(f, "\n");
}

void
ast_render_if(struct AST *node, struct File *f)
{
	AST_CHILD_LIST_FOREACH(node, child) {
		child->render(child, f);
	}

	file_puts(f, ".");
	file_puts(f, ast_expr_indent(node));
	file_puts(f, "endif");

	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
	file_puts(f, "\n");
}

void
ast_render_if_branch(struct AST *node, struct File *f)
{
	file_puts(f, ".");
	file_puts(f, ast_expr_indent(node));
	unless (
		ast_if_branch_type(node) == AST_IF_ELSE
		|| ast_get_child(ast_parent(node), 0) == node)
	{
		file_puts(f, "el");
	}
	file_puts(f, ASTIfType_human(ast_if_branch_type(node)));

	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}

	file_puts(f, "\n");

	AST_CHILD_LIST_FOREACH(node, child) {
		child->render(child, f);
	}
}

void
ast_render_include(struct AST *node, struct File *f)
{
	const char *name = ASTIncludeType_identifier(ast_include_type(node));
	if (*name == '.') {
		file_printf(f, ".%s%s", ast_expr_indent(node), name + 1);
	} else {
		file_puts(f, name);
	}
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
	file_puts(f, "\n");
}

void
ast_render_root(struct AST *node, struct File *f)
{
	AST_CHILD_LIST_FOREACH(node, child) {
		child->render(child, f);
	}
}

void
ast_render_target_command(struct AST *node, struct File *f)
{
	file_puts(f, "\t");
	file_puts(f, ast_target_command_flags(node));
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
	file_puts(f, "\n");
}

void
ast_render_target_rule(struct AST *node, struct File *f)
{
	AST_CHILD_LIST_FOREACH(node, child) {
		if (ast_target_rule_body_p(child)) {
			AST_WORD_LIST_FOREACH(node, word) {
				ast_word_render(word, f);
			}
			file_puts(f, "\n");
		}
		child->render(child, f);
	}
}

void
ast_render_target_rule_targets(struct AST *node, struct File *f)
{
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
	file_puts(f, ASTTargetRuleOperator_bmake(ast_target_rule_operator(node)));
}

void
ast_render_target_rule_dependencies(struct AST *node, struct File *f)
{
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
}

void
ast_render_target_rule_body(struct AST *node, struct File *f)
{
	AST_CHILD_LIST_FOREACH(node, child) {
		child->render(child, f);
	}
}

void
ast_render_variable(struct AST *node, struct File *f)
{
	AST_CHILD_LIST_FOREACH(node, child) {
		child->render(child, f);
	}

	file_puts(f, ASTVariableModifier_human(ast_variable_modifier(node)));

	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}

	file_puts(f, "\n");
}

void
ast_render_variable_name(struct AST *node, struct File *f)
{
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
}

void
ast_render(
	struct AST *ast,
	struct File *f)
{
	ast->render(ast, f);
}

void
ast_sexp_comment(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_expr(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "type", ASTExprType_sexp(ast_expr_type(node)));
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_for(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "indent", ast_expr_indent(node));
}

void
ast_sexp_for_bindings(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "bindings", w);
}

void
ast_sexp_for_words(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_for_body(struct AST *node, struct SexpWriter *w)
{
}

void
ast_sexp_for_end(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_if(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "end-indent", ast_expr_indent(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_if_branch(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "type", ASTIfType_human(ast_if_branch_type(node)));
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	ast_sexp_words(node, "test", w);
}

void
ast_sexp_include(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "type", ASTIncludeType_sexp(ast_include_type(node)));
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	sexp_writer_bool(w, "sys?", ast_include_system_p(node));
	sexp_writer_bool(w, "loaded?", ast_include_loaded_p(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_root(struct AST *node, struct SexpWriter *w)
{
}

void
ast_sexp_target_command(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
	const char *flags = ast_target_command_flags(node);
	if (*flags) {
		sexp_writer_string(w, "flags", flags);
	}
}

void
ast_sexp_target_rule(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "operator", ASTTargetRuleOperator_bmake(ast_target_rule_operator(node)));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_target_rule_targets(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_target_rule_dependencies(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_target_rule_body(struct AST *node, struct SexpWriter *w)
{
}

void
ast_sexp_variable(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "modifier", ASTVariableModifier_sexp(ast_variable_modifier(node)));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_variable_name(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_helper(
	struct AST *node,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, ASTType_sexp(ast_type(node)));
	node->sexp(node, w);
	if (node->children.list && list_len(node->children.list) > 0) {
		unless (ast_root_p(node)) {
			sexp_writer_open_tree(w, "children");
		}
		AST_CHILD_LIST_FOREACH(node, child) {
			ast_sexp_helper(child, w);
		}
		unless (ast_root_p(node)) {
			sexp_writer_close_tree(w);
		}
	}
	sexp_writer_close_tree(w);
}

void
ast_sexp(
	struct AST *node,
	struct File *f,
	bool color)
{
	SCOPE_MEMPOOL(pool);
	struct SexpWriter *w = sexp_writer_new(pool, color);
	ast_sexp_helper(node, w);
	file_puts(f, sexp_writer_finish(w, pool));
}

void
ast_words_validate_whitespace(struct AST *node)
{
	bool last_was_whitespace = false;
	AST_WORD_LIST_FOREACH(node, word) {
		if (last_was_whitespace) {
			if (ast_word_whitespace_p(word)) {
				// This is a bug because ast_word_list_insert_* should
				// have merged them.
				panic("word list with >2 consecutive AST_WORD_WHITESPACE");
			}
			last_was_whitespace = false;
		} else {
			last_was_whitespace = ast_word_whitespace_p(word);
		}
	}
}

void
ast_words_validate_root(struct AST *node)
{
	panic_unless(
		list_len(node->words.list) == 0,
		"AST_ROOT should not have any words");
}

void
ast_words_validate_comment(struct AST *node)
{
	AST_WORD_LIST_FOREACH(node, word) {
		if (ast_word_string_p(word)) {
			// ok
		} else if (ast_word_whitespace_p(word)) {
			// ok
		} else {
			panic("AST_COMMENT with non-static word");
		}
	}
}

void
ast_words_validate_expr(struct AST *node)
{
	ast_words_validate_whitespace(node);
	// TODO
}

void
ast_words_validate_if(struct AST *node)
{
	AST_WORD_LIST_FOREACH(node, word) {
		if (ast_word_comment_p(word)) {
			// ok
		} else if (ast_word_whitespace_p(word)) {
			// ok
		} else {
			panic("AST_IF (.endif) has non-static words");
		}
	}
}

void
ast_words_validate_if_branch(struct AST *node)
{
	ast_words_validate_whitespace(node);
}

void
ast_words_validate_for(struct AST *node)
{
	panic_unless(
		list_len(node->words.list) == 0,
		"AST_FOR should not have any words");
}

void
ast_words_validate_for_body(struct AST *node)
{
	panic_unless(
		list_len(node->words.list) == 0,
		"AST_FOR_BODY should not have any words");
}

void
ast_words_validate_for_bindings(struct AST *node)
{
	ast_words_validate_whitespace(node);

	AST_WORD_LIST_FOREACH(node, word) {
		if (ast_word_string_p(word)) {
			// ok
		} else if (ast_word_whitespace_p(word)) {
			// ok
		} else {
			SCOPE_MEMPOOL(pool);
			panic(
				"AST_FOR_BINDINGS has non-static word: '%s'",
				ast_word_flatten(pool, word));
		}
	}
}

void
ast_words_validate_for_words(struct AST *node)
{
	ast_words_validate_whitespace(node);
}

void
ast_words_validate_for_end(struct AST *node)
{
	ast_words_validate_whitespace(node);
	// TODO
}

void
ast_words_validate_include(struct AST *node)
{
	SCOPE_MEMPOOL(pool);

	struct ListEntry *current = list_head(node->words.list);
	// Word list must be of the following forms
	bool ok = false;
	struct ASTWord *path_word = NULL;
	if (list_len(node->words.list) == 0) {
		// empty
		ok = true;
	} else if (list_len(node->words.list) == 1) {
		// P
		struct ASTWord *word = list_entry_value(current);
		path_word = word;
		ok = !ast_word_whitespace_p(word)
			&& !ast_word_comment_p(word);
	} else if (list_len(node->words.list) == 2) {
		struct ASTWord *word = list_entry_value(current);
		struct ASTWord *next_word = list_entry_value(list_entry_next(current));
		if (ast_node_comment(node)) {
			// P#
			path_word = word;
			ok = !ast_word_whitespace_p(path_word)
				&& ast_word_comment_p(next_word);
		} else {
			// _P or P_
			if (ast_word_whitespace_p(word)) {
				path_word = next_word;
				ok = !ast_word_whitespace_p(path_word);
			} else {
				path_word = word;
				ok = ast_word_whitespace_p(next_word);
			}
		}
	} else if (list_len(node->words.list) == 3) {
		struct ASTWord *word = list_entry_value(current);
		struct ASTWord *next_word = list_entry_value(list_entry_next(current));
		struct ASTWord *next_next_word = list_entry_value(
			list_entry_next(list_entry_next(current)));

		if (ast_node_comment(node)) {
			// _P#
			path_word = next_word;
			ok = ast_word_whitespace_p(word)
				&& !ast_word_whitespace_p(next_word)
				&& ast_word_comment_p(next_next_word);
		} else {
			// _P_
			path_word = next_word;
			ok = ast_word_whitespace_p(word)
				&& !ast_word_whitespace_p(next_word)
				&& ast_word_whitespace_p(next_next_word);
		}
	} else if (list_len(node->words.list) == 4) {
		// _P_#
		struct ASTWord *word = list_entry_value(current);
		struct ASTWord *next_word = list_entry_value(list_entry_next(current));
		struct ASTWord *next_next_word = list_entry_value(
			list_entry_next(list_entry_next(current)));
		struct ASTWord *next_next_next_word = list_entry_value(
			list_entry_next(list_entry_next(list_entry_next(current))));

		path_word = next_word;
		ok = ast_word_whitespace_p(word)
			&& !ast_word_whitespace_p(next_word)
			&& !ast_word_comment_p(next_word)
			&& ast_word_whitespace_p(next_next_word)
			&& ast_word_comment_p(next_next_next_word);
	}

	unless (ok) {
		panic("invalid AST_INCLUDE word list");
	}

	// Recompute the path and system properties
	unless (path_word) {
		// No path is allowed for the sake of allowing easier
		// construction of AST_INCLUDE
		return;
	}

	char *path = ast_word_flatten(pool, path_word);
	bool invalid = false;
	const char *hint = "";
	if (*ASTIncludeType_identifier(ast_include_type(node)) == '.') {
		if (*path == '<') {
			path++;
			if (str_endswith(path, ">")) {
				path[strlen(path) - 1] = 0;
			} else {
				invalid = true;
				hint = ": missing > at the end";
			}
		} else if (*path == '"') {
			path++;
			if (str_endswith(path, "\"")) {
				path[strlen(path) - 1] = 0;
			} else {
				invalid = true;
				hint = ": missing \" at the end";
			}
		} else {
			invalid = true;
			hint = ": must start with < or \"";
		}
	}
	if (strlen(path) == 0 || invalid) {
		panic("AST_INCLUDE with invalid path for %s%s",
			  ASTIncludeType_identifier(ast_include_type(node)),
			  hint);
	}
}

void
ast_words_validate_target_rule(struct AST *node)
{
	panic_unless(
		list_len(node->words.list) == 0,
		"AST_TARGET_RULE should not have any words");
}

void
ast_words_validate_target_rule_dependencies(struct AST *node)
{
	ast_words_validate_whitespace(node);
}

void
ast_words_validate_target_rule_targets(struct AST *node)
{
	ast_words_validate_whitespace(node);
}

void
ast_words_validate_target_rule_body(struct AST *node)
{
	panic_unless(
		list_len(node->words.list) == 0,
		"AST_TARGET_BODY should not have any words");
}

void
ast_words_validate_target_command(struct AST *node)
{
	ast_words_validate_whitespace(node);
}

void
ast_words_validate_variable(struct AST *node)
{
	ast_words_validate_whitespace(node);
}

void
ast_words_validate_variable_name(struct AST *node)
{
	struct ListEntry *current = list_head(node->words.list);
	// Word list must be of the following forms
	bool ok = false;
	if (list_len(node->words.list) == 0) {
		// empty
		ok = true;
	} else if (list_len(node->words.list) == 1) {
		// X
		struct ASTWord *word = list_entry_value(current);
		ok = !ast_word_whitespace_p(word)
			&& !ast_word_comment_p(word);
	} else if (list_len(node->words.list) == 2) {
		// _X or X_
		struct ASTWord *word = list_entry_value(current);
		struct ASTWord *next_word = list_entry_value(list_entry_next(current));
		ok = (ast_word_whitespace_p(word)
			  && !ast_word_whitespace_p(next_word))
			|| (!ast_word_whitespace_p(word)
				&& ast_word_whitespace_p(next_word));
		ok = ok
			&& !ast_word_comment_p(word)
			&& !ast_word_comment_p(next_word);
	} else if (list_len(node->words.list) == 3) {
		// _X_
		struct ASTWord *word = list_entry_value(current);
		struct ASTWord *next_word = list_entry_value(list_entry_next(current));
		struct ASTWord *next_next_word = list_entry_value(
			list_entry_next(list_entry_next(current)));
		ok = ast_word_whitespace_p(word)
			&& !ast_word_whitespace_p(next_word)
			&& !ast_word_comment_p(next_word)
			&& ast_word_whitespace_p(next_next_word);
	}

	unless (ok) {
		panic("invalid AST_VARIABLE_NAME word list");
	}
}

void
ast_words_validate(struct AST *node)
{
	node->words_validate(node);
}

// Remove NODE from its parent. NODE is *not* freed.
void
ast_remove_from_parent(struct AST *node)
{
	struct AST *parent = node->parent;
	if (parent == node) {
		return;
	}

	if (parent->children.list) {
		struct ListEntry *entry = ast_child_list_entry_for_node(parent, node);
		if (entry) {
			ast_child_list_entry_remove(parent, entry);
		}
	}

	node->parent = node;
}

bool
ast_child_p(struct AST *node, struct AST *needle)
{
	if (node == needle) {
		return true;
	} else {
		return node->children.list
			&& ast_child_list_entry_for_node(node, needle);
	}
}

bool
ast_ports_framework_include_p(struct AST *node)
{
	if (
		ast_include_p(node)
		&& ast_include_type(node) == AST_INCLUDE_BMAKE
		&& ast_include_path(node)
		&& ast_include_system_p(node)
		)
	{
		SCOPE_MEMPOOL(pool);
		const char *path = ast_word_flatten(
			pool,
			ast_include_path(node));
		if (
			strcmp(path, "<bsd.port.options.mk>") == 0
			|| strcmp(path, "<bsd.port.pre.mk>") == 0
			|| strcmp(path, "<bsd.port.post.mk>") == 0
			|| strcmp(path, "<bsd.port.mk>") == 0
			)
		{
			return true;
		}
	}

	return false;
}

enum ASTType
ast_type(const struct AST *node)
{
	return node->type;
}

bool
ast_root_p(const struct AST *node)
{
	return node->type == AST_ROOT;
}

bool
ast_comment_p(const struct AST *node)
{
	return node->type == AST_COMMENT;
}

bool
ast_expr_p(const struct AST *node)
{
	return node->type == AST_EXPR;
}

bool
ast_if_p(const struct AST *node)
{
	return node->type == AST_IF;
}

bool
ast_if_branch_p(const struct AST *node)
{
	return node->type == AST_IF_BRANCH;
}

bool
ast_for_p(const struct AST *node)
{
	return node->type == AST_FOR;
}

bool
ast_for_bindings_p(const struct AST *node)
{
	return node->type == AST_FOR_BINDINGS;
}

bool
ast_for_words_p(const struct AST *node)
{
	return node->type == AST_FOR_WORDS;
}

bool
ast_for_body_p(const struct AST *node)
{
	return node->type == AST_FOR_BODY;
}

bool
ast_for_end_p(const struct AST *node)
{
	return node->type == AST_FOR_END;
}

bool
ast_include_p(const struct AST *node)
{
	return node->type == AST_INCLUDE;
}

bool
ast_target_rule_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE;
}

bool
ast_target_rule_dependencies_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE_DEPENDENCIES;
}

bool
ast_target_rule_targets_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE_TARGETS;
}

bool
ast_target_rule_body_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE_BODY;
}

bool
ast_target_command_p(const struct AST *node) {
	return node->type == AST_TARGET_COMMAND;
}

bool
ast_variable_p(const struct AST *node) {
	return node->type == AST_VARIABLE;
}

bool
ast_variable_name_p(const struct AST *node) {
	return node->type == AST_VARIABLE_NAME;
}

int
compare_ast_sort_words(
	const void *ap,
	const void *bp,
	void *userdata)
{
	struct ASTSortWordsData *this = userdata;
	struct ASTWord *a = this->words[*((size_t *)ap)];
	struct ASTWord *b = this->words[*((size_t *)bp)];

	// AST_WORD_COMMENT is always last and there can only be one of it
	if (ast_word_comment_p(a) && ast_word_comment_p(b)) {
		return 0;
	} else if (ast_word_comment_p(a)) {
		return 1;
	} else if (ast_word_comment_p(b)) {
		return -1;
	} else {
		return this->compare->compare(
			&a,
			&b,
			this->compare->compare_userdata);
	}
}

// Sort the words in NODE based on COMPARE. This ignores
// AST_WORD_WHITESPACE and only reorders the other words. The whitespace
// in between them is preserved.
void
ast_sort_words(
	struct AST *node,
	struct CompareTrait *compare)
{
	unless (list_head(node->words.list)) {
		return;
	}

	// This code doesn't handle AST_WORD_BOOL but sorting if expression
	// words doesn't make much sense anyway.
	if (ast_if_branch_p(node)) {
		return;
	}

	size_t *unsorted_index_map = reallocarray(NULL, list_len(node->words.list), sizeof(size_t));
	panic_unless(unsorted_index_map, "reallocarray: %s", strerror(errno));

	size_t *sorted_index_map = reallocarray(NULL, list_len(node->words.list), sizeof(size_t));
	panic_unless(sorted_index_map, "reallocarray: %s", strerror(errno));

	struct ASTWord **unsorted_words = reallocarray(NULL, list_len(node->words.list), sizeof(struct ASTWord *));
	panic_unless(unsorted_words, "reallocarray: %s", strerror(errno));

	struct ASTWord **sorted_words = reallocarray(NULL, list_len(node->words.list), sizeof(struct ASTWord *));
	panic_unless(sorted_words, "reallocarray: %s", strerror(errno));

	size_t index_map_len = 0;
	size_t word_index = 0;
	AST_WORD_LIST_FOREACH(node, word) {
		unsorted_words[word_index] = word;
		sorted_words[word_index] = word;
		unless (ast_word_whitespace_p(word)) {
			unsorted_index_map[index_map_len] = word_index;
			sorted_index_map[index_map_len] = word_index;
			index_map_len++;
		}
		word_index++;
	}

	struct ASTSortWordsData this = {
		.compare = compare,
		.words = sorted_words,
	};
	struct CompareTrait ast_sort_words_compare = {
		.compare = compare_ast_sort_words,
		.compare_userdata = &this,
	};
	sort(sorted_index_map, index_map_len, sizeof(size_t), &ast_sort_words_compare);

	// Swap the words according to our sort result
	for (size_t i = 0; i < index_map_len; i++) {
		sorted_words[unsorted_index_map[i]] = unsorted_words[sorted_index_map[i]];
	}

	// Apply to our word list
	size_t i = 0;
	AST_WORD_LIST_FOREACH(node, current_word) {
		struct ASTWord *word = sorted_words[i++];
		if (word != current_word) {
			list_entry_set_value(current_word_list_entry, word);
		}
	}

	map_truncate(node->words.word_to_list_entry_map);
	AST_WORD_LIST_FOREACH(node, word) {
		map_add(node->words.word_to_list_entry_map, word, word_list_entry);
	}

	free(unsorted_words);
	free(sorted_words);
	free(sorted_index_map);
	free(unsorted_index_map);
}

// Returns the NODE's parent. We are at the root node if NODE itself is
// returned.
struct AST *
ast_parent(struct AST *node)
{
	return node->parent;
}

// Return the parent AST_FOR of NODE. Returns NULL if there is no such
// node.
struct AST *
ast_parent_for(struct AST *node)
{
	if (ast_for_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_for(parent);
	}
}

// Return the parent AST_IF of NODE. Returns NULL if there is no such
// node.
struct AST *
ast_parent_if(struct AST *node)
{
	if (ast_if_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_if(parent);
	}
}

// Return the parent AST_INCLUDE of NODE. Returns NULL if there is no
// such node.
struct AST *
ast_parent_include(struct AST *node)
{
	if (ast_include_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_include(parent);
	}
}

// Return the parent AST_TARGET_RULE of NODE. Returns NULL if there is
// no such node.
struct AST *
ast_parent_target_rule(struct AST *node)
{
	if (ast_target_rule_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_target_rule(parent);
	}
}

void
ast_sexp_words(struct AST *node, const char *name, struct SexpWriter *w)
{
	if (list_head(node->words.list)) {
		sexp_writer_open_tree(w, name);
		AST_WORD_LIST_FOREACH(node, word) {
			ast_word_sexp(word, w);
		}
		sexp_writer_close_tree(w);
	}
}

void
ast_free(struct AST *node)
{
	if (node) {
		ast_remove_from_parent(node);

		if (node->children.list) {
			struct Mempool *pool = mempool_new();
			AST_CHILD_LIST_FOREACH(node, child) {
				mempool_add(pool, child, ast_free);
			}
			mempool_free(pool);
			list_free(node->children.list);
			node->children.list = NULL;
			map_free(node->children.child_to_list_entry_map);
			node->children.child_to_list_entry_map = NULL;
		}

		AST_WORD_LIST_FOREACH(node, word) {
			ast_word_free(word);
		}
		list_free(node->words.list);
		map_free(node->words.word_to_list_entry_map);

		free(node->indent);
		node->free(node);
		free(node);
	}
}

// Return the NODE's nth child. INDEX is zero-based. If INDEX is
// negative then the nth last child is returned instead. -1 returns the
// last child, -2 the second to last child, 0 the first child, etc.
//
// Returns NULL if there's no nth child.
struct AST *
ast_get_child(struct AST *node, ssize_t index)
{
	if (node->children.list) {
		if (index < 0) {
			index = MAX(0, list_len(node->children.list) + index);
		}

		ssize_t i = 0;
		AST_CHILD_LIST_FOREACH(node, child) {
			if (i == index) {
				return child;
			}
			i++;
		}
	}

	return NULL;
}

bool
ast_include_loaded_p(struct AST *node)
{
	if (ast_include_p(node)) {
		return node->include.loaded;
	} else {
		return false;
	}
}

void
ast_set_include_loaded(struct AST *node, bool loaded)
{
	if (ast_include_p(node)) {
		node->include.loaded = loaded;
	}
}

bool
ast_include_system_p(struct AST *node)
{
	SCOPE_MEMPOOL(pool);

	unless (ast_include_p(node)) {
		return false;
	}

	struct ASTWord *path_word = NULL;
	AST_WORD_LIST_FOREACH(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		} else {
			path_word = word;
			break;
		}
	}
	unless (path_word) {
		return false;
	}

	const char *path = ast_word_flatten(pool, path_word);
	if (*ASTIncludeType_identifier(ast_include_type(node)) == '.') {
		if (*path == '<') {
			return true;
		} else if (*path == '"') {
			return false;
		}
	}

	return false;
}

struct ASTWord *
ast_include_path(struct AST *node)
{
	unless (ast_include_p(node)) {
		return NULL;
	}

	AST_WORD_LIST_FOREACH(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		} else {
			return word;
		}
	}

	return NULL;
}

enum ASTIncludeType
ast_include_type(struct AST *node)
{
	return node->include.type;
}

void
ast_set_include_type(
	struct AST *node,
	const enum ASTIncludeType type)
{
	unless (ast_include_p(node)) {
		return;
	}

	node->include.type = type;
}

// Return the node's comment. This is the end of line comment
// immediately after an expression, variable, target command, or target
// rule. NULL is returned if no comment is set NULL.
//
// The node comment is also available as an AST_WORD_COMMENT in the
// node's word list. It's always the last word.
//
// To change the node comment use ast_words_edit_add_word().
struct ASTWord *
ast_node_comment(struct AST *node)
{
	if (list_tail(node->words.list)) {
		struct ASTWord *comment = list_entry_value(list_tail(node->words.list));
		if (ast_word_comment_p(comment)) {
			return comment;
		}
	}

	return NULL;
}

const char *
ast_expr_indent(struct AST *node)
{
	switch (ast_type(node)) {
	case AST_EXPR:
	case AST_FOR:
	case AST_FOR_END:
	case AST_IF:
	case AST_IF_BRANCH:
	case AST_INCLUDE:
		break;
	default:
		return "";
	}

	return node->indent;
}

void
ast_set_expr_indent(
	struct AST *node,
	const char *indent)
{
	switch (ast_type(node)) {
	case AST_EXPR:
	case AST_FOR:
	case AST_FOR_END:
	case AST_IF:
	case AST_IF_BRANCH:
	case AST_INCLUDE:
		break;
	default:
		return;
	}

	free(node->indent);
	node->indent = str_dup(NULL, indent);
}

enum ASTExprType
ast_expr_type(struct AST *node)
{
	if (ast_expr_p(node)) {
		return node->expr.type;
	} else {
		return AST_EXPR_ERROR;
	}
}

void
ast_set_expr_type(
	struct AST *node,
	const enum ASTExprType type)
{
	unless (ast_expr_p(node)) {
		return;
	}

	switch (type) {
	case AST_EXPR_ERROR:
	case AST_EXPR_EXPORT_ENV:
	case AST_EXPR_EXPORT_ENV_DOT:
	case AST_EXPR_EXPORT_LITERAL:
	case AST_EXPR_EXPORT:
	case AST_EXPR_INFO:
	case AST_EXPR_UNDEF:
	case AST_EXPR_UNEXPORT_ENV:
	case AST_EXPR_UNEXPORT:
	case AST_EXPR_WARNING:
		node->expr.type = type;
		return;
	}

	panic("invalid ASTExprType: %d", type);
}

enum ASTIfType
ast_if_branch_type(struct AST *node)
{
	if (ast_if_branch_p(node)) {
		return node->if_branch.type;
	} else {
		return AST_IF_IF;
	}
}

void
ast_set_if_branch_type(
	struct AST *node,
	const enum ASTIfType type)
{
	unless (ast_if_branch_p(node)) {
		return;
	}

	switch (type) {
	case AST_IF_IF:
	case AST_IF_DEF:
	case AST_IF_ELSE:
	case AST_IF_MAKE:
	case AST_IF_NDEF:
	case AST_IF_NMAKE:
		node->if_branch.type = type;
		return;
	}

	panic("invalid ASTIfType: %d", type);
}

const char *
ast_target_command_flags(struct AST *node)
{
	if (ast_target_command_p(node)) {
		return node->target_command.flags;
	} else {
		return "";
	}
}

enum ASTTargetRuleOperator
ast_target_rule_operator(struct AST *node)
{
	if (ast_target_rule_p(node)) {
		return node->target_rule.operator;
	} else {
		return AST_TARGET_RULE_OPERATOR_1;
	}
}

void
ast_set_target_command_flags(
	struct AST *node,
	const char *flags)
{
	unless (ast_target_command_p(node)) {
		return;
	}

	// TODO only keep valid flags

	free(node->target_command.flags);
	node->target_command.flags = str_dup(NULL, flags);
}

enum ASTVariableModifier
ast_variable_modifier(struct AST *node)
{
	if (ast_variable_p(node)) {
		return node->variable.modifier;
	} else {
		return AST_VARIABLE_MODIFIER_ASSIGN;
	}
}

struct ASTWord *
ast_variable_name(struct AST *node)
{
	switch (ast_type(node)) {
	case AST_VARIABLE:
		node = ast_get_child(node, AST_CHILD_VARIABLE_NAME);
		break;
	case AST_VARIABLE_NAME:
		break;
	default:
		return NULL;
	}

	AST_WORD_LIST_FOREACH(node, word) {
		unless (ast_word_whitespace_p(word)) {
			return word;
		}
	}

	panic("variable name has no non-whitespace word");
}

struct ListEntry *
ast_word_list_append(
	struct AST *node,
	struct ASTWord *word)
{
	struct ListEntry *tail = list_tail(node->words.list);
	if (tail && ast_word_comment_p(list_entry_value(tail))) {
		if (ast_word_comment_p(word)) {
			ast_word_list_entry_remove(node, tail);
			tail = list_tail(node->words.list);
		} else {
			return ast_word_list_insert_before(node, tail, word, false);
		}
	}

	return ast_word_list_insert_after(node, tail, word, false);
}

struct ListEntry *
ast_word_list_insert_after(
	struct AST *node,
	struct ListEntry *head,
	struct ASTWord *word,
	bool replace)
{
	if (ast_word_bool_p(word) && !ast_if_branch_p(node)) {
		// TODO: Diagnostic
		return head;
	}

	if (head) {
		if (ast_word_whitespace_p(word)) {
			struct ListEntry *merge_entry = NULL;
			if (ast_word_whitespace_p(list_entry_value(head))) {
				merge_entry = head;
			} else if (
				list_entry_next(head)
				&& ast_word_whitespace_p(list_entry_value(list_entry_next(head)))
				)
			{
				merge_entry = list_entry_next(head);
			}

			if (merge_entry) {
				struct ASTWord *old_word = list_entry_value(merge_entry);
				unless (replace) {
					char *value = str_printf(
						NULL, "%s%s",
						ast_word_value(old_word),
						ast_word_value(word));
					ast_word_free(word);
					word = ast_word_new_whitespace(value, ast_word_pos(old_word));
					free(value);
				}
				ast_word_free(old_word);
				map_remove(node->words.word_to_list_entry_map, old_word);
				list_entry_set_value(merge_entry, word);
				map_add(node->words.word_to_list_entry_map, word, merge_entry);
				return merge_entry;
			}
		}
	}

	struct ListEntry *new_list_entry = list_insert_after(
		node->words.list,
		head,
		word);
	map_add(node->words.word_to_list_entry_map, word, new_list_entry);
	return new_list_entry;
}

struct ListEntry *
ast_word_list_insert_before(
	struct AST *node,
	struct ListEntry *head,
	struct ASTWord *word,
	bool replace)
{
	if (ast_word_bool_p(word) && !ast_if_branch_p(node)) {
		return head;
	}

	if (head) {
		if (ast_word_whitespace_p(word)) {
			struct ListEntry *merge_entry = NULL;
			if (ast_word_whitespace_p(list_entry_value(head))) {
				merge_entry = head;
			} else if (
				list_entry_prev(head)
				&& ast_word_whitespace_p(list_entry_value(list_entry_prev(head)))
				)
			{
				merge_entry = list_entry_prev(head);
			}

			if (merge_entry) {
				struct ASTWord *old_word = list_entry_value(merge_entry);
				unless (replace) {
					char *value = str_printf(
						NULL, "%s%s",
						ast_word_value(old_word),
						ast_word_value(word));
					ast_word_free(word);
					word = ast_word_new_whitespace(value, ast_word_pos(old_word));
					free(value);
				}
				ast_word_free(old_word);
				map_remove(node->words.word_to_list_entry_map, old_word);
				list_entry_set_value(merge_entry, word);
				map_add(node->words.word_to_list_entry_map, word, merge_entry);
				return merge_entry;
			}
		}
	}

	struct ListEntry *new_list_entry = list_insert_before(
		node->words.list,
		head,
		word);
	map_add(node->words.word_to_list_entry_map, word, new_list_entry);
	return new_list_entry;
}

struct ListEntry *
ast_word_list_entry_remove(
	struct AST *node,
	struct ListEntry *list_entry)
{
	ast_word_free(list_entry_value(list_entry));
	list_entry = list_entry_remove(list_entry);
	// Remove whitespace after the entry too to maintain the word list invariant
	if (list_entry && ast_word_whitespace_p(list_entry_value(list_entry))) {
		map_remove(node->words.word_to_list_entry_map, list_entry_value(list_entry));
		ast_word_free(list_entry_value(list_entry));
		list_entry = list_entry_remove(list_entry);
	}
	return list_entry;
}

struct ListEntry *
ast_word_list_entry_for_word(
	struct AST *node,
	struct ASTWord *word)
{
	return map_get(node->words.word_to_list_entry_map, word);
}

struct ASTWordsEdit *
ast_words_edit_new(
	struct AST *node,
	enum ASTWordsEditFlags flags)
{
	struct ASTWordsEdit *edit = xmalloc(sizeof(struct ASTWordsEdit));
	edit->flags = flags;
	edit->node = node;
	edit->queue = queue_new();
	edit->pool = mempool_new();
	return edit;
}

void
ast_words_edit_free(struct ASTWordsEdit *edit)
{
	if (edit) {
		queue_free(edit->queue);
		mempool_free(edit->pool);
		free(edit);
	}
}

void
ast_words_edit_apply(struct ASTWordsEdit *edit)
{
	void (*fn)(struct ASTWordsEdit *, struct Queue *) = NULL;
	while ((fn = queue_pop(edit->queue)) != NULL) {
		fn(edit, edit->queue);
	}

	// Check word list invariants are still ok
	ast_words_validate(edit->node);

	ast_words_edit_free(edit);
}

void
ast_words_edit_add_word_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);
	struct ASTWord *other = queue_pop(args);

	if (ast_word_comment_p(word)) {
		ast_word_list_append(edit->node, word);
	} else {
		struct ListEntry *current;
		current = ast_word_list_entry_for_word(edit->node, other);
		if (current) {
			// insert after other word
			ast_word_list_insert_after(edit->node, current, word, false);
		} else {
			// append at the end
			ast_word_list_append(edit->node, word);
		}
	}

	mempool_forget(edit->pool, word);
}

// Adds WORD before the OTHER word in the NODE. NODE takes ownership of
// WORD. Appends WORD if OTHER is NULL or can't be found.
void
ast_words_edit_add_word_before_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);
	struct ASTWord *other = queue_pop(args);

	if (ast_word_comment_p(word)) {
		ast_word_list_append(edit->node, word);
	} else {
		struct ListEntry *current;
		current = ast_word_list_entry_for_word(edit->node, other);
		if (current) {
			// insert before other word
			ast_word_list_insert_before(edit->node, current, word, false);
		} else {
			// append at the end
			ast_word_list_append(edit->node, word);
		}
	}

	mempool_forget(edit->pool, word);
}

void
ast_words_edit_append_word_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);
	ast_word_list_append(edit->node, word);
	mempool_forget(edit->pool, word);
}

void
ast_words_edit_remove_word_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);

	struct ListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	if (list_entry) {
		struct ASTWord *word = list_entry_value(list_entry);
		if (
			ast_word_whitespace_p(word)
			&& !ast_comment_p(edit->node)
			&& list_entry_next(list_entry) != NULL
			)
		{
			// Can't remove whitespace since we must maintain our words
			// invariant but reset to default whitespace of " ".
			ast_word_set_value(word, " ");
		} else {
			ast_word_list_entry_remove(edit->node, list_entry);
		}
	}
}

void
ast_words_edit_remove_all_words_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	for (struct ListEntry *current = list_head(edit->node->words.list);
		 current;
		 current = ast_word_list_entry_remove(edit->node, current));
}

void
ast_words_edit_remove_beginning_whitespace_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	while (
		list_head(edit->node->words.list) &&
		ast_word_whitespace_p(list_entry_value(list_head(edit->node->words.list)))
		)
	{
		ast_word_list_entry_remove(edit->node, list_head(edit->node->words.list));
	}
}

void
ast_words_edit_remove_trailing_whitespace_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	while (
		list_tail(edit->node->words.list)
		&& ast_word_whitespace_p(list_entry_value(list_tail(edit->node->words.list)))
		)
	{
		ast_word_list_entry_remove(edit->node, list_tail(edit->node->words.list));
	}
}

void
ast_words_edit_remove_whitespace_after_word_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);

	struct ListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	unless (list_entry) {
		return;
	}
	unless (
		ast_word_bool_p(list_entry_value(list_entry))
		|| list_tail(edit->node->words.list) == list_entry_next(list_entry)
		)
	{
		return;
	}
	if (
		list_entry_next(list_entry)
		&& ast_word_whitespace_p(list_entry_value(list_entry_next(list_entry)))
		)
	{
		ast_word_list_entry_remove(edit->node, list_entry_next(list_entry));
	}
}

void
ast_words_edit_remove_whitespace_before_word_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);

	struct ListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	unless (list_entry) {
		return;
	}
	unless (
		ast_word_bool_p(list_entry_value(list_entry))
		|| list_head(edit->node->words.list) == list_entry_prev(list_entry)
		)
	{
		return;
	}
	if (
		list_entry_prev(list_entry)
		&& ast_word_whitespace_p(list_entry_value(list_entry_prev(list_entry)))
		)
	{
		ast_word_list_entry_remove(edit->node, list_entry_prev(list_entry));
	}
}

void
ast_words_edit_set_whitespace_after_word_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);
	struct ASTWord *whitespace = queue_pop(args);

	struct ListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	unless (list_entry) {
		return;
	}

	// Can't add whitespace after an AST_WORD_COMMENT
	if (ast_word_comment_p(list_entry_value(list_entry))) {
		return;
	}

	ast_word_list_insert_after(edit->node, list_entry, whitespace, true);
	mempool_forget(edit->pool, whitespace);
}

void
ast_words_edit_set_whitespace_before_word_impl(
	struct ASTWordsEdit *edit,
	struct Queue *args)
{
	struct ASTWord *word = queue_pop(args);
	struct ASTWord *whitespace = queue_pop(args);

	unless (ast_word_whitespace_p(whitespace)) {
		return;
	}
	struct ListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	unless (list_entry) {
		return;
	}

	ast_word_list_insert_before(edit->node, list_entry, whitespace, true);
	mempool_forget(edit->pool, whitespace);
}

// Adds WORD after the OTHER word in the NODE. OTHER is searched for by
// identity not by value. NODE takes ownership of WORD.
void
ast_words_edit_add_word(
	struct ASTWordsEdit *edit,
	struct ASTWord *word,
	struct ASTWord *other)
{
	queue_push(edit->queue, ast_words_edit_add_word_impl);
	queue_push(edit->queue, word);
	mempool_add(edit->pool, word, ast_word_free);
	queue_push(edit->queue, other);
}

// Adds WORD before the OTHER word in the NODE. OTHER is searched for by
// identity not by value. NODE takes ownership of WORD.
void
ast_words_edit_add_word_before(
	struct ASTWordsEdit *edit,
	struct ASTWord *word,
	struct ASTWord *other)
{
	queue_push(edit->queue, ast_words_edit_add_word_before_impl);
	queue_push(edit->queue, word);
	mempool_add(edit->pool, word, ast_word_free);
	queue_push(edit->queue, other);
}

// Appends WORD to the NODE's word list. NODE takes ownership of WORD.
void
ast_words_edit_append_word(
	struct ASTWordsEdit *edit,
	struct ASTWord *word)
{
	queue_push(edit->queue, ast_words_edit_append_word_impl);
	queue_push(edit->queue, word);
	mempool_add(edit->pool, word, ast_word_free);
}

// Remove WORD from NODE. WORD is searched for by identity not by value.
// If there is whitespace after WORD it is removed too. All removed
// words are freed immediately.
void
ast_words_edit_remove_word(
	struct ASTWordsEdit *edit,
	struct ASTWord *word)
{
	queue_push(edit->queue, ast_words_edit_remove_word_impl);
	queue_push(edit->queue, word);
}

void
ast_words_edit_remove_all_words(
	struct ASTWordsEdit *edit)
{
	queue_push(edit->queue, ast_words_edit_remove_all_words_impl);
}

void
ast_words_edit_remove_beginning_whitespace(
	struct ASTWordsEdit *edit)
{
	queue_push(edit->queue, ast_words_edit_remove_beginning_whitespace_impl);
}

void
ast_words_edit_remove_trailing_whitespace(
	struct ASTWordsEdit *edit)
{
	queue_push(edit->queue, ast_words_edit_remove_trailing_whitespace_impl);
}

void
ast_words_edit_remove_whitespace_after_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word)
{
	queue_push(edit->queue, ast_words_edit_remove_whitespace_after_word_impl);
	queue_push(edit->queue, word);
}

void
ast_words_edit_remove_whitespace_before_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word)
{
	queue_push(edit->queue, ast_words_edit_remove_whitespace_before_word_impl);
	queue_push(edit->queue, word);
}

void
ast_words_edit_set_whitespace_after_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word,
	struct ASTWord *ws)
{
	queue_push(edit->queue, ast_words_edit_set_whitespace_after_word_impl);
	queue_push(edit->queue, word);
	queue_push(edit->queue, ws);
	mempool_add(edit->pool, ws, ast_word_free);
}

void
ast_words_edit_set_whitespace_before_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word,
	struct ASTWord *ws)
{
	queue_push(edit->queue, ast_words_edit_set_whitespace_before_word_impl);
	queue_push(edit->queue, word);
	queue_push(edit->queue, ws);
	mempool_add(edit->pool, ws, ast_word_free);
}

// Render the NODE's words into a new string owned by EXTPOOL.
char *
ast_words_flatten(
	struct AST *node,
	struct Mempool *extpool)
{
	unless (node->words.list) {
		return str_dup(extpool, "");
	}

	SCOPE_MEMPOOL(pool);
	struct File *f = file_open_memstream(pool);
	AST_WORD_LIST_FOREACH(node, word) {
		ast_word_render(word, f);
	}
	return file_slurp(f, extpool, NULL);
}

// Render the NODE's words into a new string owned by EXTPOOL. Meta
// words are skipped and each word is separated by SEPARATOR.
char *
ast_words_no_meta_flatten(
	struct AST *node,
	struct Mempool *extpool,
	const char *separator)
{
	unless (node->words.list) {
		return str_dup(extpool, "");
	}

	SCOPE_MEMPOOL(pool);
	struct File *f = file_open_memstream(pool);
	size_t print_sep = false;
	AST_WORD_LIST_FOREACH(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		}
		if (print_sep) {
			file_puts(f, separator);
		} else {
			print_sep = true;
		}
		ast_word_render(word, f);
	}
	return file_slurp(f, extpool, NULL);
}

// Return the number of words of NODE.
size_t
ast_words_len(struct AST *node)
{
	if (node->words.list) {
		return list_len(node->words.list);
	} else {
		return 0;
	}
}

// Return the number of non-meta words of NODE.
size_t
ast_words_no_meta_len(struct AST *node)
{
	unless (node->words.list) {
		return 0;
	}

	size_t len = 0;
	AST_WORD_LIST_FOREACH(node, word) {
		if (ast_word_meta_p(word)) {
			// nothing
		} else {
			len++;
		}
	}
	return len;
}

// Return the sibling just before NODE. Return NULL if there is no such
// sibling. Return NULL if NODE has no next sibling.
struct AST *
ast_next_sibling(struct AST *node)
{
	if (node->parent == node) {
		return NULL;
	} else unless (node->parent->children.list) {
		return NULL;
	}

	struct ListEntry *list_entry;
	list_entry = ast_child_list_entry_for_node(node->parent, node);
	unless (list_entry) {
		return NULL;
	}

	list_entry = list_entry_next(list_entry);
	unless (list_entry) {
		return NULL;
	}

	return list_entry_value(list_entry);
}

// Return the sibling just after NODE. Return NULL if there is no such
// sibling. Return NULL if NODE has no previous sibling.
struct AST *
ast_prev_sibling(struct AST *node)
{
	if (node->parent == node) {
		return NULL;
	} else unless (node->parent->children.list) {
		return NULL;
	}

	struct ListEntry *list_entry;
	list_entry = ast_child_list_entry_for_node(node->parent, node);
	unless (list_entry) {
		return NULL;
	}

	list_entry = list_entry_prev(list_entry);
	unless (list_entry) {
		return NULL;
	}

	return list_entry_value(list_entry);
}

// Return the word just after CURRENT_WORD. Return NULL if there is no
// such word or if CURRENT_WORD isn't part of NODE. If CURRENT_WORD is
// NULL return the first word of NODE or NULL if NODE has no words.
struct ASTWord *
ast_next_word(
	struct AST *node,
	struct ASTWord *current_word)
{
	unless (node->words.list) {
		return NULL;
	}

	struct ListEntry *list_entry;
	if (current_word) {
		list_entry = ast_word_list_entry_for_word(node, current_word);
	} else {
		list_entry = list_head(node->words.list);
	}
	unless (list_entry) {
		return NULL;
	}

	list_entry = list_entry_next(list_entry);
	unless (list_entry) {
		return NULL;
	}

	return list_entry_value(list_entry);
}

// Return the word just before CURRENT_WORD. Return NULL if there is no
// such word or if CURRENT_WORD isn't part of NODE. If CURRENT_WORD is
// NULL return the first word of NODE or NULL if NODE has no words.
struct ASTWord *
ast_prev_word(
	struct AST *node,
	struct ASTWord *current_word)
{
	unless (node->words.list) {
		return NULL;
	}

	struct ListEntry *list_entry;
	if (current_word) {
		list_entry = ast_word_list_entry_for_word(node, current_word);
	} else {
		list_entry = list_head(node->words.list);
	}
	unless (list_entry) {
		return NULL;
	}

	list_entry = list_entry_prev(list_entry);
	unless (list_entry) {
		return NULL;
	}

	return list_entry_value(list_entry);
}

struct ListEntry *
ast_child_list_append(
	struct AST *node,
	struct AST *new_child)
{
	panic_if(
		map_contains(node->children.child_to_list_entry_map, new_child),
		"cannot add child twice to the same tree");
	struct ListEntry *new_entry = list_append(node->children.list, new_child);
	map_add(node->children.child_to_list_entry_map, new_child, new_entry);
	new_child->parent = node;
	return new_entry;
}

struct ListEntry *
ast_child_list_insert_before(
	struct AST *node,
	struct ListEntry *list_entry,
	struct AST *new_child)
{
	panic_if(
		map_contains(node->children.child_to_list_entry_map, new_child),
		"cannot add child twice to the same tree");
	struct ListEntry *new_entry = list_insert_before(
		node->children.list,
		list_entry,
		new_child);
	map_add(node->children.child_to_list_entry_map, new_child, new_entry);
	new_child->parent = node;
	return new_entry;
}

struct ListEntry *
ast_child_list_insert_after(
	struct AST *node,
	struct ListEntry *list_entry,
	struct AST *new_child)
{
	panic_if(
		map_contains(node->children.child_to_list_entry_map, new_child),
		"cannot add child twice to the same tree");
	struct ListEntry *new_entry = list_insert_after(
		node->children.list,
		list_entry,
		new_child);
	map_add(node->children.child_to_list_entry_map, new_child, new_entry);
	new_child->parent = node;
	return new_entry;
}

struct ListEntry *
ast_child_list_entry_for_node(
	struct AST *node,
	struct AST *child)
{
	return map_get(node->children.child_to_list_entry_map, child);
}

struct ListEntry *
ast_child_list_entry_remove(
	struct AST *node,
	struct ListEntry *entry)
{
	map_remove(node->children.child_to_list_entry_map, list_entry_value(entry));
	return list_entry_remove(entry);
}

struct ASTEdit *
ast_edit_new(
	struct AST *node,
	enum ASTEditFlags flags)
{
	struct ASTEdit *edit = xmalloc(sizeof(struct ASTEdit));
	edit->flags = flags;
	edit->root = node;
	edit->queue = queue_new();
	edit->pool = mempool_new();
	return edit;
}

void
ast_edit_free(struct ASTEdit *edit)
{
	if (edit) {
		queue_free(edit->queue);
		mempool_free(edit->pool);
		free(edit);
	}
}

struct AST *
ast_edit_apply(struct ASTEdit *edit)
{
	void (*fn)(struct ASTEdit *, struct Queue *) = NULL;
	while ((fn = queue_pop(edit->queue)) != NULL) {
		fn(edit, edit->queue);
	}

	// Check node invariants are still ok
	// ast_validate(edit->node);

	struct AST *root = edit->root;
	ast_edit_free(edit);
	return root;
}

struct ListEntry *
ast_find_child(
	struct AST *root,
	struct AST *child,
	struct AST **parent)
{
	unless (root->children.list) {
		return NULL;
	}

	struct ListEntry *entry = ast_child_list_entry_for_node(root, child);
	if (entry) {
		*parent = root;
		return entry;
	}

	AST_CHILD_LIST_FOREACH(root, node) {
		struct ListEntry *entry = ast_find_child(node, child, parent);
		if (entry) {
			return entry;
		}
	}

	return NULL;
}

void
ast_edit_add_child_before_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	struct AST *new_child = queue_pop(args);
	struct AST *other = queue_pop(args);

	struct AST *parent = NULL;
	struct ListEntry *list_entry = ast_find_child(
		edit->root,
		other,
		&parent);
	if (list_entry) {
		ast_child_list_insert_before(parent, list_entry, new_child);
	} else {
		ast_child_list_append(edit->root, new_child);
	}
	mempool_forget(edit->pool, new_child);
}

void
ast_edit_add_child_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	struct AST *new_child = queue_pop(args);
	struct AST *other = queue_pop(args);

	struct AST *parent = NULL;
	struct ListEntry *list_entry = ast_find_child(
		edit->root,
		other,
		&parent);
	if (list_entry) {
		ast_child_list_insert_after(parent, list_entry, new_child);
	} else {
		ast_child_list_append(edit->root, new_child);
	}
	mempool_forget(edit->pool, new_child);
}

void
ast_edit_append_child_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	struct AST *new_child = queue_pop(args);
	ast_child_list_append(edit->root, new_child);
	mempool_forget(edit->pool, new_child);
}

void
ast_edit_remove_child_impl(
	struct ASTEdit  *edit,
	struct Queue *args)
{
	struct AST *child = queue_pop(args);

	struct AST *parent = NULL;
	struct ListEntry *list_entry = ast_find_child(
		edit->root,
		child,
		&parent);
	if (list_entry) {
		ast_free(list_entry_value(list_entry));
	}
}

void
ast_edit_remove_all_children_impl(
	struct ASTEdit  *edit,
	struct Queue *args)
{
	SCOPE_MEMPOOL(pool);
	AST_CHILD_LIST_FOREACH(edit->root, child) {
		mempool_add(pool, child, ast_free);
	}
}

void
ast_edit_replace_child_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	struct AST *child = queue_pop(args);
	struct AST *replacement = queue_pop(args);

	struct AST *parent = NULL;
	struct ListEntry *list_entry = ast_find_child(
		edit->root,
		child,
		&parent);
	if (list_entry) {
		ast_child_list_insert_after(parent, list_entry, replacement);
		mempool_forget(edit->pool, replacement);
		ast_free(child);
	}
}

void
ast_edit_chain_apply_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	struct ASTEdit *other = queue_pop(args);
	ast_edit_apply(other);
	mempool_forget(edit->pool, other);
}

void
ast_edit_apply_words_edit_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	struct ASTWordsEdit *words_edit = queue_pop(args);
	ast_words_edit_apply(words_edit);
	mempool_forget(edit->pool, words_edit);
}

void
ast_edit_set_variable_modifier_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	enum ASTVariableModifier *modifier = queue_pop(args);
	edit->root->variable.modifier = *modifier;
	mempool_release(edit->pool, modifier);
}

void
ast_edit_set_target_rule_operator_impl(
	struct ASTEdit *edit,
	struct Queue *args)
{
	enum ASTTargetRuleOperator *operator = queue_pop(args);
	edit->root->target_rule.operator = *operator;
	mempool_release(edit->pool, operator);
}

// Add NEW_NODE to OTHER's parent just above OTHER. NEW_NODE becomes a
// new sibling of OTHER.
void
ast_edit_add_child_before(
	struct ASTEdit *edit,
	struct AST *new_child,
	struct AST *other)
{
	queue_push(edit->queue, ast_edit_add_child_before_impl);
	queue_push(edit->queue, new_child);
	mempool_add(edit->pool, new_child, ast_free);
	queue_push(edit->queue, other);
}

// Adds NEW_CHILD to the tree. NEW_CHILD will be added after OTHER. The
// AST will take ownership of NEW_CHILD.
void
ast_edit_add_child(
	struct ASTEdit *edit,
	struct AST *new_child,
	struct AST *other)
{
	queue_push(edit->queue, ast_edit_add_child_impl);
	queue_push(edit->queue, new_child);
	mempool_add(edit->pool, new_child, ast_free);
	queue_push(edit->queue, other);
}

// Appends NEW_CHILD to the tree. The AST will take ownership of
// NEW_CHILD.
void
ast_edit_append_child(
	struct ASTEdit *edit,
	struct AST *new_child)
{
	queue_push(edit->queue, ast_edit_append_child_impl);
	queue_push(edit->queue, new_child);
	mempool_add(edit->pool, new_child, ast_free);
}

// Removes CHILD from the edited tree. CHILD will be freed.
void
ast_edit_remove_child(
	struct ASTEdit *edit,
	struct AST *child)
{
	queue_push(edit->queue, ast_edit_remove_child_impl);
	queue_push(edit->queue, child);
}

// Remove all children of the edited tree.
void
ast_edit_remove_all_children(struct ASTEdit *edit)
{
	queue_push(edit->queue, ast_edit_remove_all_children_impl);
}

// Replaces CHILD with REPLACEMENT. CHILD will be freed.
void
ast_edit_replace_child(
	struct ASTEdit *edit,
	struct AST *child,
	struct AST *replacement)
{
	queue_push(edit->queue, ast_edit_replace_child_impl);
	queue_push(edit->queue, child);
	queue_push(edit->queue, replacement);
	mempool_add(edit->pool, replacement, ast_free);
}

// Apply OTHER together with EDIT.
void
ast_edit_chain_apply(
	struct ASTEdit *edit,
	struct ASTEdit *other)
{
	queue_push(edit->queue, ast_edit_chain_apply_impl);
	queue_push(edit->queue, other);
	mempool_add(edit->pool, other, ast_edit_free);
}

// Apply WORDS_EDIT together with EDIT. Editing words doesn't change the
// tree structure, so it could safely be applied immediately. With this
// function you can delay it and only apply it when the entire edit
// operation is applied.
//
// EDIT takes ownership of WORDS_EDIT.
void
ast_edit_apply_words_edit(
	struct ASTEdit *edit,
	struct ASTWordsEdit *words_edit)
{
	queue_push(edit->queue, ast_edit_apply_words_edit_impl);
	queue_push(edit->queue, words_edit);
	mempool_add(edit->pool, words_edit, ast_words_edit_free);
}

// Set the node's variable modifier. The node must be a
// AST_VARIABLE or this is a no-op.
void
ast_edit_set_variable_modifier(
	struct ASTEdit *edit,
	const enum ASTVariableModifier modifier)
{
	unless (ast_variable_p(edit->root)) {
		return;
	}

	queue_push(edit->queue, ast_edit_set_variable_modifier_impl);
	enum ASTVariableModifier *mod = mempool_alloc(
		edit->pool,
		sizeof(enum ASTVariableModifier));
	*mod = modifier;
	queue_push(edit->queue, mod);
}

// Set the node's target rule operator. The node must be a
// AST_TARGET_RULE or this is a no-op.
void
ast_edit_set_target_rule_operator(
	struct ASTEdit *edit,
	const enum ASTTargetRuleOperator operator)
{
	unless (ast_target_rule_p(edit->root)) {
		return;
	}

	queue_push(edit->queue, ast_edit_set_target_rule_operator_impl);
	enum ASTTargetRuleOperator *op = mempool_alloc(
		edit->pool,
		sizeof(enum ASTTargetRuleOperator));
	*op = operator;
	queue_push(edit->queue, op);
}

void
ast_visit_trait_init(struct ASTVisitTrait *this)
{
	memset(this, 0, sizeof(struct ASTVisitTrait));
}

ASTVisitFn
ast_visit_trait_get_pre_visit_fn(
	struct ASTVisitTrait *trait,
	enum ASTType type)
{
	switch (type) {
	case AST_ROOT:
		return trait->visit_pre_root;
	case AST_COMMENT:
		return trait->visit_pre_comment;
	case AST_EXPR:
		return trait->visit_pre_expr;
	case AST_FOR:
		return trait->visit_pre_for;
	case AST_FOR_BINDINGS:
		return trait->visit_pre_for_bindings;
	case AST_FOR_WORDS:
		return trait->visit_pre_for_words;
	case AST_FOR_BODY:
		return trait->visit_pre_for_body;
	case AST_FOR_END:
		return trait->visit_pre_for_end;
	case AST_IF:
		return trait->visit_pre_if;
	case AST_IF_BRANCH:
		return trait->visit_pre_if_branch;
	case AST_INCLUDE:
		return trait->visit_pre_include;
	case AST_TARGET_COMMAND:
		return trait->visit_pre_target_command;
	case AST_TARGET_RULE:
		return trait->visit_pre_target_rule;
	case AST_TARGET_RULE_TARGETS:
		return trait->visit_pre_target_rule_targets;
	case AST_TARGET_RULE_DEPENDENCIES:
		return trait->visit_pre_target_rule_dependencies;
	case AST_TARGET_RULE_BODY:
		return trait->visit_pre_target_rule_body;
	case AST_VARIABLE:
		return trait->visit_pre_variable;
	case AST_VARIABLE_NAME:
		return trait->visit_pre_variable_name;
	}

	return NULL;
}

ASTVisitFn
ast_visit_trait_get_visit_fn(
	struct ASTVisitTrait *trait,
	enum ASTType type)
{
	switch (type) {
	case AST_ROOT:
		return trait->visit_root;
	case AST_COMMENT:
		return trait->visit_comment;
	case AST_EXPR:
		return trait->visit_expr;
	case AST_FOR:
		return trait->visit_for;
	case AST_FOR_BINDINGS:
		return trait->visit_for_bindings;
	case AST_FOR_WORDS:
		return trait->visit_for_words;
	case AST_FOR_BODY:
		return trait->visit_for_body;
	case AST_FOR_END:
		return trait->visit_for_end;
	case AST_IF:
		return trait->visit_if;
	case AST_IF_BRANCH:
		return trait->visit_if_branch;
	case AST_INCLUDE:
		return trait->visit_include;
	case AST_TARGET_COMMAND:
		return trait->visit_target_command;
	case AST_TARGET_RULE:
		return trait->visit_target_rule;
	case AST_TARGET_RULE_TARGETS:
		return trait->visit_target_rule_targets;
	case AST_TARGET_RULE_DEPENDENCIES:
		return trait->visit_target_rule_dependencies;
	case AST_TARGET_RULE_BODY:
		return trait->visit_target_rule_body;
	case AST_VARIABLE:
		return trait->visit_variable;
	case AST_VARIABLE_NAME:
		return trait->visit_variable_name;
	}

	return NULL;
}

void
ast_visit_pre_dispatch(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (set_contains(visit->visited_nodes.pre, node)) {
		return;
	}

	ASTVisitPredicateFn visit_pre_node_p = visit->trait.visit_pre_node_p;
	if (visit_pre_node_p) {
		unless (visit_pre_node_p(visit, node)) {
			goto done;
		}
	}

	ASTVisitFn visit_fn = ast_visit_trait_get_pre_visit_fn(
		&visit->trait,
		ast_type(node));
	if (visit_fn) {
		visit_fn(visit, node);
	} else if (visit->trait.default_pre_visit_fn) {
		visit->trait.default_pre_visit_fn(visit, node);
	}

done:
	set_add(visit->visited_nodes.pre, node);
}

void
ast_visit_dispatch(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (set_contains(visit->visited_nodes.post, node)) {
		return;
	}

	ASTVisitPredicateFn visit_node_p = visit->trait.visit_node_p;
	if (visit_node_p) {
		unless (visit_node_p(visit, node)) {
			goto done;
		}
	}

	ASTVisitFn visit_fn = ast_visit_trait_get_visit_fn(
		&visit->trait,
		ast_type(node));
	if (visit_fn) {
		visit_fn(visit, node);
	} else if (visit->trait.default_visit_fn) {
		visit->trait.default_visit_fn(visit, node);
	}

done:
	set_add(visit->visited_nodes.post, node);
}

void
ast_visit_helper(
	struct ASTVisit *visit,
	struct AST *node)
{
	ast_visit_pre_dispatch(visit, node);

	switch (visit->state) {
	case AST_VISIT_STATE_CONTINUE:
	{
		ASTVisitPredicateFn visit_children_p = visit->trait.visit_children_p;
		if (!visit_children_p || visit_children_p(visit, node)) {
			if (node->children.list) {
				AST_CHILD_LIST_FOREACH(node, child) {
					ast_visit_helper(visit, child);
					switch (visit->state) {
					case AST_VISIT_STATE_ABORT:
					case AST_VISIT_STATE_STOP:
						return;
					case AST_VISIT_STATE_CONTINUE:
						break;
					}
				}
			}
		}
		ast_visit_dispatch(visit, node);
		return;
	}
	case AST_VISIT_STATE_ABORT:
	case AST_VISIT_STATE_STOP:
		return;
	}
}

void
ast_visit_run(struct ASTVisit *visit)
{
	ast_visit_reinit(visit);
	ast_visit_helper(visit, visit->root);
	ast_visit_done(visit);
}

// Set the current formatting settings. This determines the formatting
// settings of all subsequent calls to ast_vist_format() until
// ast_visit_set format_settings() is called again. Calls to
// ast_visit_set_format_settings() will not override the settings of
// already scheduled formats.
//
// VISIT will _not_ take ownership of RULES and FORMAT_SETTINGS. The
// lifetime of both must at least as long as VISIT.
void
ast_visit_set_format_settings(
	struct ASTVisit *visit,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings)
{
	visit->format.settings = format_settings;
	visit->format.rules = rules;
}

// Schedule a format of NODE with the current formatting
// settings. ast_visit_set_format_settings() must have been called
// first. The formatting will happen after the visit.
void
ast_visit_format(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (visit->format.settings && visit->format.rules) {
		queue_push(visit->format.queue, node);
		queue_push(visit->format.queue, visit->format.settings);
		queue_push(visit->format.queue, visit->format.rules);
	}
}

struct ASTVisit *
ast_visit_new(
	struct AST *node,
	struct ASTVisitTrait *trait)
{
	struct ASTVisit *visit = xmalloc(sizeof(struct ASTVisit));

	visit->format.queue = queue_new();
	visit->visited_nodes.pre = set_new(id_compare);
	visit->visited_nodes.post = set_new(id_compare);
	visit->root = node;
	visit->trait = *trait;

	return visit;
}

void
ast_visit_free(struct ASTVisit *visit)
{
	if (visit) {
		queue_free(visit->format.queue);
		set_free(visit->visited_nodes.pre);
		set_free(visit->visited_nodes.post);
		ast_edit_free(visit->root_ast_edit);
		free(visit);
	}
}

void
ast_visit_reinit(struct ASTVisit *visit)
{
	queue_truncate(visit->format.queue);
	set_truncate(visit->visited_nodes.pre);
	set_truncate(visit->visited_nodes.post);
	visit->state = AST_VISIT_STATE_CONTINUE;

	unless (visit->root_ast_edit) {
		visit->root_ast_edit = ast_edit_new(
			visit->root,
			AST_EDIT_DEFAULT);
	}
}

void
ast_visit_done(struct ASTVisit *visit)
{
	switch (visit->state) {
	case AST_VISIT_STATE_STOP:
	case AST_VISIT_STATE_CONTINUE:
	{
		ast_edit_apply(visit->root_ast_edit);
		visit->root_ast_edit = NULL;
		struct AST *node;
		while ((node = queue_pop(visit->format.queue)) != NULL) {
			struct ASTFormatSettings *settings = queue_pop(visit->format.queue);
			struct Rules *rules = queue_pop(visit->format.queue);
			ast_format(node, rules, settings);
		}
		break;
	}
	case AST_VISIT_STATE_ABORT:
		ast_edit_free(visit->root_ast_edit);
		visit->root_ast_edit = NULL;
		break;
	}
}

void
ast_visit_set_context(
	struct ASTVisit *visit,
	void *context)
{
	visit->context = context;
}

void *
ast_visit_context(struct ASTVisit *visit)
{
	return visit->context;
}

struct ASTEdit *
ast_visit_root_edit(struct ASTVisit *visit)
{
	return visit->root_ast_edit;
}

// Tells VISIT to abort. Same as ast_visit_stop() but the queued up
// edits and formatting requests will be discarded.
void
ast_visit_abort(struct ASTVisit *visit)
{
	visit->state = AST_VISIT_STATE_ABORT;
}

// Tells VISIT to continue. This can be used to overwrite a previous
// ast_vist_stop() or ast_vist_abort().
void
ast_visit_continue(struct ASTVisit *visit)
{
	visit->state = AST_VISIT_STATE_CONTINUE;
}

// Tells VISIT to stop. No more nodes will be visited. Any queued up
// edits and formatting requests will still be applied.
void
ast_visit_stop(struct ASTVisit *visit)
{
	visit->state = AST_VISIT_STATE_STOP;
}

// Skip the visit of NODE. NODE is added to the set of already visited
// nodes. This is useless if you have already processed NODE and don't
// need to do it again. The visit_* methods will not be called for NODE.
void
ast_visit_skip(
	struct ASTVisit *visit,
	struct AST *node)
{
	set_add(visit->visited_nodes.post, node);
	set_add(visit->visited_nodes.pre, node);
}

bool
ast_visit_visited_node_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	return set_contains(visit->visited_nodes.post, node);
}

bool
ast_visit_pre_visited_node_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	return set_contains(visit->visited_nodes.pre, node);
}

struct ASTChildrenIterator *
ast_children_iterator(struct AST *node, ssize_t a, ssize_t b)
{
	struct ASTChildrenIterator *iter = xmalloc(sizeof(struct ASTChildrenIterator));
	iter->node = node;
	size_t len = 0;
	if (node->children.list) {
		iter->entry = list_head(node->children.list);
		len = list_len(node->children.list);
	}
	slice_to_range(len, a, b, &iter->i, &iter->len);

	for (size_t i = 0; iter->entry && i < iter->i; i++) {
		iter->entry = list_entry_next(iter->entry);
	}

	return iter;
}

void
ast_children_iterator_cleanup(struct ASTChildrenIterator **iter_)
{
	struct ASTChildrenIterator *iter = *iter_;
	if (iter != NULL) {
		free(iter);
		*iter_ = NULL;
	}
}

bool
ast_children_iterator_next(
	struct ASTChildrenIterator **iter_,
	size_t *index,
	struct AST **key,
	struct AST **value)
{
	struct ASTChildrenIterator *iter = *iter_;
	if (iter->entry && iter->i < iter->len) {
		*index = iter->i++;
		*key = list_entry_value(iter->entry);
		*value = list_entry_value(iter->entry);
		iter->entry = list_entry_next(iter->entry);
		return true;
	} else {
		ast_children_iterator_cleanup(iter_);
		*iter_ = NULL;
		return false;
	}
}

struct ASTWordsIterator *
ast_words_iterator(struct AST *node, ssize_t a, ssize_t b)
{
	struct ASTWordsIterator *iter = xmalloc(sizeof(struct ASTWordsIterator));
	iter->node = node;
	size_t len = 0;
	if (node->words.list) {
		iter->entry = list_head(node->words.list);
		len = list_len(node->words.list);
	}
	slice_to_range(len, a, b, &iter->i, &iter->len);

	for (size_t i = 0; iter->entry && i < iter->i; i++) {
		iter->entry = list_entry_next(iter->entry);
	}

	return iter;
}

void
ast_words_iterator_cleanup(struct ASTWordsIterator **iter_)
{
	struct ASTWordsIterator *iter = *iter_;
	if (iter != NULL) {
		free(iter);
		*iter_ = NULL;
	}
}

bool
ast_words_iterator_next(
	struct ASTWordsIterator **iter_,
	size_t *index,
	struct ASTWord **key,
	struct ASTWord **value)
{
	struct ASTWordsIterator *iter = *iter_;
	if (iter->entry && iter->i < iter->len) {
		*index = iter->i++;
		*key = list_entry_value(iter->entry);
		*value = list_entry_value(iter->entry);
		iter->entry = list_entry_next(iter->entry);
		return true;
	} else {
		ast_words_iterator_cleanup(iter_);
		*iter_ = NULL;
		return false;
	}
}
