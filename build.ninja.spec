CSTD = gnu11
LDADD += -lm $LDADD_EXECINFO

default-features
	subpackages
if subpackages
	CPPFLAGS += -DPORTFMT_SUBPACKAGES=1
if !subpackages
	CPPFLAGS += -DPORTFMT_SUBPACKAGES=0
if mimalloc
	LDADD += $LDADD_mimalloc
pkg-config
	mimalloc if mimalloc

bundle libias.a
	subdir = $srcdir/libias
	libias/array.c
	libias/compats.c
	libias/diff.c
	libias/diffutil.c
	libias/flow.c
	libias/io.c
	libias/io/mkdirp.c
	libias/list.c
	libias/map.c
	libias/mem.c
	libias/mempool.c
	libias/path.c
	libias/queue.c
	libias/set.c
	libias/stack.c
	libias/str.c
	libias/trait/compare.c
	libias/util.c
	libias/workqueue.c

bundle libportfmt.a
	ast.c
	ast/format.c
	ast/word.c
	ast/word/expand/modifier.c
	$builddir/buildinfo.c
	constants.c
	mainutils.c
	$builddir/enum.c
	parser.c
	parser/astbuilder.c
	parser/astbuilder/tokenizer.c
	parser/astbuilder/word.c
	parser/edits/edit/bump_revision.c
	parser/edits/edit/merge.c
	parser/edits/edit/set_version.c
	parser/edits/lint/bsd_port.c
	parser/edits/lint/clones.c
	parser/edits/lint/commented_portrevision.c
	parser/edits/lint/order.c
	parser/edits/output/conditional_token.c
	parser/edits/output/target_command_token.c
	parser/edits/output/unknown_targets.c
	parser/edits/output/unknown_variables.c
	parser/edits/output/variable_value.c
	parser/edits/refactor/collapse_adjacent_variables.c
	parser/edits/refactor/dedup_tokens.c
	parser/edits/refactor/remove_consecutive_empty_lines.c
	parser/edits/refactor/sanitize_append_modifier.c
	parser/edits/refactor/sanitize_cmake_args.c
	parser/edits/refactor/sanitize_comments.c
	parser/edits/refactor/sanitize_eol_comments.c
	portscan.c
	portscan/log.c
	portscan/status.c
	regexp.c
	rules.c
	sexp.c

gen $builddir/enum.c $srcdir/scripts/enum.awk
	ast.h
	ast/word.h
	ast/word/expand/modifier.h
	mainutils.h
	parser.h
	parser/astbuilder/enum.h
	parser/astbuilder/word.h
	portscan/log.h
	portscan/status.h
	rules.h

# To rebuild buildinfo.c on every commit add a pre-commit hook with
# touch CHANGELOG.md
gen $builddir/buildinfo.c $srcdir/scripts/buildinfo.awk
	CHANGELOG.md

bin portfmt
	LDFLAGS += -pthread
	libias.a
	libportfmt.a
	main.c

install-man
	man/man1/portfmt.1
	man/man1/portfmt-lint.1
	man/man1/portfmt-scan.1

install-script
	scripts/portclippy
	scripts/portedit
	scripts/portscan

tests $builddir/libportfmt.a
	LDADD += $builddir/libias.a
	tests/unit-tests/ast.c

include $builddir/tests.ninja
