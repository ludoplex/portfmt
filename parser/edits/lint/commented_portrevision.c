// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/color.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"

struct LintCommentedPortrevisionState {
	struct Mempool *comments_pool;
	struct Set *comments;
};

// Prototypes
static void lint_commented_portrevision_visit_comment(struct ASTVisit *visit, struct AST *node);

void
lint_commented_portrevision_visit_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct LintCommentedPortrevisionState *this = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);

	AST_WORDS_FOREACH(node, word) {
		const char *comment = str_trim(pool, ast_word_flatten(pool, word));
		if (strlen(comment) <= 1) {
			continue;
		}

		struct ParserSettings settings;
		parser_init_settings(&settings);
		struct Parser *subparser = parser_new(pool, &settings);
		if (parser_read_from_buffer(subparser, comment + 1, strlen(comment) - 1) != PARSER_ERROR_OK) {
			continue;
		}
		if (parser_read_from_buffer(subparser, "\n", 1) != PARSER_ERROR_OK) {
			continue;
		}
		if (parser_read_finish(subparser) != PARSER_ERROR_OK) {
			continue;
		}

		if (parser_lookup_variable(subparser, "PORTEPOCH", PARSER_LOOKUP_FIRST) ||
			parser_lookup_variable(subparser, "PORTREVISION", PARSER_LOOKUP_FIRST)) {
			if (!set_contains(this->comments, comment)) {
				set_add(this->comments, str_dup(this->comments_pool, comment));
			}
		}
	}
}

PARSER_EDIT(lint_commented_portrevision)
{
	struct Mempool *comments_pool = mempool_pool(extpool);
	struct LintCommentedPortrevisionState this = {
		.comments_pool = comments_pool,
		.comments = mempool_set(comments_pool, str_compare),
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_comment = lint_commented_portrevision_visit_comment;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	ast_visit_free(visit);

	struct Set **retval = userdata;
	bool no_color = parser_settings(parser).behavior & PARSER_OUTPUT_NO_COLOR;
	if (retval == NULL && set_len(this.comments) > 0) {
		if (!no_color) {
			parser_enqueue_output(parser, ANSI_COLOR_CYAN);
		}
		parser_enqueue_output(parser, "# Commented PORTEPOCH or PORTREVISION\n");
		if (!no_color) {
			parser_enqueue_output(parser, ANSI_COLOR_RESET);
		}
		SET_FOREACH(this.comments, const char *, comment) {
			parser_enqueue_output(parser, comment);
			parser_enqueue_output(parser, "\n");
		}
	}

	if (retval) {
		*retval = this.comments;
	} else {
		mempool_release(extpool, comments_pool);
	}
}
