// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#include <libias/array.h>
#include <libias/color.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"

struct LintClonesState {
	struct Mempool *pool;
	struct Set *seen;
	struct Set *seen_in_cond;
	struct Set *clones;
	struct Mempool *clones_pool;
};

// Prototypes
static void add_clones(struct LintClonesState *this);
static void lint_clones_visit_variable(struct ASTVisit *visit, struct AST *node);

void
add_clones(struct LintClonesState *this)
{
	SET_FOREACH(this->seen_in_cond, const char *, name) {
		if (set_contains(this->seen, name) && !set_contains(this->clones, name)) {
			set_add(this->clones, str_dup(this->clones_pool, name));
		}
	}
	set_truncate(this->seen_in_cond);
}

void
lint_clones_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct LintClonesState *this = ast_visit_context(visit);

	bool in_conditional =
		ast_parent_if(node)
		|| ast_parent_for(node)
		|| ast_parent_include(node);

	if (ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_ASSIGN) {
		const char *name = ast_word_flatten(this->pool, ast_variable_name(node));
		if (in_conditional) {
			set_add(this->seen_in_cond, name);
		} else if (set_contains(this->seen, name)) {
			if (!set_contains(this->clones, name)) {
				set_add(this->clones, str_dup(this->clones_pool, name));
			}
		} else {
			set_add(this->seen, name);
		}
	}

	unless (in_conditional) {
		add_clones(this);
	}
}

PARSER_EDIT(lint_clones)
{
	SCOPE_MEMPOOL(pool);

	struct Set **clones_ret = userdata;
	bool no_color = parser_settings(parser).behavior & PARSER_OUTPUT_NO_COLOR;

	struct Mempool *clones_pool = mempool_pool(extpool);
	struct LintClonesState this = {
		.pool = pool,
		.seen = mempool_set(pool, str_compare),
		.seen_in_cond = mempool_set(pool, str_compare),
		.clones_pool = clones_pool,
		.clones = mempool_set(clones_pool, str_compare),
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_variable = lint_clones_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	ast_visit_free(visit);

	add_clones(&this);

	if (clones_ret == NULL && set_len(this.clones) > 0) {
		if (!no_color) {
			parser_enqueue_output(parser, ANSI_COLOR_CYAN);
		}
		parser_enqueue_output(parser, "# Variables set twice or more\n");
		if (!no_color) {
			parser_enqueue_output(parser, ANSI_COLOR_RESET);
		}
		SET_FOREACH(this.clones, const char *, name) {
			parser_enqueue_output(parser, name);
			parser_enqueue_output(parser, "\n");
		}
	}

	if (clones_ret) {
		*clones_ret = this.clones;
	} else {
		mempool_release(extpool, clones_pool);
	}
}
