// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"
#include "rules.h"

struct SanitizeAppendModifierState {
	struct Set *seen;
};

// Prototypes
static void refactor_sanitize_append_modifier_visit_include(struct ASTVisit *visit, struct AST *node);
static void refactor_sanitize_append_modifier_visit_variable(struct ASTVisit *visit, struct AST *node);

void
refactor_sanitize_append_modifier_visit_include(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (ast_ports_framework_include_p(node)) {
		ast_visit_stop(visit);
	}
}

void
refactor_sanitize_append_modifier_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct SanitizeAppendModifierState *this = ast_visit_context(visit);
	struct ASTWord *name = ast_variable_name(node);
	if (set_contains(this->seen, name)) {
		if (ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_APPEND) {
			ast_visit_format(visit, node);
		} else {
			set_remove(this->seen, name);
		}
	} else {
		set_add(this->seen, name);
		if (ast_word_strcmp(name, "CXXFLAGS") != 0 &&
			ast_word_strcmp(name, "CFLAGS") != 0 &&
			ast_word_strcmp(name, "LDFLAGS") != 0 &&
			ast_word_strcmp(name, "RUSTFLAGS") != 0 &&
			ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_APPEND) {
			const struct AST *parent = ast_parent(node);
			if (!ast_if_p(parent) && !ast_if_branch_p(parent) && !ast_for_p(parent)) {
				struct ASTEdit *ast_edit = ast_edit_new(node, AST_EDIT_DEFAULT);
				ast_edit_set_variable_modifier(ast_edit, AST_VARIABLE_MODIFIER_ASSIGN);
				ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);
			}
			ast_visit_format(visit, node);
		}
	}
}

PARSER_EDIT(refactor_sanitize_append_modifier)
{
	SCOPE_MEMPOOL(pool);

	if (userdata != NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, NULL);
		return;
	}

	// Sanitize += before bsd.options.mk

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_include = refactor_sanitize_append_modifier_visit_include;
	visit_trait.visit_pre_variable = refactor_sanitize_append_modifier_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_format_settings(visit, rules, format_settings);
	ast_visit_set_context(
		visit,
		&(struct SanitizeAppendModifierState){
			.seen = mempool_set(pool, ast_word_compare),
		});
	ast_visit_run(visit);
	ast_visit_free(visit);
}
