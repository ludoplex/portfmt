// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"
#include "rules.h"

enum DedupAction {
	DEFAULT,
	USES,
};

// Prototypes
static void refactor_dedup_tokens_visit_variable(struct ASTVisit *visit, struct AST *node);

void
refactor_dedup_tokens_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct Rules *rules = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);

	const char *varname = ast_word_flatten(pool, ast_variable_name(node));
	if (rules_skip_dedup_p(rules, varname, ast_variable_modifier(node))) {
		return;
	} else {
		struct Set *seen = mempool_set(pool, str_compare);
		struct Set *uses = mempool_set(pool, str_compare);
		enum DedupAction action = DEFAULT;
		char *helper = NULL;
		if (rules_parse_variable_options_helper(rules, varname, pool, NULL, &helper, NULL)) {
			if (strcmp(helper, "USES") == 0 || strcmp(helper, "USES_OFF") == 0) {
				action = USES;
			}
		} else if (strcmp(varname, "USES") == 0) {
			action = USES;
		}
		struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
		AST_WORDS_FOREACH(node, word) {
			if (ast_word_comment_p(word)) {
				continue;
			}
			const char *value = ast_word_flatten(pool, word);
			// XXX: Handle *_DEPENDS (turn 'RUN_DEPENDS=foo>=1.5.6:misc/foo foo>0:misc/foo'
			// into 'RUN_DEPENDS=foo>=1.5.6:misc/foo')?
			switch (action) {
			case USES: {
				char *buf = str_dup(pool, value);
				char *args = strchr(buf, ':');
				if (args) {
					*args = 0;
				}
				// We follow the semantics of the ports framework.
				// 'USES=compiler:c++11-lang compiler:c++14-lang' is
				// semantically equivalent to just USES=compiler:c++11-lang
				// since compiler_ARGS has already been set once before.
				// As such compiler:c++14-lang can be dropped entirely.
				if (set_contains(uses, buf)) {
					ast_words_edit_remove_word(edit, word);
				} else {
					set_add(uses, buf);
					set_add(seen, value);
				}
				break;
			} default:
				if (set_contains(seen, value)) {
					ast_words_edit_remove_word(edit, word);
				} else {
					set_add(seen, value);
				}
				break;
			}
		}
		ast_words_edit_apply(edit);
	}
}

PARSER_EDIT(refactor_dedup_tokens)
{
	if (userdata != NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, NULL);
		return;
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_variable = refactor_dedup_tokens_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, rules);
	ast_visit_run(visit);
	ast_visit_free(visit);
}
