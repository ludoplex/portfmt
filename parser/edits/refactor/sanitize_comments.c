// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"

// Prototypes
static void refactor_sanitize_comments_visit_target_command(struct ASTVisit *visit, struct AST *node);

void
refactor_sanitize_comments_visit_target_command(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTEdit *ast_edit = ast_visit_root_edit(visit);
	SCOPE_MEMPOOL(pool);

	struct ASTWord *node_comment = ast_node_comment(node);
	if (
		node_comment
		&& ast_words_no_meta_len(node) == 0
		)
	{
		struct AST *comment = ast_new_comment();
		struct ASTWordsEdit *edit = ast_words_edit_new(
			comment,
			AST_WORDS_EDIT_DEFAULT);
		struct ASTWord *comment_word = ast_word_new_string(
			str_printf(pool, "#%s", ast_word_value(node_comment)),
			0);
		ast_words_edit_append_word(edit, comment_word);
		ast_words_edit_apply(edit);

		ast_edit_replace_child(ast_edit, node, comment);
	}
}

PARSER_EDIT(refactor_sanitize_comments)
{
	if (userdata != NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, NULL);
		return;
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_target_command = refactor_sanitize_comments_visit_target_command;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_run(visit);
	ast_visit_free(visit);
}
