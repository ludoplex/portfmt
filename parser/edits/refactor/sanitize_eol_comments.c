// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"
#include "rules.h"

// Prototypes
static bool preserve_eol_comment(const char *word);
static void refactor_sanitize_eol_comments_visit_variable(struct ASTVisit *visit, struct AST *node);

bool
preserve_eol_comment(const char *word)
{
	SCOPE_MEMPOOL(pool);

	unless (*word) {
		return true;
	}

	/* Remove all whitespace from the comment first to cover more cases */
	char *token = mempool_alloc(pool, strlen(word) + 1);
	const char *datap = word;
	for (char *tokenp = token; *datap != 0; datap++) {
		if (!isspace((unsigned char)*datap)) {
			*tokenp++ = *datap;
		}
	}
	return strcmp(token, "#") == 0 || strcmp(token, "#empty") == 0 || strcmp(token, "#none") == 0;
}

void
refactor_sanitize_eol_comments_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTEdit *root_ast_edit = ast_visit_root_edit(visit);
	SCOPE_MEMPOOL(pool);

	/* Try to push end of line comments out of the way above
	 * the variable as a way to preserve them.  They clash badly
	 * with sorting tokens in variables.  We could add more
	 * special cases for this, but often having them at the top
	 * is just as good.
	 */
	struct ASTWord *var_comment_word = ast_node_comment(node);
	const char *var_comment = str_printf(pool, "#%s", ast_word_value(var_comment_word));
	if (preserve_eol_comment(var_comment)) {
		return;
	}

	struct AST *comment = ast_new_comment();
	struct ASTWordsEdit *edit = ast_words_edit_new(comment, AST_WORDS_EDIT_DEFAULT);
	ast_words_edit_append_word(edit, ast_word_new_string(var_comment, 0));
	ast_words_edit_apply(edit);

	edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
	ast_words_edit_remove_word(edit, var_comment_word);
	ast_words_edit_apply(edit);

	ast_visit_format(visit, comment);
	ast_visit_format(visit, node);

	struct ASTEdit *ast_edit = ast_edit_new(
		ast_parent(node),
		AST_EDIT_DEFAULT);
	ast_edit_add_child_before(ast_edit, comment, node);
	ast_edit_chain_apply(root_ast_edit, ast_edit);
}

PARSER_EDIT(refactor_sanitize_eol_comments)
{
	if (userdata != NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, NULL);
		return;
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_variable = refactor_sanitize_eol_comments_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_format_settings(visit, rules, format_settings);
	ast_visit_run(visit);
	ast_visit_free(visit);
}
