// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/trait/compare.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"
#include "rules.h"

// Prototypes
static bool candidate_p(struct ASTWord *group_name, struct AST *node);
static void refactor_collapse_adjacent_variables_visit_variable(struct ASTVisit *visit, struct AST *first);

bool
candidate_p(
	struct ASTWord *group_name,
	struct AST *node)
{
	unless (ast_variable_p(node)) {
		return false;
	}

	if (ast_node_comment(node)) {
		return false;
	}

	struct ASTWord *node_name = ast_variable_name(node);
	if (ast_word_compare->compare(&group_name, &node_name, NULL) != 0) {
		return false;
	}

	switch (ast_variable_modifier(node)) {
	case AST_VARIABLE_MODIFIER_APPEND:
	case AST_VARIABLE_MODIFIER_ASSIGN:
		return true;
	default:
		return false;
	}
}

void
refactor_collapse_adjacent_variables_visit_variable(
	struct ASTVisit *visit,
	struct AST *first)
{
	SCOPE_MEMPOOL(pool);
	struct ASTWord *group_name = ast_variable_name(first);

	if (!candidate_p(group_name, first)) {
		return;
	}

	struct Array *group = mempool_array(pool);
	struct AST *sibling = ast_next_sibling(first);
	while (sibling) {
		if (candidate_p(group_name, sibling)) {
			array_append(group, sibling);
			ast_visit_skip(visit, sibling);
			sibling = ast_next_sibling(sibling);
		} else {
			break;
		}
	}

	// Merge the variables into FIRST

	if (array_len(group) == 0) {
		return;
	}

	struct ASTWordsEdit *first_edit = ast_words_edit_new(first, AST_WORDS_EDIT_DEFAULT);
	ARRAY_FOREACH(group, struct AST *, node) {
		struct ASTWordsEdit *node_edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
		AST_WORDS_FOREACH(node, word) {
			ast_words_edit_append_word(first_edit, ast_word_clone(word));
			ast_words_edit_remove_word(node_edit, word);
		}
		ast_words_edit_apply(node_edit);
	}
	ast_words_edit_apply(first_edit);

	// The words of each variable in GROUP were added to the first
	// variable. Remove the superfluous variable nodes now.
	ARRAY_FOREACH(group, struct AST *, node) {
		ast_edit_remove_child(ast_visit_root_edit(visit), node);
	}

	// Schedule a reformat of the first variable. It was changed and if
	// we didn't do this we'd probably have to be more careful with
	// adding whitespace words to the variable above.
	ast_visit_format(visit, first);
}

PARSER_EDIT(refactor_collapse_adjacent_variables)
{
	if (userdata != NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, NULL);
		return;
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_variable = refactor_collapse_adjacent_variables_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_format_settings(visit, rules, format_settings);
	ast_visit_run(visit);
	ast_visit_free(visit);
}
