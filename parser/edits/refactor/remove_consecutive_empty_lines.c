// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <ctype.h>
#include <stdio.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"

struct RemoveConsecutiveEmptyLinesState {
	size_t counter;
};

// Prototypes
static void refactor_remove_consecutive_empty_lines_visit_comment(struct ASTVisit *visit, struct AST *node);
static void refactor_remove_consecutive_empty_lines_visit_other(struct ASTVisit *visit, struct AST *node);
static void refactor_remove_consecutive_empty_lines_visit_root(struct ASTVisit *visit, struct AST *node);

void
refactor_remove_consecutive_empty_lines_visit_root(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct RemoveConsecutiveEmptyLinesState *this = ast_visit_context(visit);
	// Clear initial empty lines
	this->counter = 1;
}

void
refactor_remove_consecutive_empty_lines_visit_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct RemoveConsecutiveEmptyLinesState *this = ast_visit_context(visit);
	struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
	AST_WORDS_FOREACH(node, line) {
		if (ast_word_whitespace_p(line)) {
			if (this->counter++ > 0) {
				ast_words_edit_remove_word(edit, line);
			}
		} else {
			this->counter = 0;
		}
	}
	ast_words_edit_apply(edit);

	// There's no need to reformat the edited node. Formatting a
	// comment does nothing at the moment.
}

void
refactor_remove_consecutive_empty_lines_visit_other(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct RemoveConsecutiveEmptyLinesState *this = ast_visit_context(visit);
	this->counter = 0;
}

PARSER_EDIT(refactor_remove_consecutive_empty_lines)
{
	if (userdata != NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, NULL);
		return;
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.default_visit_fn = refactor_remove_consecutive_empty_lines_visit_other;
	visit_trait.visit_pre_root = refactor_remove_consecutive_empty_lines_visit_root;
	visit_trait.visit_comment = refactor_remove_consecutive_empty_lines_visit_comment;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(
		visit,
		&(struct RemoveConsecutiveEmptyLinesState){
			.counter = 0,
		});
	ast_visit_run(visit);
	ast_visit_free(visit);
}
