// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"
#include "rules.h"

enum State {
	NONE,
	CMAKE_ARGS,
	CMAKE_D,
};

// Prototypes
static void refactor_sanitize_cmake_args_visit_variable(struct ASTVisit *visit, struct AST *node);

void
refactor_sanitize_cmake_args_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct Rules *rules = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);

	char *helper = NULL;
	enum State state = NONE;
	const char *varname = ast_word_flatten(pool, ast_variable_name(node));
	if (rules_parse_variable_options_helper(rules, varname, pool, NULL, &helper, NULL)) {
		if (strcmp(helper, "CMAKE_ON") == 0 || strcmp(helper, "CMAKE_OFF") == 0 ||
			strcmp(helper, "MESON_ON") == 0 || strcmp(helper, "MESON_OFF") == 0) {
			state = CMAKE_ARGS;
		}
	} else if (strcmp(varname, "CMAKE_ARGS") == 0 || strcmp(varname, "MESON_ARGS") == 0) {
		state = CMAKE_ARGS;
	}
	size_t len = ast_words_no_meta_len(node);
	struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
	AST_WORDS_FOREACH(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		}

		if (state == CMAKE_ARGS && ast_word_strcmp(word, "-D") == 0) {
			state = CMAKE_D;
			unless (word_index + 1 == len) {
				ast_words_edit_remove_word(edit, word);
			}
		} else if (state == CMAKE_D) {
			struct ASTWord *wrapper = ast_word_wrap(
				ast_word_new_string("-D", ast_word_pos(word)));
			ast_word_add_child(wrapper, ast_word_clone(word));
			ast_words_edit_add_word(edit, wrapper, word);
			ast_words_edit_remove_word(edit, word);

			state = CMAKE_ARGS;
		}
	}
	ast_words_edit_apply(edit);
}

PARSER_EDIT(refactor_sanitize_cmake_args)
{
	if (userdata != NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, NULL);
		return;
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_variable = refactor_sanitize_cmake_args_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, rules);
	ast_visit_run(visit);
	ast_visit_free(visit);
}
