// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "libias/io.h"
#include "parser.h"
#include "parser/edits.h"

// Prototypes
static ssize_t extract_git_describe_prefix(const char *ver);
static ssize_t extract_git_describe_suffix(const char *ver);
static bool is_git_describe_version(struct Mempool *pool, const char *ver, char **distversion, char **prefix, char **suffix);

ssize_t
extract_git_describe_suffix(const char *ver)
{
	if (strlen(ver) == 0) {
		return -1;
	}

	bool gflag = false;
	for (size_t i = strlen(ver) - 1; i != 0; i--) {
		switch (ver[i]) {
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
			break;
		case 'g':
			gflag = true;
			break;
		case '-':
			if (gflag) {
				return i;
			} else {
				return -1;
			}
		default:
			if (!isdigit((unsigned char)ver[i])) {
				return -1;
			}
		}
	}

	return -1;
}

ssize_t
extract_git_describe_prefix(const char *ver)
{
	if (*ver == 0 || isdigit((unsigned char)*ver)) {
		return -1;
	}

	for (size_t i = 0; i < strlen(ver); i++) {
		if (i > 0 && isdigit((unsigned char)ver[i])) {
			return i - 1;
		}
	}

	return -1;
}

bool
is_git_describe_version(struct Mempool *pool, const char *ver, char **distversion, char **prefix, char **suffix)
{
	ssize_t suffix_index;
	if ((suffix_index = extract_git_describe_suffix(ver)) == -1) {
		if (distversion != NULL) {
			*distversion = NULL;
		}
		if (prefix != NULL) {
			*prefix = NULL;
		}
		if (suffix != NULL) {
			*suffix = NULL;
		}
		return false;
	}

	ssize_t prefix_index;
	if ((prefix_index = extract_git_describe_prefix(ver)) != -1) {
		if (prefix != NULL) {
			*prefix = str_ndup(pool, ver, prefix_index + 1);
		}

	} else {
		if (prefix != NULL) {
			*prefix = NULL;
		}
	}

	if (suffix != NULL) {
		*suffix = str_dup(pool, ver + suffix_index);
	}

	if (distversion != NULL) {
		*distversion = str_slice(pool, ver, prefix_index + 1, suffix_index);
	}

	return true;
}

PARSER_EDIT(edit_set_version)
{
	SCOPE_MEMPOOL(pool);

	const struct ParserEdit *params = userdata;
	if (params == NULL ||
	    params->subparser != NULL ||
	    params->arg1 == NULL ||
	    params->merge_behavior != PARSER_MERGE_DEFAULT) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, "missing version");
		return;
	}

	const char *newversion = str_dup(pool, params->arg1);

	const char *ver = "DISTVERSION";
	if (parser_lookup_variable(parser, "PORTVERSION", PARSER_LOOKUP_FIRST)) {
		ver = "PORTVERSION";
	}

	uint32_t rev = 0;
	bool rev_opt = false;
	struct AST *node = parser_lookup_variable(parser, ver, PARSER_LOOKUP_FIRST);
	if (node) {
		const char *version = ast_words_no_meta_flatten(node, pool, " ");
		struct AST *rev_var;
		if (strcmp(version, newversion) != 0 &&
		    (rev_var = parser_lookup_variable(parser, "PORTREVISION", PARSER_LOOKUP_FIRST))) {
			const char *revision = ast_words_no_meta_flatten(rev_var, pool, " ");
			rev_opt = ast_variable_modifier(rev_var) == AST_VARIABLE_MODIFIER_OPTIONAL;
			const char *errstr = NULL;
			rev = strtonum(revision, 0, INT_MAX, &errstr);
			if (errstr != NULL) {
				parser_set_error(parser, PARSER_ERROR_EXPECTED_INT, errstr);
				return;
			}
		}
	}

	bool remove_distversionprefix = false;
	bool remove_distversionsuffix = false;
	char *distversion;
	char *prefix;
	char *suffix;
	if (!is_git_describe_version(pool, newversion, &distversion, &prefix, &suffix)) {
		struct AST *node = parser_lookup_variable(parser, "DISTVERSIONSUFFIX", PARSER_LOOKUP_FIRST);
		if (node) {
			suffix = ast_words_no_meta_flatten(node, pool, " ");
			if (str_endswith(newversion, suffix)) {
				newversion = str_ndup(pool, newversion, strlen(newversion) - strlen(suffix));
			} else {
				remove_distversionsuffix = true;
			}
			suffix = NULL;
		}
		node = parser_lookup_variable(parser, "DISTVERSIONPREFIX", PARSER_LOOKUP_FIRST);
		if (node) {
			prefix  = ast_words_no_meta_flatten(node, pool, " ");
			if (str_startswith(newversion, prefix)) {
				newversion += strlen(prefix);
			}
			prefix = NULL;
		}
	} else if (prefix == NULL) {
		remove_distversionprefix = true;
		ver = "DISTVERSION";
	} else {
		ver = "DISTVERSION";
	}

	struct ParserSettings settings = parser_settings(parser);
	struct Parser *subparser = parser_new(pool, &settings);

	struct File *script = file_open_memstream(pool);
	if (suffix) {
		file_printf(script, "DISTVERSIONSUFFIX=%s\n", suffix);
	} else if (remove_distversionsuffix) {
		file_puts(script, "DISTVERSIONSUFFIX!=\n");
	}

	if (prefix) {
		file_printf(script, "DISTVERSIONPREFIX=%s\n", prefix);
	} else if (remove_distversionprefix) {
		file_puts(script, "DISTVERSIONPREFIX!=\n");
	}

	if (strcmp(ver, "DISTVERSION") == 0) {
		file_puts(script, "PORTVERSION!=\n");
	}

	if (distversion) {
		newversion = distversion;
	}

	if (rev > 0) {
		if (rev_opt) {
			// Reset PORTREVISION?= to 0
			file_printf(script, "%s=%s\nPORTREVISION=0\n", ver, newversion);
		} else {
			// Remove PORTREVISION
			file_printf(script, "%s=%s\nPORTREVISION!=\n", ver, newversion);
		}
	} else {
		file_printf(script, "%s=%s\n", ver, newversion);
	}

	size_t script_len;
	char *script_buf = file_slurp(script, pool, &script_len);
	enum ParserError error = parser_read_from_buffer(subparser, script_buf, script_len);
	if (error != PARSER_ERROR_OK) {
		return;
	}
	error = parser_read_finish(subparser);
	if (error != PARSER_ERROR_OK) {
		return;
	}
	parser_merge(parser, subparser, params->merge_behavior | PARSER_MERGE_SHELL_IS_DELETE);
}
