// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"

struct WalkerData {
	struct Parser *parser;
	struct Mempool *pool;
	struct ParserEditOutput *param;
};

// Prototypes
static void output_conditional_token_visit_conditional(struct ASTVisit *visit, struct AST *node);

void
output_conditional_token_visit_conditional(
	struct ASTVisit *visit,
	struct AST *node)
{
	SCOPE_MEMPOOL(pool);
	struct WalkerData *this = ast_visit_context(visit);

	AST_WORDS_FOREACH(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		}
		const char *value = ast_word_flatten(pool, word);
		if (
			this->param->filter == NULL
			|| this->param->filter(this->parser, value, this->param->filteruserdata)
			)
		{
			this->param->found = true;
			if (this->param->callback) {
				this->param->callback(this->pool, value, value, NULL, this->param->callbackuserdata);
			}
		}
	}
}

PARSER_EDIT(output_conditional_token)
{
	struct ParserEditOutput *param = userdata;
	if (param == NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, "missing parameter");
		return;
	}

	param->found = false;

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_expr = output_conditional_token_visit_conditional;
	visit_trait.visit_for_bindings = output_conditional_token_visit_conditional;
	visit_trait.visit_for_words = output_conditional_token_visit_conditional;
	visit_trait.visit_include = output_conditional_token_visit_conditional;
	visit_trait.visit_if_branch = output_conditional_token_visit_conditional;

	struct WalkerData this = {
		.parser = parser,
		.pool = extpool,
		.param = param,
	};

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	ast_visit_free(visit);
}
