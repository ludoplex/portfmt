// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/edits.h"
#include "rules.h"

struct WalkerData {
	struct Parser *parser;
	struct Rules *rules;
	struct Mempool *pool;
	struct ParserEditOutput *param;
	struct Set *targets;
	struct Set *deps;
	struct Set *post_plist_targets;
};

// Prototypes
static void check_target(struct WalkerData *this, const char *name, bool deps);
static void output_unknown_targets_visit_target_rule(struct ASTVisit *visit, struct AST *node);

void
check_target(struct WalkerData *this, const char *name, bool deps)
{
	if (deps) {
		if (rules_special_source_p(this->rules, name)) {
			return;
		}
		if (rules_known_target_p(this->rules, name)) {
			return;
		}
		if (set_contains(this->targets, name)) {
			return;
		}
		if (set_contains(this->post_plist_targets, name)) {
			return;
		}
	} else {
		if (rules_special_target_p(this->rules, name)) {
			return;
		}
		if (rules_known_target_p(this->rules, name)) {
			return;
		}
		if (set_contains(this->deps, name)) {
			return;
		}
		if (set_contains(this->post_plist_targets, name)) {
			return;
		}
	}
	if ((this->param->keyfilter == NULL || this->param->keyfilter(this->parser, name, this->param->keyuserdata))) {
		this->param->found = true;
		if (this->param->callback) {
			// XXX: provide option as hint for opthelper targets?
			this->param->callback(this->pool, name, name, NULL, this->param->callbackuserdata);
		}
	}
}

void
output_unknown_targets_visit_target_rule(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct WalkerData *this = ast_visit_context(visit);
	bool skip_deps = false;
	struct AST *targets = ast_get_child(
		node,
		AST_CHILD_TARGET_RULE_TARGETS);
	AST_WORDS_FOREACH(targets, name) {
		if (ast_word_meta_p(name)) {
			continue;
		}
		const char *source = ast_word_flatten(this->pool, name);
		if (rules_special_target_p(this->rules, source)) {
			skip_deps = true;
		}
		set_add(this->targets, source);
	}
	unless (skip_deps) {
		struct AST *dependencies = ast_get_child(
			node,
			AST_CHILD_TARGET_RULE_DEPENDENCIES);
		AST_WORDS_FOREACH(dependencies, name) {
			if (ast_word_meta_p(name)) {
				continue;
			}
			set_add(this->deps, ast_word_flatten(this->pool, name));
		}
	}
}

PARSER_EDIT(output_unknown_targets)
{
	SCOPE_MEMPOOL(pool);

	struct ParserEditOutput *param = userdata;
	if (param == NULL) {
		parser_set_error(parser, PARSER_ERROR_INVALID_ARGUMENT, "missing parameter");
		return;
	}

	param->found = true;
	struct WalkerData this = {
		.parser = parser,
		.rules = rules,
		.pool = extpool,
		.param = param,
		.targets = mempool_set(pool, str_compare),
		.deps = mempool_set(pool, str_compare),
		.post_plist_targets = parser_metadata(parser, PARSER_METADATA_POST_PLIST_TARGETS),
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_target_rule = output_unknown_targets_visit_target_rule;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	ast_visit_free(visit);

	SET_FOREACH(this.targets, const char *, name) {
		check_target(&this, name, false);
	}
	SET_FOREACH(this.deps, const char *, name) {
		check_target(&this, name, true);
	}
}
