// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ParserASTBuilderTokenType {
	PARSER_AST_BUILDER_TOKEN_COMMENT,				// human:"comment"
	PARSER_AST_BUILDER_TOKEN_CONDITIONAL_END,		// human:"conditional-end"
	PARSER_AST_BUILDER_TOKEN_CONDITIONAL_EXPR,		// human:"conditional-expr"
	PARSER_AST_BUILDER_TOKEN_CONDITIONAL_POSIX,		// human:"conditional-posix"
	PARSER_AST_BUILDER_TOKEN_CONDITIONAL_TOKEN,		// human:"conditional-token"
	PARSER_AST_BUILDER_TOKEN_CONDITIONAL_START,		// human:"conditional-start"
	PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_END,	// human:"target-command-end"
	PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_START,	// human:"target-command-start"
	PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_FLAGS,	// human:"target-command-flags"
	PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_TOKEN,	// human:"target-command-token"
	PARSER_AST_BUILDER_TOKEN_TARGET_RULE_END,		// human:"target-rule-end"
	PARSER_AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR,  // human:"target-rule-operator"
	PARSER_AST_BUILDER_TOKEN_TARGET_RULE_START,		// human:"target-rule-start"
	PARSER_AST_BUILDER_TOKEN_TARGET_RULE_TOKEN,		// human:"target-rule-token"
	PARSER_AST_BUILDER_TOKEN_VARIABLE_END,			// human:"variable-end"
	PARSER_AST_BUILDER_TOKEN_VARIABLE_START,		// human:"variable-start"
	PARSER_AST_BUILDER_TOKEN_VARIABLE_MODIFIER,		// human:"variable-modifier"
	PARSER_AST_BUILDER_TOKEN_VARIABLE_TOKEN,		// human:"variable-token"
	PARSER_AST_BUILDER_TOKEN_WS,					// human:"whitespace"
};

libias_attr_returns_nonnull
const char *ParserASTBuilderTokenType_human(enum ParserASTBuilderTokenType);

libias_attr_returns_nonnull
const char *ParserASTBuilderTokenType_tostring(enum ParserASTBuilderTokenType);

enum ParserASTBuilderConditionalType {
	PARSER_AST_BUILDER_CONDITIONAL_INVALID,						// human:"<invalid>" bmake:"<invalid>"
	PARSER_AST_BUILDER_CONDITIONAL_ELIF,						// human:".elif" bmake:"elif"
	PARSER_AST_BUILDER_CONDITIONAL_ELIFDEF,						// human:".elifdef" bmake:"elifdef"
	PARSER_AST_BUILDER_CONDITIONAL_ELIFNDEF,					// human:".elifndef" bmake:"elifndef"
	PARSER_AST_BUILDER_CONDITIONAL_ELIFMAKE,					// human:".elifmake" bmake:"elifmake"
	PARSER_AST_BUILDER_CONDITIONAL_ELIFNMAKE,					// human:".elifnmake" bmake:"elifnmake"
	PARSER_AST_BUILDER_CONDITIONAL_ELSE,						// human:".else" bmake:"else"
	PARSER_AST_BUILDER_CONDITIONAL_ENDFOR,						// human:".endfor" bmake:"endfor"
	PARSER_AST_BUILDER_CONDITIONAL_ENDIF,						// human:".endif" bmake:"endif"
	PARSER_AST_BUILDER_CONDITIONAL_ERROR,						// human:".error" bmake:"error"
 	PARSER_AST_BUILDER_CONDITIONAL_EXPORT_ENV,					// human:".export-env" bmake:"export-env"
	PARSER_AST_BUILDER_CONDITIONAL_EXPORT_ENV_DOT,				// human:".export.env" bmake:"export.env"
	PARSER_AST_BUILDER_CONDITIONAL_EXPORT_LITERAL,				// human:".export-literal" bmake:"export-literal"
	PARSER_AST_BUILDER_CONDITIONAL_EXPORT,						// human:".export" bmake:"export"
	PARSER_AST_BUILDER_CONDITIONAL_FOR,							// human:".for" bmake:"for"
	PARSER_AST_BUILDER_CONDITIONAL_IF,							// human:".if" bmake:"if"
	PARSER_AST_BUILDER_CONDITIONAL_IFDEF,						// human:".ifdef" bmake:"ifdef"
	PARSER_AST_BUILDER_CONDITIONAL_IFMAKE,						// human:".ifmake" bmake:"ifmake"
	PARSER_AST_BUILDER_CONDITIONAL_IFNDEF,						// human:".ifndef" bmake:"ifndef"
	PARSER_AST_BUILDER_CONDITIONAL_IFNMAKE,						// human:".ifnmake" bmake:"ifnmake"
	PARSER_AST_BUILDER_CONDITIONAL_INCLUDE,						// human:".include" bmake:"include"
	PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL,			// human:".-include" bmake:"-include"
	PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_D,			// human:".dinclude" bmake:"dinclude"
	PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S,			// human:".sinclude" bmake:"sinclude"
	PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX,				// human:"include" bmake:"include"
	PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL,		// human:"-include" bmake:"-include"
	PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S,	// human:"sinclude" bmake:"sinclude"
	PARSER_AST_BUILDER_CONDITIONAL_INFO,						// human:".info" bmake:"info"
	PARSER_AST_BUILDER_CONDITIONAL_UNDEF,						// human:".undef" bmake:"undef"
	PARSER_AST_BUILDER_CONDITIONAL_UNEXPORT_ENV,				// human:".unexport-env" bmake:"unexport-env"
	PARSER_AST_BUILDER_CONDITIONAL_UNEXPORT,					// human:".unexport" bmake:"unexport"
	PARSER_AST_BUILDER_CONDITIONAL_WARNING,						// human:".warning" bmake:"warning"
};

libias_attr_nonnull(1)
bool ParserASTBuilderConditionalType_from_bmake(const char *, const size_t, enum ParserASTBuilderConditionalType *);

libias_attr_returns_nonnull
const char *ParserASTBuilderConditionalType_human(enum ParserASTBuilderConditionalType);

libias_attr_returns_nonnull
const char *ParserASTBuilderConditionalType_tostring(enum ParserASTBuilderConditionalType);
