// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/io.h>
#include <libias/mem.h>
#include <libias/mempool.h>
#include <libias/stack.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "parser.h"
#include "parser/astbuilder.h"
#include "parser/astbuilder/enum.h"
#include "parser/astbuilder/tokenizer.h"
#include "parser/astbuilder/word.h"
#include "rules.h"

struct ParserASTBuilder {
	bool finished;
	struct Mempool *pool;
	struct Parser *parser;
	struct AST *root;
	struct Array *comments;
	struct Array *tokens;
	struct Stack *forstack;
	struct Stack *ifstack;
	struct Stack *nodestack;
	struct File *only_whitespace;

	const char *buf;
	size_t buflen;
};

// Prototypes
static bool ParserASTBuilderConditionalType_to_ASTExprType(enum ParserASTBuilderConditionalType, enum ASTExprType *);
static bool ParserASTBuilderConditionalType_to_ASTIncludeType(enum ParserASTBuilderConditionalType, enum ASTIncludeType *);
static bool ParserASTBuilderConditionalType_to_ASTIfType(enum ParserASTBuilderConditionalType, enum ASTIfType *);
static struct ASTWord *create_ast_words(struct Mempool *, struct ParserASTBuilder *, ssize_t, ssize_t, struct ASTWordsEdit *, size_t, bool);
static void flush_comments(struct ParserASTBuilder *, struct AST *);
static void flush_trailing_whitespace(struct ParserASTBuilder *, struct ASTWordsEdit *, const size_t);
struct AST *unwind_until_node(struct Stack *, struct AST *);

bool
ParserASTBuilderConditionalType_to_ASTExprType(enum ParserASTBuilderConditionalType value, enum ASTExprType *retval)
{
	switch (value) {
	case PARSER_AST_BUILDER_CONDITIONAL_ERROR:
		*retval = AST_EXPR_ERROR;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_EXPORT_ENV:
		*retval = AST_EXPR_EXPORT_ENV;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_EXPORT_ENV_DOT:
		*retval = AST_EXPR_EXPORT_ENV_DOT;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_EXPORT_LITERAL:
		*retval = AST_EXPR_EXPORT_LITERAL;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_EXPORT:
		*retval = AST_EXPR_EXPORT;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_INFO:
		*retval = AST_EXPR_INFO;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_UNDEF:
		*retval = AST_EXPR_UNDEF;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_UNEXPORT_ENV:
		*retval = AST_EXPR_UNEXPORT_ENV;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_UNEXPORT:
		*retval = AST_EXPR_UNEXPORT;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_WARNING:
		*retval = AST_EXPR_WARNING;
		return true;
	default:
		return false;
	}
}

bool
ParserASTBuilderConditionalType_to_ASTIncludeType(enum ParserASTBuilderConditionalType value, enum ASTIncludeType *retval)
{
	switch (value) {
	case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE:
		*retval = AST_INCLUDE_BMAKE;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL:
		*retval = AST_INCLUDE_OPTIONAL;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_D:
		*retval = AST_INCLUDE_OPTIONAL_D;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S:
		*retval = AST_INCLUDE_OPTIONAL_S;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX:
		*retval = AST_INCLUDE_POSIX;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL:
		*retval = AST_INCLUDE_POSIX_OPTIONAL;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S:
		*retval = AST_INCLUDE_POSIX_OPTIONAL_S;
		return true;
	default:
		return false;
	}
}

bool
ParserASTBuilderConditionalType_to_ASTIfType(enum ParserASTBuilderConditionalType value, enum ASTIfType *retval)
{
	switch (value) {
	case PARSER_AST_BUILDER_CONDITIONAL_IF:
		*retval = AST_IF_IF;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_IFDEF:
		*retval = AST_IF_DEF;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_IFMAKE:
		*retval = AST_IF_MAKE;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_IFNDEF:
		*retval = AST_IF_NDEF;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_IFNMAKE:
		*retval = AST_IF_NMAKE;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_ELIF:
		*retval = AST_IF_IF;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_ELIFDEF:
		*retval = AST_IF_DEF;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_ELIFNDEF:
		*retval = AST_IF_NDEF;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_ELIFMAKE:
		*retval = AST_IF_MAKE;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_ELIFNMAKE:
		*retval = AST_IF_NMAKE;
		return true;
	case PARSER_AST_BUILDER_CONDITIONAL_ELSE:
		*retval = AST_IF_ELSE;
		return true;
	default:
		return false;
	}
}

struct ASTWord *
create_ast_words(
	struct Mempool *pool,
	struct ParserASTBuilder *this,
	ssize_t a,
	ssize_t b,
	struct ASTWordsEdit *edit,
	size_t word_start_pos,
	bool if_expr)
{
	struct ParserASTBuilderToken *comment = NULL;
	ARRAY_FOREACH_SLICE(this->tokens, a, b, struct ParserASTBuilderToken *, t) {
		panic_if(
			t->type == PARSER_AST_BUILDER_TOKEN_WS,
			"whitespace tokens are not allowed here");
		if (t->len == 0) {
			continue;
		}

		if (t->whitespace_before) {
			size_t len = strlen(t->whitespace_before);
			if (len > 0) {
				struct ASTWord *word = ast_word_new_whitespace(
					t->whitespace_before,
					t->i - len);
				ast_words_edit_append_word(edit, word);
			}
		}

		if (str_startswith(this->buf + t->i, "#")) {
			panic_unless(
				t_index + 1 == array_len(this->tokens),
				"comment should be the last token");
			comment = t;
			break;
		} else {
			struct ASTWord *word = parser_astbuilder_parse_word(
				this->buf + t->i,
				t->len,
				t->i,
				if_expr);
			ast_words_edit_append_word(edit, word);
		}
	}

	if (comment) {
		panic_unless(
			comment->len > 0,
			"zero length comment");
		// We remove the # since it's implied for AST_WORD_COMMENT
		struct ASTWord *word = ast_word_new_comment(
			str_ndup(pool, this->buf + comment->i + 1, comment->len -1 ),
			comment->i + 1);
		ast_words_edit_append_word(edit, word);
		return word;
	} else {
		return NULL;
	}
}

struct ParserASTBuilder *
parser_astbuilder_new(
	struct Parser *parser,
	const char *buf,
	const size_t buflen)
{
	struct ParserASTBuilder *builder = xmalloc(sizeof(struct ParserASTBuilder));
	builder->parser = parser;
	builder->pool = mempool_new();
	builder->comments = mempool_array(builder->pool);
	builder->only_whitespace = file_open_memstream(builder->pool);
	builder->tokens = mempool_array(builder->pool);
	builder->forstack = mempool_stack(builder->pool);
	builder->ifstack = mempool_stack(builder->pool);
	builder->nodestack = mempool_stack(builder->pool);
	builder->root = ast_new_root();
	stack_push(builder->nodestack, builder->root);

	builder->buf = buf;
	builder->buflen = buflen;

	return builder;
}

void
parser_astbuilder_free(struct ParserASTBuilder *builder)
{
	if (builder) {
		ast_free(builder->root);
		mempool_free(builder->pool);
		free(builder);
	}
}

void
flush_comments(struct ParserASTBuilder *this, struct AST *parent)
{
	SCOPE_MEMPOOL(pool);

	struct Array *comments = this->comments;

	if (array_len(comments) == 0) {
		return;
	}

	struct AST *node = ast_new_comment();
	struct ASTEdit *ast_edit = ast_edit_new(parent, AST_EDIT_DEFAULT);
	ast_edit_append_child(ast_edit, node);
	ast_edit_apply(ast_edit);

	{
		struct Array *tokens = this->tokens;
		this->tokens = comments;
		this->tokens = tokens;
	}

	struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
	ARRAY_FOREACH(comments, struct ParserASTBuilderToken *, t) {
		size_t len = t->len;
		if (len > 0 && this->buf[t->i + len - 1] == '\n') {
			len--;
		}
		const char *comment = str_ndup(pool, this->buf + t->i, len);
		bool whitespace = true;
		for (size_t i = 0; i < len; i++) {
			unless (isspace((unsigned char)comment[i])) {
				whitespace = false;
				break;
			}
		}
		struct ASTWord *word;
		if (whitespace) {
			word = ast_word_new_whitespace(comment, t->i);
		} else {
			word = ast_word_new_string(comment, t->i);
		}
		ast_words_edit_append_word(edit, word);
	}

	ast_words_edit_apply(edit);

	array_truncate(comments);
}

void
flush_trailing_whitespace(
	struct ParserASTBuilder *this,
	struct ASTWordsEdit *edit,
	const size_t i)
{
	// Record trailing whitespace if any
	if (file_len(this->only_whitespace) > 0) {
		SCOPE_MEMPOOL(pool);
		struct ASTWord *word = ast_word_new_whitespace(
			file_slurp(this->only_whitespace, pool, NULL),
			i);
		ast_words_edit_append_word(edit, word);
		file_truncate(this->only_whitespace, 0);
	}
}

struct AST *
unwind_until_node(struct Stack *nodestack, struct AST *parent)
{
	unless (parent) {
		return NULL;
	}

	// Unwind until we hit parent. Child nodes must now go to the parent's parent.
	struct AST *lastnode = stack_peek(nodestack);
	while (1) {
		struct AST *node = stack_peek(nodestack);
		if (node == NULL) {
			// the stack cannot be left empty; the last node is the
			// root node, so put it back on the stack.
			stack_push(nodestack, lastnode);
			return NULL;
		} else if (node == parent) {
			stack_pop(nodestack);
			return node;
		} else {
			lastnode = stack_pop(nodestack);
		}
	}

	return NULL;
}

#define ERROR(err, ...)													\
	do {																\
		parser_set_error(this->parser, (err),							\
						 str_printf(pool, __VA_ARGS__));				\
		goto error;														\
	} while(0);

bool
parser_astbuilder_process_token(
	struct ParserASTBuilder *this,
	struct ParserASTBuilderToken *t)
{
	panic_if(this->finished, "AST was already built");
	SCOPE_MEMPOOL(pool);
	if (stack_len(this->nodestack) == 0) {
		ERROR(PARSER_ERROR_AST_BUILD_FAILED,
			  "node stack exhausted at byte %zu", t->i);
	}

#if PARSER_ASTBUILDER_DEBUG
	fprintf(
		stderr,
		"%9s %9s %-21s '%s'\n",
		str_printf(pool, "[%zu-%zu]", t->i, t->i + t->len),
		str_printf(pool, "[%zu-%zu]", t->lines.a, t->lines.b),
		ParserASTBuilderTokenType_human(t->type),
		str_ndup(pool, this->buf + t->i, t->len));
#endif

	if (t->type != PARSER_AST_BUILDER_TOKEN_COMMENT) {
		flush_comments(this, stack_peek(this->nodestack));
	}

	switch (t->type) {
	case PARSER_AST_BUILDER_TOKEN_CONDITIONAL_START:
	case PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_START:
	case PARSER_AST_BUILDER_TOKEN_TARGET_RULE_START:
	case PARSER_AST_BUILDER_TOKEN_VARIABLE_START:
		file_truncate(this->only_whitespace, 0);
		array_truncate(this->tokens);
		break;
	case PARSER_AST_BUILDER_TOKEN_CONDITIONAL_EXPR:
	case PARSER_AST_BUILDER_TOKEN_CONDITIONAL_POSIX:
	case PARSER_AST_BUILDER_TOKEN_CONDITIONAL_TOKEN:
	case PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_FLAGS:
	case PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_TOKEN:
	case PARSER_AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR:
	case PARSER_AST_BUILDER_TOKEN_TARGET_RULE_TOKEN:
	case PARSER_AST_BUILDER_TOKEN_VARIABLE_MODIFIER:
	case PARSER_AST_BUILDER_TOKEN_VARIABLE_TOKEN:
		t->whitespace_before = file_slurp(this->only_whitespace, this->pool, NULL);
		file_truncate(this->only_whitespace, 0);
		array_append(this->tokens, t);
		break;
	case PARSER_AST_BUILDER_TOKEN_CONDITIONAL_END:
	{
		struct ParserASTBuilderToken *start = array_get(this->tokens, 0);
		unless (start) {
			ERROR(PARSER_ERROR_AST_BUILD_FAILED,
				  "conditional at byte %zu has no tokens", t->i);
		}
		unless (
			start->type == PARSER_AST_BUILDER_TOKEN_CONDITIONAL_EXPR ||
			start->type == PARSER_AST_BUILDER_TOKEN_CONDITIONAL_POSIX) {
			ERROR(PARSER_ERROR_AST_BUILD_FAILED,
				  "invalid token of type '%s' at byte %zu",
				  ParserASTBuilderTokenType_human(start->type),
				  t->i);
		}

		const char *indent = "";
		if (start->whitespace_before) {
			indent = start->whitespace_before;
		}
		enum ParserASTBuilderConditionalType condtype =
			PARSER_AST_BUILDER_CONDITIONAL_INVALID;
		unless (ParserASTBuilderConditionalType_from_bmake(
					this->buf + start->i,
					start->len,
					&condtype)) {
			condtype = PARSER_AST_BUILDER_CONDITIONAL_INVALID;
		}
		if (start->type == PARSER_AST_BUILDER_TOKEN_CONDITIONAL_POSIX) {
			switch (condtype) {
			case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL:
			case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL:
				condtype = PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL;
				break;
			case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S:
			case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S:
				condtype = PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S;
				break;
			case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE:
			case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX:
				condtype = PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX;
				break;
			default:
				condtype = PARSER_AST_BUILDER_CONDITIONAL_INVALID;
				break;
			}
		}
		switch (condtype) {
		case PARSER_AST_BUILDER_CONDITIONAL_INVALID:
			ERROR(PARSER_ERROR_AST_BUILD_FAILED,
				  "invalid conditional at byte %zu", t->i);
		case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX:
		case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL:
		case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S:
		case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE:
		case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL:
		case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_D:
		case PARSER_AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S:
		{
			enum ASTIncludeType type;
			unless (ParserASTBuilderConditionalType_to_ASTIncludeType(condtype, &type)) {
				ERROR(PARSER_ERROR_AST_BUILD_FAILED,
					  "cannot map %s to ASTIncludeType",
					  ParserASTBuilderConditionalType_tostring(condtype));
			}
			struct AST *node = ast_new_include();
			ast_set_include_type(node, type);
			ast_set_expr_indent(node, indent);
			struct ASTEdit *ast_edit = ast_edit_new(
				stack_peek(this->nodestack),
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			ast_edit_apply(ast_edit);

			struct ASTWordsEdit *edit = ast_words_edit_new(
				node,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				false);
			ast_words_edit_apply(edit);

			// An .include terminates the previous target per bmake behavior
			node = stack_peek(this->nodestack);
			if (node && ast_target_rule_p(node)) {
				stack_pop(this->nodestack);
			}

			break;
		} case PARSER_AST_BUILDER_CONDITIONAL_ERROR:
		case PARSER_AST_BUILDER_CONDITIONAL_EXPORT_ENV:
		case PARSER_AST_BUILDER_CONDITIONAL_EXPORT_ENV_DOT:
		case PARSER_AST_BUILDER_CONDITIONAL_EXPORT_LITERAL:
		case PARSER_AST_BUILDER_CONDITIONAL_EXPORT:
		case PARSER_AST_BUILDER_CONDITIONAL_INFO:
		case PARSER_AST_BUILDER_CONDITIONAL_UNDEF:
		case PARSER_AST_BUILDER_CONDITIONAL_UNEXPORT_ENV:
		case PARSER_AST_BUILDER_CONDITIONAL_UNEXPORT:
		case PARSER_AST_BUILDER_CONDITIONAL_WARNING:
		{
			enum ASTExprType type;
			unless (ParserASTBuilderConditionalType_to_ASTExprType(condtype, &type)) {
				ERROR(PARSER_ERROR_AST_BUILD_FAILED,
					  "cannot map %s to ASTExprType",
					  ParserASTBuilderConditionalType_tostring(condtype));
			}
			struct AST *node = ast_new_expr();
			ast_set_expr_type(node, type);
			ast_set_expr_indent(node, indent);
			struct ASTEdit *ast_edit = ast_edit_new(
				stack_peek(this->nodestack),
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			ast_edit_apply(ast_edit);
			struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				false);
			ast_words_edit_apply(edit);
			break;
		}
		case PARSER_AST_BUILDER_CONDITIONAL_FOR:
		{
			struct AST *node = mempool_add(this->pool, ast_new_for(), ast_free);
			struct AST *for_bindings = ast_get_child(node, AST_CHILD_FOR_BINDINGS);
			struct AST *for_words = ast_get_child(node, AST_CHILD_FOR_WORDS);
			struct AST *for_body = ast_get_child(node, AST_CHILD_FOR_BODY);
			ast_set_expr_indent(node, indent);
			stack_push(this->forstack, for_body);
			size_t word_start = 1;
			struct ASTWordsEdit *bindings_edit = ast_words_edit_new(for_bindings, AST_WORDS_EDIT_DEFAULT);
			ARRAY_FOREACH_SLICE(this->tokens, 1, -1, struct ParserASTBuilderToken *, t) {
				const char *data = this->buf + t->i;
				if (t->len == 2 && str_startswith(data, "in")) {
					word_start = t_index + 1;
					if (t->whitespace_before) {
						size_t len = strlen(t->whitespace_before);
						if (len > 0) {
							struct ASTWord *ws = ast_word_new_whitespace(
								t->whitespace_before,
								t->i - len);
							ast_words_edit_append_word(bindings_edit, ws);
						}
					}
					break;
				} else {
					struct ASTWord *word = parser_astbuilder_parse_word(
						data,
						t->len,
						t->i,
						false);
					if (t->whitespace_before) {
						size_t len = strlen(t->whitespace_before);
						if (len > 0) {
							struct ASTWord *ws = ast_word_new_whitespace(
								t->whitespace_before,
								t->i - len);
							ast_words_edit_append_word(bindings_edit, ws);
						}
					}
					ast_words_edit_append_word(bindings_edit, word);
				}
			}
			ast_words_edit_apply(bindings_edit);
			struct ASTWordsEdit *edit = ast_words_edit_new(for_words, AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				word_start, -1,
				edit,
				0,
				false);
			ast_words_edit_apply(edit);
			stack_push(this->nodestack, for_body);
			break;
		}
		case PARSER_AST_BUILDER_CONDITIONAL_ENDFOR:
		{
			struct AST *node = unwind_until_node(this->nodestack, stack_pop(this->forstack));
			unless (node && ast_for_body_p(node)) {
				ERROR(PARSER_ERROR_AST_BUILD_FAILED,
					  "could not find matching .for for .endfor at byte %zu",
					  t->i);
			}
			struct AST *for_node = ast_parent(node);
			unless (for_node && ast_for_p(for_node)) {
				ERROR(PARSER_ERROR_AST_BUILD_FAILED,
					  "could not find matching .for for .endfor at byte %zu",
					  t->i);
			}
			struct AST *for_end = ast_get_child(for_node, AST_CHILD_FOR_END);
			struct ASTWordsEdit *edit = ast_words_edit_new(
				for_end,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				false);
			ast_words_edit_apply(edit);
			ast_set_expr_indent(for_end, indent);
			struct ASTEdit *ast_edit = ast_edit_new(
				stack_peek(this->nodestack),
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, mempool_forget(this->pool, for_node));
			ast_edit_apply(ast_edit);
			break;
		}
		case PARSER_AST_BUILDER_CONDITIONAL_IF:
		case PARSER_AST_BUILDER_CONDITIONAL_IFDEF:
		case PARSER_AST_BUILDER_CONDITIONAL_IFMAKE:
		case PARSER_AST_BUILDER_CONDITIONAL_IFNDEF:
		case PARSER_AST_BUILDER_CONDITIONAL_IFNMAKE:
		{
			enum ASTIfType type;
			unless (ParserASTBuilderConditionalType_to_ASTIfType(condtype, &type)) {
				ERROR(PARSER_ERROR_AST_BUILD_FAILED,
					  "cannot map %s to ASTIfType",
					  ParserASTBuilderConditionalType_tostring(condtype));
			}

			struct AST *ifnode = mempool_add(this->pool, ast_new_if(), ast_free);
			stack_push(this->ifstack, ifnode);
			stack_push(this->nodestack, ifnode);

			struct AST *node = ast_new_if_branch();
			ast_set_if_branch_type(node, type);
			ast_set_expr_indent(node, indent);
			struct ASTEdit *ast_edit = ast_edit_new(
				ifnode,
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			ast_edit_apply(ast_edit);
			stack_push(this->nodestack, node);

			struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				true);
			ast_words_edit_apply(edit);
			break;
		}
		case PARSER_AST_BUILDER_CONDITIONAL_ELIF:
		case PARSER_AST_BUILDER_CONDITIONAL_ELIFDEF:
		case PARSER_AST_BUILDER_CONDITIONAL_ELIFNDEF:
		case PARSER_AST_BUILDER_CONDITIONAL_ELIFMAKE:
		case PARSER_AST_BUILDER_CONDITIONAL_ELIFNMAKE:
		case PARSER_AST_BUILDER_CONDITIONAL_ELSE:
		{
			struct AST *parent = stack_peek(this->ifstack);
			unless (parent) {
				ERROR(PARSER_ERROR_AST_BUILD_FAILED,
					  "%s without parent",
					  ParserASTBuilderConditionalType_tostring(condtype));
			}
			enum ASTIfType type;
			unless (ParserASTBuilderConditionalType_to_ASTIfType(condtype, &type)) {
				ERROR(PARSER_ERROR_AST_BUILD_FAILED,
					  "cannot map %s to ASTIfType",
					  ParserASTBuilderConditionalType_tostring(condtype));
			}

			struct AST *node = ast_new_if_branch();
			ast_set_if_branch_type(node, type);
			ast_set_expr_indent(node, indent);
			struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				true);
			ast_words_edit_apply(edit);
			stack_push(this->nodestack, node);
			struct ASTEdit *ast_edit = ast_edit_new(
				parent,
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			ast_edit_apply(ast_edit);

			switch (type) {
			case AST_IF_IF:
			case AST_IF_DEF:
			case AST_IF_MAKE:
			case AST_IF_NDEF:
			case AST_IF_NMAKE:
				if (ast_words_no_meta_len(node) == 0) {
					ERROR(PARSER_ERROR_AST_BUILD_FAILED,
						  ".%s with no words in test expression",
						  ASTIfType_human(type));
				}
				break;
			case AST_IF_ELSE:
			{
				if (ast_words_no_meta_len(node) != 0) {
					ERROR(PARSER_ERROR_AST_BUILD_FAILED,
						  ".%s with non-whitespace words in test expression: %s",
						  ASTIfType_human(type),
						  ast_words_no_meta_flatten(node, pool, " "));
				}
				break;
			}
			}
			break;
		}
		case PARSER_AST_BUILDER_CONDITIONAL_ENDIF:
		{
				struct AST *parentif = stack_pop(this->ifstack);
				unless (parentif && ast_if_p(parentif)) {
					ERROR(PARSER_ERROR_AST_BUILD_FAILED,
						  "could not find matching .if for .endif at byte %zu",
						  t->i);
				}
				ast_set_expr_indent(parentif, indent);
				struct ASTWordsEdit *edit = ast_words_edit_new(
					parentif,
					AST_WORDS_EDIT_DEFAULT);
				create_ast_words(
					pool,
					this,
					1, -1,
					edit,
					t->i,
					false);
				ast_words_edit_apply(edit);
				unwind_until_node(this->nodestack, parentif);
				struct ASTEdit *ast_edit = ast_edit_new(
					stack_peek(this->nodestack),
					AST_EDIT_DEFAULT);
				ast_edit_append_child(
					ast_edit,
					mempool_forget(this->pool, parentif));
				ast_edit_apply(ast_edit);
		}
		}
		break;
	}
	case PARSER_AST_BUILDER_TOKEN_TARGET_RULE_END:
	{
		// A new target terminates the previous target
		struct AST *node = stack_peek(this->nodestack);
		if (node && ast_target_rule_p(node)) {
			stack_pop(this->nodestack);
		}

		node = ast_new_target_rule();
		size_t dependencies_start = 0;
		struct ParserASTBuilderToken *operator_token = NULL;
		ARRAY_FOREACH(this->tokens, struct ParserASTBuilderToken *, t) {
			if (t->type == PARSER_AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR) {
				dependencies_start = t_index + 1;
				operator_token = t;
				enum ASTTargetRuleOperator operator;
				if (!ASTTargetRuleOperator_from_bmake(
						this->buf + t->i,
						t->len,
						&operator))
				{
					ERROR(
						PARSER_ERROR_AST_BUILD_FAILED,
						"invalid target rule operator at byte %zu",
						t->i);
				}
				struct ASTEdit *ast_edit = ast_edit_new(node, AST_EDIT_DEFAULT);
				ast_edit_set_target_rule_operator(ast_edit, operator);
				ast_edit_apply(ast_edit);
				break;
			}
		}

		unless (dependencies_start > 0) {
			ERROR(
				PARSER_ERROR_AST_BUILD_FAILED,
				"unable to parse target rule at byte %zu",
				t->i);
		}

		struct ASTWordsEdit *edit = ast_words_edit_new(
			ast_get_child(node, AST_CHILD_TARGET_RULE_TARGETS),
			AST_WORDS_EDIT_DEFAULT);
		// We shouldn't get an eol comment here yet…
		if (create_ast_words(
				pool,
				this,
				0, dependencies_start - 1,
				edit,
				0,
				false))
		{
			ERROR(
				PARSER_ERROR_AST_BUILD_FAILED,
				"unable to parse target rule at byte %zu",
				t->i);
		}

		if (strlen(operator_token->whitespace_before) > 0) {
			struct ASTWord *word = ast_word_new_whitespace(
				operator_token->whitespace_before,
				operator_token->i - strlen(operator_token->whitespace_before));
			ast_words_edit_append_word(edit, word);
		}

		ast_words_edit_apply(edit);

		edit = ast_words_edit_new(
			ast_get_child(node, AST_CHILD_TARGET_RULE_DEPENDENCIES),
			AST_WORDS_EDIT_DEFAULT);
		create_ast_words(
			pool,
			this,
			dependencies_start, -1,
			edit,
			0,
			false);
		flush_trailing_whitespace(
			this,
			edit,
			t->i);
		ast_words_edit_apply(edit);

		// Attach the target
		struct AST *parent = stack_peek(this->nodestack);
		struct ASTEdit *ast_edit = ast_edit_new(
			parent,
			AST_EDIT_DEFAULT);
		ast_edit_append_child(
			ast_edit,
			mempool_forget(this->pool, node));
		ast_edit_apply(ast_edit);
		stack_push(this->nodestack, ast_get_child(node, AST_CHILD_TARGET_RULE_BODY));
		break;
	}
	case PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_END:
	{
		struct AST *node = ast_new_target_command();
		struct ASTEdit *ast_edit = ast_edit_new(
			stack_peek(this->nodestack),
			AST_EDIT_DEFAULT);
		ast_edit_append_child(ast_edit, node);
		ast_edit_apply(ast_edit);
		struct ParserASTBuilderToken *flags = array_get(this->tokens, 0);
		size_t start = 0;
		if (flags && flags->type == PARSER_AST_BUILDER_TOKEN_TARGET_COMMAND_FLAGS) {
			start = 1;
			const char *target_command_flags = str_ndup(
				pool,
				this->buf + flags->i,
				flags->len);
			ast_set_target_command_flags(
				node,
				target_command_flags);
		}
		struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
		create_ast_words(
			pool,
			this,
			start, -1,
			edit,
			0,
			false);
		flush_trailing_whitespace(
			this,
			edit,
			t->i);
		ast_words_edit_apply(edit);
		break;
	}
	case PARSER_AST_BUILDER_TOKEN_VARIABLE_END: {
		unless (array_len(this->tokens) > 0) {
			ERROR(PARSER_ERROR_AST_BUILD_FAILED, "variable has no tokens");
		}
		struct ParserASTBuilderToken *varstart = array_get(this->tokens, 0);
		struct ParserASTBuilderToken *modifier_token = array_get(this->tokens, 1);
		if (!varstart ||
			!modifier_token ||
			modifier_token->type != PARSER_AST_BUILDER_TOKEN_VARIABLE_MODIFIER) {
			ERROR(
				PARSER_ERROR_AST_BUILD_FAILED,
				"unable to parse variable at byte %zu",
				t->i);
		}

		enum ASTVariableModifier modifier;
		unless(
			ASTVariableModifier_from_human(
				this->buf + modifier_token->i,
				modifier_token->len,
				&modifier)) {
			ERROR(
				PARSER_ERROR_AST_BUILD_FAILED,
				"unknown variable modifier at byte %zu",
				modifier_token->i);
		}

		struct AST *node = ast_new_variable();
		{
			struct ASTEdit *ast_edit = ast_edit_new(node, AST_EDIT_DEFAULT);
			ast_edit_set_variable_modifier(ast_edit, modifier);
			ast_edit_apply(ast_edit);
		}

		struct ASTWordsEdit *name_edit = ast_words_edit_new(
			ast_get_child(node, AST_CHILD_VARIABLE_NAME),
			AST_WORDS_EDIT_DEFAULT);
		if (strlen(varstart->whitespace_before) > 0) {
			struct ASTWord *word = ast_word_new_whitespace(
				varstart->whitespace_before,
				varstart->i - strlen(varstart->whitespace_before));
			ast_words_edit_append_word(name_edit, word);
		}
		{
			struct ASTWord *word = parser_astbuilder_parse_word(
				this->buf + varstart->i,
				varstart->len,
				varstart->i,
				false);
			ast_words_edit_append_word(name_edit, word);
		}
		if (strlen(modifier_token->whitespace_before) > 0) {
			struct ASTWord *word = ast_word_new_whitespace(
				modifier_token->whitespace_before,
				modifier_token->i - strlen(modifier_token->whitespace_before));
			ast_words_edit_append_word(name_edit, word);
		}
		ast_words_edit_apply(name_edit);

		struct ASTEdit *ast_edit = ast_edit_new(
			stack_peek(this->nodestack),
			AST_EDIT_DEFAULT);
		ast_edit_append_child(
			ast_edit,
			node);
		ast_edit_apply(ast_edit);
		struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
		create_ast_words(
			pool,
			this,
			2, -1,
			edit,
			varstart->i + varstart->len,
			false);
		flush_trailing_whitespace(
			this,
			edit,
			t->i);
		ast_words_edit_apply(edit);

		// A variable terminates the previous target per bmake behavior
		node = stack_peek(this->nodestack);
		if (node && ast_target_rule_p(node)) {
			stack_pop(this->nodestack);
		}

		break;
	} case PARSER_AST_BUILDER_TOKEN_COMMENT:
		array_append(this->comments, t);
		break;
	case PARSER_AST_BUILDER_TOKEN_WS:
		file_write(this->only_whitespace, this->buf + t->i, 1, t->len);
		break;
	}

	return true;

error:
	ast_free(this->root);
	this->root = NULL;
	return false;
}

struct AST *
parser_astbuilder_finish(struct ParserASTBuilder *this)
{
	SCOPE_MEMPOOL(pool);
	panic_if(this->finished, "AST was already built");

	if (parser_astbuilder_tokenize_buffer(this, pool, this->buf, this->buflen)) {
		flush_comments(this, stack_peek(this->nodestack));

		if (stack_len(this->ifstack) > 0) {
			ERROR(PARSER_ERROR_AST_BUILD_FAILED,
				  "node stack not exhausted: missing .endif?");
		} else if (stack_len(this->forstack) > 0) {
			ERROR(PARSER_ERROR_AST_BUILD_FAILED,
				  "node stack not exhausted: missing .endfor?");
		} else {
			this->finished = true;
			struct AST *root = this->root;
			this->root = NULL;
			return root;
		}
	} else {
		//parser_set_error(this->parser, PARSER_ERROR_UNSPECIFIED, NULL);
		return NULL;
	}

error:
	return NULL;
}

#undef ERROR
