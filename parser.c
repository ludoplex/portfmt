// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include <libias/array.h>
#include <libias/color.h>
#include <libias/diff.h>
#include <libias/diffutil.h>
#include <libias/flow.h>
#include <libias/io.h>
#include <libias/map.h>
#include <libias/mem.h>
#include <libias/mempool.h>
#include <libias/path.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/compare.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/word.h"
#include "capsicum_helpers.h"
#include "constants.h"
#include "parser.h"
#include "parser/astbuilder.h"
#include "parser/edits.h"
#include "rules.h"

struct Parser {
	struct ParserSettings settings;
	enum ParserError error;
	char *error_msg;

	struct File *inbuf;
	struct Array *rawlines;

	struct Mempool *pool;
	struct AST *ast;
	struct Rules *rules;
	struct File *result;
	struct Mempool *metadata_pool;
	void *metadata[PARSER_METADATA_USES + 1];
	bool metadata_valid[PARSER_METADATA_USES + 1];

	bool read_finished;
};

struct ParserFindGoalcolsState {
	struct Rules *rules;
	uint32_t moving_goalcol;
	struct Array *nodes;
};

struct ParserLoadIncludesState {
	struct Parser *parser;
	struct Directory *portsdir;
};

struct ParserLookupTargetState {
	const char *name;
	struct AST *retval;
};

struct ParserLookupVariableState {
	const char *name;
	enum ParserLookupVariableBehavior behavior;
	struct Array *nodes;
};

// Prototypes
static void parser_init_format_settings(struct Parser *parser, struct ASTFormatSettings *settings);
static enum ParserError parser_load_includes(struct Parser *parser);
static bool parser_load_includes_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void parser_load_includes_visit_include(struct ASTVisit *visit, struct AST *node);
static void parser_lookup_target_visit_target_rule_targets(struct ASTVisit *visit, struct AST *node);
static bool parser_lookup_variable_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void parser_lookup_variable_visit_variable(struct ASTVisit *visit, struct AST *node);
static void parser_meta_values(struct Parser *parser, const char *var, struct Set *set);
static void parser_meta_values_helper(struct Parser *parser, struct Set *set, const char *var, char *value);
static void parser_metadata_alloc(struct Parser *parser);
static void parser_metadata_port_options(struct Parser *parser);
static void parser_output_diff(struct Parser *parser);
static void parser_output_dump_tokens(struct Parser *parser);
static void parser_output_prepare(struct Parser *parser);
static void parser_port_options_add_from_group(struct Parser *parser, const char *groupname);
static void parser_port_options_add_from_var(struct Parser *parser, const char *var);
static const char *process_include(struct Parser *parser, struct Mempool *extpool, const char *curdir, char *filename);

void
parser_init_settings(struct ParserSettings *settings)
{
	settings->filename = NULL;
	settings->portsdir = NULL;
	settings->behavior = PARSER_DEFAULT;
	settings->diff_context = 3;
	settings->if_wrapcol = 80;
	settings->for_wrapcol = 80;
	settings->target_command_format_threshold = 8;
	settings->target_command_format_wrapcol = 80;
	settings->variable_wrapcol = 80;
	settings->debug_level = 0;
}

struct Parser *
parser_new(struct Mempool *extpool, struct ParserSettings *settings)
{
	struct Parser *parser = xmalloc(sizeof(struct Parser));

	parser->pool = mempool_new();
	parser->metadata_pool = mempool_new();
	parser->inbuf = file_open_memstream(NULL);
	parser->rawlines = array_new();
	parser->result = file_open_memstream(NULL);
	parser_metadata_alloc(parser);
	parser->error = PARSER_ERROR_OK;
	parser->error_msg = NULL;
	parser->settings = *settings;
	if (settings->filename) {
		parser->settings.filename = path_normalize(parser->pool, settings->filename, NULL);
	} else {
		parser->settings.filename = str_dup(parser->pool, "/dev/stdin");
	}

	if (parser->settings.behavior & PARSER_OUTPUT_EDITED) {
		parser->settings.behavior &= ~PARSER_COLLAPSE_ADJACENT_VARIABLES;
	}

	if ((settings->behavior & PARSER_OUTPUT_DUMP_TOKENS) ||
	    (settings->behavior & PARSER_OUTPUT_DIFF) ||
	    (settings->behavior & PARSER_OUTPUT_RAWLINES)) {
		settings->behavior &= ~PARSER_OUTPUT_INPLACE;
	}

	parser->rules = rules_new(parser);

	return mempool_add(extpool, parser, parser_free);
}

void
parser_free(struct Parser *parser)
{
	if (parser == NULL) {
		return;
	}

	file_free(parser->result);

	ARRAY_FOREACH(parser->rawlines, char *, line) {
		free(line);
	}
	array_free(parser->rawlines);

	file_free(parser->inbuf);
	mempool_free(parser->pool);
	mempool_free(parser->metadata_pool);
	free(parser->error_msg);

	ast_free(parser->ast);
	rules_free(parser->rules);
	free(parser);
}

void
parser_set_error(struct Parser *parser, enum ParserError error, const char *msg)
{
	free(parser->error_msg);
	if (msg) {
		parser->error_msg = str_dup(NULL, msg);
	} else {
		parser->error_msg = NULL;
	}
	parser->error = error;
}

char *
parser_error_tostring(struct Parser *parser, struct Mempool *extpool)
{
	if (parser->error_msg) {
		return str_printf(extpool, "%s: %s", ParserError_human(parser->error), parser->error_msg);
	} else {
		return str_printf(extpool, "%s", ParserError_human(parser->error));
	}
}

void
parser_enqueue_output(struct Parser *parser, const char *s)
{
	panic_unless(s, "parser_enqueue_output() is not NULL-safe");
	file_puts(parser->result, s);
}

void
parser_output_prepare(struct Parser *parser)
{
	if (!parser->read_finished) {
		parser_read_finish(parser);
	}

	if (parser->error != PARSER_ERROR_OK) {
		return;
	}

	if (parser->settings.behavior & PARSER_OUTPUT_DUMP_TOKENS) {
		parser_output_dump_tokens(parser);
	} else if (parser->settings.behavior & PARSER_OUTPUT_RAWLINES) {
		/* no-op */
	} else if (parser->settings.behavior & PARSER_OUTPUT_EDITED) {
		// Always format category makefiles fully since they don't
		// follow the normal formattings rules, i.e., the edits applied
		// the wrong formatting before this point and we "correct" that
		// here as a hack.
		if (ast_format_category_makefile_p(parser->ast)) {
			// TODO: Can we use a custom Rules class instead?
			struct ASTFormatSettings settings;
			parser_init_format_settings(parser, &settings);
			ast_format(parser->ast, parser->rules, &settings);
		}
		ast_render(parser->ast, parser->result);
	} else if (parser->settings.behavior & PARSER_OUTPUT_REFORMAT) {
		struct ASTFormatSettings settings;
		parser_init_format_settings(parser, &settings);
		ast_format(parser->ast, parser->rules, &settings);
		ast_render(parser->ast, parser->result);
	}

	if (parser->settings.behavior & PARSER_OUTPUT_DIFF) {
		parser_output_diff(parser);
	}
}

void
parser_init_format_settings(
	struct Parser *parser,
	struct ASTFormatSettings *settings)
{
	ast_format_init_settings(settings);
	if (parser->settings.behavior & PARSER_UNSORTED_VARIABLES) {
		settings->flags |= AST_FORMAT_UNSORTED_VARIABLES;
	}
	if (parser->settings.behavior & PARSER_FORMAT_TARGET_COMMANDS) {
		settings->flags |= AST_FORMAT_TARGET_COMMANDS;
	}
	settings->if_wrapcol = parser->settings.if_wrapcol;
	settings->for_wrapcol = parser->settings.for_wrapcol;
	settings->target_command_format_threshold = parser->settings.target_command_format_threshold;
	settings->target_command_wrapcol = parser->settings.target_command_format_wrapcol;
	settings->target_rule_wrapcol = parser->settings.for_wrapcol; // XXX
	settings->variable_wrapcol = parser->settings.variable_wrapcol;
}

void
parser_output_diff(struct Parser *parser)
{
	SCOPE_MEMPOOL(pool);

	if (parser->error != PARSER_ERROR_OK) {
		return;
	}

	// Normalize result: one element = one line like parser->rawlines
	struct Array *lines = str_split(pool, file_slurp(parser->result, pool, NULL), "\n");
	struct diff *p = array_diff(parser->rawlines, lines, pool, str_compare);
	if (p == NULL) {
		parser_set_error(parser, PARSER_ERROR_UNSPECIFIED,
				 str_printf(pool, "could not create diff"));
		return;
	}

	file_truncate(parser->result, 0);

	if (p->editdist > 0) {
		const char *filename = parser->settings.filename;
		if (filename == NULL) {
			filename = "Makefile";
		}
		const char *color_add = ANSI_COLOR_GREEN;
		const char *color_delete = ANSI_COLOR_RED;
		const char *color_reset = ANSI_COLOR_RESET;
		bool nocolor = parser->settings.behavior & PARSER_OUTPUT_NO_COLOR;
		if (nocolor) {
			color_add = "";
			color_delete = "";
			color_reset = "";
		}
		file_printf(
			parser->result,
			"%s--- %s\n%s+++ %s%s\n",
			color_delete, filename,
			color_add, filename,
			color_reset);
		char *buf = diff_to_patch(
			p, NULL, NULL, NULL, parser->settings.diff_context, !nocolor);
		file_puts(parser->result, buf);
		free(buf);
		parser_set_error(parser, PARSER_ERROR_DIFFERENCES_FOUND, NULL);
	}
}

void
parser_output_dump_tokens(struct Parser *parser)
{
	SCOPE_MEMPOOL(pool);

	if (parser->error != PARSER_ERROR_OK) {
		return;
	}

	if (parser->settings.debug_level == 2) {
		struct ASTFormatSettings settings;
		ast_format_init_settings(&settings);
		if (parser->settings.behavior & PARSER_UNSORTED_VARIABLES) {
			settings.flags |= AST_FORMAT_UNSORTED_VARIABLES;
		}
		if (parser->settings.behavior & PARSER_FORMAT_TARGET_COMMANDS) {
			settings.flags |= AST_FORMAT_TARGET_COMMANDS;
		}
		settings.if_wrapcol = parser->settings.if_wrapcol;
		settings.for_wrapcol = parser->settings.for_wrapcol;
		settings.target_command_format_threshold = parser->settings.target_command_format_threshold;
		settings.target_command_wrapcol = parser->settings.target_command_format_wrapcol;
		settings.target_rule_wrapcol = parser->settings.for_wrapcol; // XXX
		settings.variable_wrapcol = parser->settings.variable_wrapcol;
		ast_format(parser->ast, parser->rules, &settings);
		bool color = !(parser->settings.behavior & PARSER_OUTPUT_NO_COLOR);
		ast_sexp(parser->ast, parser->result, color);
	} else if (parser->settings.debug_level >= 4) {
		struct File *f = file_open_memstream(pool);
		ast_render(parser->ast, f);
		parser_enqueue_output(parser, file_slurp(f, pool, NULL));
	} else if (parser->settings.debug_level >= 1) {
		struct File *f = file_open_memstream(pool);
		panic_unless(f, "file_open_memstream: %s", strerror(errno));
		bool color = !(parser->settings.behavior & PARSER_OUTPUT_NO_COLOR);
		ast_sexp(parser->ast, f, color);
		parser_enqueue_output(parser, file_slurp(f, pool, NULL));
	}
}

enum ParserError
parser_read_from_file(struct Parser *parser, struct File *file)
{
	SCOPE_MEMPOOL(pool);

	if (parser->error != PARSER_ERROR_OK) {
		return parser->error;
	}

	size_t buflen;
	char *buf = file_slurp(file, pool, &buflen);
	unless (buf) {
		parser_set_error(parser, PARSER_ERROR_IO, "file_write");
		return parser->error;
	}

	return parser_read_from_buffer(parser, buf, buflen);
}

enum ParserError
parser_read_finish(struct Parser *parser)
{
	SCOPE_MEMPOOL(pool);

	panic_if(parser->read_finished, "parser_read_finish() called multiple times");

	if (parser->error != PARSER_ERROR_OK) {
		return parser->error;
	}

	for (size_t i = 0; i <= PARSER_METADATA_USES; i++) {
		parser->metadata_valid[i] = false;
	}

	parser->read_finished = true;
	ast_free(parser->ast);

	size_t buflen;
	char *buf = file_slurp(parser->inbuf, pool, &buflen);
	file_free(parser->inbuf);
	parser->inbuf = NULL;
	struct ParserASTBuilder *builder = parser_astbuilder_new(
		parser,
		buf,
		buflen);
	mempool_add(pool, builder, parser_astbuilder_free);
	if (parser->error != PARSER_ERROR_OK) {
		return parser->error;
	}
	parser->ast = parser_astbuilder_finish(builder);
	if (parser->error != PARSER_ERROR_OK) {
		return parser->error;
	} else unless (parser->ast) {
		parser_set_error(parser, PARSER_ERROR_UNSPECIFIED, NULL);
		return parser->error;
	}

	ARRAY_FOREACH(parser->rawlines, char *, line) {
		free(line);
	}
	array_truncate(parser->rawlines);
	ARRAY_FOREACH(str_nsplit(pool, buf, buflen, "\n"), const char *, line) {
		array_append(parser->rawlines, str_dup(NULL, line));
	}

	if (parser->settings.behavior & PARSER_LOAD_LOCAL_INCLUDES &&
	    PARSER_ERROR_OK != parser_load_includes(parser)) {
		return parser->error;
	}

	if ((parser->settings.behavior & PARSER_OUTPUT_DUMP_TOKENS) &&
	    parser->settings.debug_level > 2) {
		return parser->error;
	}

	if (parser->settings.behavior & PARSER_SANITIZE_COMMENTS &&
	    PARSER_ERROR_OK != parser_edit(parser, NULL, refactor_sanitize_comments, NULL)) {
		return parser->error;
	}

	if (parser->settings.behavior & PARSER_SANITIZE_CMAKE_ARGS &&
	    PARSER_ERROR_OK != parser_edit(parser, NULL, refactor_sanitize_cmake_args, NULL)) {
		return parser->error;
	}

	if ((parser->settings.behavior & PARSER_COLLAPSE_ADJACENT_VARIABLES) &&
	    PARSER_ERROR_OK != parser_edit(parser, NULL, refactor_collapse_adjacent_variables, NULL)) {
		return parser->error;
	}

	if (parser->settings.behavior & PARSER_SANITIZE_APPEND &&
	    PARSER_ERROR_OK != parser_edit(parser, NULL, refactor_sanitize_append_modifier, NULL)) {
		return parser->error;
	}

	if (parser->settings.behavior & PARSER_DEDUP_TOKENS &&
	    PARSER_ERROR_OK != parser_edit(parser, NULL, refactor_dedup_tokens, NULL)) {
		return parser->error;
	}

	if (PARSER_ERROR_OK != parser_edit(parser, NULL, refactor_remove_consecutive_empty_lines, NULL)) {
		return parser->error;
	}

	return parser->error;
}

struct AST *
parser_ast(struct Parser *parser)
{
	panic_unless(parser->read_finished, "parser_ast() called before parser_read_finish()");
	if (parser->error == PARSER_ERROR_OK) {
		return parser->ast;
	} else {
		return NULL;
	}
}

enum ParserError
parser_output_write_to_file(struct Parser *parser, struct File *file)
{
	SCOPE_MEMPOOL(pool);

	parser_output_prepare(parser);
	if (file == NULL ||
	    (parser->error != PARSER_ERROR_OK &&
	     parser->error != PARSER_ERROR_DIFFERENCES_FOUND)) {
		return parser->error;
	}

	if (parser->settings.behavior & PARSER_OUTPUT_INPLACE) {
		if (file_seek(file, 0, SEEK_SET) < 0) {
			parser_set_error(parser, PARSER_ERROR_IO,
					 str_printf(pool, "lseek: %s", strerror(errno)));
			return parser->error;
		}
		if (file_truncate(file, 0) < 0) {
			parser_set_error(parser, PARSER_ERROR_IO,
					 str_printf(pool, "ftruncate: %s", strerror(errno)));
			return parser->error;
		}
	}

	size_t buflen = 0;
	char *buf = file_slurp(parser->result, pool, &buflen);
	if (file_write(file, buf, buflen, 1) < 0) {
		parser_set_error(
			parser,
			PARSER_ERROR_IO,
			str_printf(pool, "fputs: %s", strerror(errno)));
		return parser->error;
	}
	file_truncate(parser->result, 0);

	return parser->error;
}

enum ParserError
parser_read_from_buffer(struct Parser *parser, const char *input, size_t len)
{
	if (parser->error != PARSER_ERROR_OK) {
		return parser->error;
	}

	if (file_write(parser->inbuf, input, len, 1) == -1) {
		parser_set_error(parser, PARSER_ERROR_IO, "file_write");
	}

	return parser->error;
}

const char *
process_include(
	struct Parser *parser,
	struct Mempool *extpool,
	const char *curdir,
	char *filename)
{
	SCOPE_MEMPOOL(pool);

	if (*filename == '"') {
		filename++;
	}
	if (str_endswith(filename, "\"")) {
		filename[strlen(filename) - 1] = 0;
	}

	if (str_startswith(filename, "${MASTERDIR}/")) {
		filename += strlen("${MASTERDIR}/");
		const char *masterdir = parser_metadata(parser, PARSER_METADATA_MASTERDIR);
		unless (masterdir) {
			masterdir = ".";
		}
		filename = str_printf(pool, "%s/%s", masterdir, filename);
	}

	if (strstr(filename, "${PORTNAME}")) {
		const char *portname = parser_metadata(parser, PARSER_METADATA_PORTNAME);
		if (portname) {
			// XXX: implement a str_replace()
			filename = str_join(pool, str_split(pool, filename, "${PORTNAME}"), portname);
		}
	}

	struct Array *path = mempool_array(pool);
	if (str_startswith(filename, "${.PARSEDIR}/")) {
		array_append(path, curdir);
		array_append(path, filename + strlen("${.PARSEDIR}/"));
	} else if (str_startswith(filename, "${.CURDIR}/")) {
		array_append(path, curdir);
		array_append(path, filename + strlen("${.CURDIR}/"));
	} else if (str_startswith(filename, "${.CURDIR:H}/")) {
		array_append(path, curdir);
		array_append(path, "..");
		array_append(path, filename + strlen("${.CURDIR:H}/"));
	} else if (str_startswith(filename, "${.CURDIR:H:H}/")) {
		array_append(path, curdir);
		array_append(path, "..");
		array_append(path, "..");
		array_append(path, filename + strlen("${.CURDIR:H:H}/"));
	} else if (str_startswith(filename, "${PORTSDIR}/")) {
		array_append(path, filename + strlen("${PORTSDIR}/"));
	} else if (str_startswith(filename, "${FILESDIR}/")) {
		array_append(path, curdir);
		array_append(path, "files");
		array_append(path, filename + strlen("${FILESDIR}/"));
	} else {
		array_append(path, curdir);
		array_append(path, filename);
	}

	return path_join(extpool, path);
}

bool
parser_load_includes_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	switch (ast_type(node)) {
	case AST_FOR:
	case AST_FOR_BINDINGS:
	case AST_FOR_WORDS:
	case AST_FOR_BODY:
	case AST_FOR_END:
	case AST_IF:
	case AST_IF_BRANCH:
		// Shorten the walk; we only care about top level includes
		return false;
	default:
		return true;
	}
}

void
parser_load_includes_visit_include(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ParserLoadIncludesState *this = ast_visit_context(visit);;
	SCOPE_MEMPOOL(pool);

	if (
		ast_include_type(node) == AST_INCLUDE_BMAKE
		&& !ast_include_loaded_p(node)
		&& !ast_include_system_p(node)
		)
	{
		struct Array *components = path_split(pool, this->parser->settings.filename);
		if (array_len(components) > 0) {
			array_truncate_at(components, array_len(components) - 1);
		}
		const char *curdir = path_join(pool, components);
		char *filename = ast_word_flatten(pool, ast_include_path(node));
		const char *path = process_include(
			this->parser,
			pool,
			curdir,
			filename);
		unless (path) {
			parser_set_error(
				this->parser,
				PARSER_ERROR_IO,
				str_printf(pool, "cannot open include: %s", filename));
			ast_visit_stop(visit);
			return;
		}
		struct File *f = file_openat(pool, this->portsdir, path, O_RDONLY, 0);
		if (f == NULL) {
			parser_set_error(
				this->parser,
				PARSER_ERROR_IO,
				str_printf(pool, "cannot open include: %s: %s", path, strerror(errno)));
			ast_visit_stop(visit);
			return;
		}
#if HAVE_CAPSICUM
		panic_if(caph_limit_stream(file_fd(f), CAPH_READ) < 0, "caph_limit_stream");
#endif
		struct ParserSettings settings = this->parser->settings;
		settings.behavior &= ~PARSER_LOAD_LOCAL_INCLUDES;
		settings.filename = path;
		struct Parser *incparser = parser_new(pool, &settings);
		if (PARSER_ERROR_OK != parser_read_from_file(incparser, f)) {
			parser_set_error(
				this->parser,
				PARSER_ERROR_IO,
				str_printf(pool, "cannot open include: %s: %s", path, strerror(errno)));
			ast_visit_stop(visit);
			return;
		}
		if (PARSER_ERROR_OK != parser_read_finish(incparser)) {
			parser_set_error(
				this->parser,
				PARSER_ERROR_IO,
				parser_error_tostring(incparser, pool));
			ast_visit_stop(visit);
			return;
		}

		struct AST *incroot = incparser->ast;
		panic_unless(ast_root_p(incroot), "incroot != AST_ROOT");
		struct ASTEdit *ast_edit = ast_edit_new(
			node,
			AST_EDIT_DEFAULT);
		AST_CHILDREN_FOREACH(incroot, child) {
			ast_edit_append_child(ast_edit, ast_clone(child));
		}
		ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);
		ast_set_include_loaded(node, true);
	}
}

enum ParserError
parser_load_includes(struct Parser *parser)
{
	SCOPE_MEMPOOL(pool);
	panic_unless(parser->read_finished, "parser_load_includes() called before parser_read_finish()");

	if (parser->error != PARSER_ERROR_OK) {
		return parser->error;
	}

	unless (parser->settings.portsdir) {
		parser_set_error(parser, PARSER_ERROR_IO, str_printf(pool, "invalid portsdir"));
		return parser->error;
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_children_p = parser_load_includes_visit_children_p;
	visit_trait.visit_pre_include = parser_load_includes_visit_include;

	struct ParserLoadIncludesState state = {
		.parser = parser,
		.portsdir = parser->settings.portsdir,
	};

	struct ASTVisit *visit = ast_visit_new(parser->ast, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);
	ast_visit_free(visit);

	return parser->error;
}

enum ParserError
parser_edit(struct Parser *parser, struct Mempool *extpool, ParserEditFn f, void *userdata)
{
	SCOPE_MEMPOOL(pool);
	panic_unless(parser->read_finished, "parser_edit() called before parser_read_finish()");

	if (parser->error != PARSER_ERROR_OK) {
		return parser->error;
	}

	struct ASTFormatSettings format_settings;
	parser_init_format_settings(parser, &format_settings);
	f(parser, parser->rules, &format_settings, parser->ast, extpool, userdata);
	if (parser->error != PARSER_ERROR_OK) {
		parser_set_error(parser, PARSER_ERROR_EDIT_FAILED, parser_error_tostring(parser, pool));
	}

	return parser->error;
}

struct ParserSettings parser_settings(struct Parser *parser)
{
	return parser->settings;
}

void
parser_meta_values_helper(struct Parser *parser, struct Set *set, const char *var, char *value)
{
	if (strcmp(var, "USES") == 0) {
		char *buf = strchr(value, ':');
		if (buf != NULL) {
			char *val = str_ndup(NULL, value, buf - value);
			if (set_contains(set, val)) {
				free(val);
			} else {
				set_add(set, mempool_take(parser->metadata_pool, val));
			}
			return;
		}
	}

	if (!set_contains(set, value)) {
		set_add(set, str_dup(parser->metadata_pool, value));
	}
}

void
parser_meta_values(struct Parser *parser, const char *var, struct Set *set)
{
	SCOPE_MEMPOOL(pool);

	struct Array *nodes = parser_lookup_variables(parser, var, PARSER_LOOKUP_DEFAULT, pool);
	if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
		AST_WORDS_FOREACH(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			parser_meta_values_helper(parser, set, var, ast_word_flatten(pool, word));
		}
	}

	struct Set *options = parser_metadata(parser, PARSER_METADATA_OPTIONS);
	SET_FOREACH(options, const char *, opt) {
		char *buf = str_printf(pool, "%s_VARS", opt);
		nodes = parser_lookup_variables(parser, buf, PARSER_LOOKUP_DEFAULT, pool);
		if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
			AST_WORDS_FOREACH(node, word) {
				if (ast_word_meta_p(word)) {
					continue;
				}
				char *value = ast_word_flatten(pool, word);
				char *buf = str_printf(pool, "%s+=", var);
				if (str_startswith(value, buf)) {
					value += strlen(buf);
				} else {
					buf = str_printf(pool, "%s=", var);
					if (str_startswith(value, buf)) {
						value += strlen(buf);
					} else {
						continue;
					}
				}
				parser_meta_values_helper(parser, set, var, value);
			}
		}

		buf = str_printf(pool, "%s_VARS_OFF", opt);
		nodes = parser_lookup_variables(parser, buf, PARSER_LOOKUP_DEFAULT, pool);
		if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
			AST_WORDS_FOREACH(node, word) {
				if (ast_word_meta_p(word)) {
					continue;
				}
				char *value = ast_word_flatten(pool, word);
				char *buf = str_printf(pool, "%s+=", var);
				if (str_startswith(value, buf)) {
					value += strlen(buf);
				} else {
					buf = str_printf(pool, "%s=", var);
					if (str_startswith(value, buf)) {
						value += strlen(buf);
					} else {
						continue;
					}
				}
				parser_meta_values_helper(parser, set, var, value);
			}
		}

#if PORTFMT_SUBPACKAGES
		if (strcmp(var, "USES") == 0 || strcmp(var, "SUBPACKAGES") == 0) {
#else
		if (strcmp(var, "USES") == 0) {
#endif
			buf = str_printf(pool, "%s_%s", opt, var);
			nodes = parser_lookup_variables(parser, buf, PARSER_LOOKUP_DEFAULT, pool);
			if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
				AST_WORDS_FOREACH(node, word) {
					if (ast_word_meta_p(word)) {
						continue;
					}
					parser_meta_values_helper(parser, set, var, ast_word_flatten(pool, word));
				}
			}

			buf = str_printf(pool, "%s_%s_OFF", opt, var);
			nodes = parser_lookup_variables(parser, buf, PARSER_LOOKUP_DEFAULT, pool);
			if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
				AST_WORDS_FOREACH(node, word) {
					if (ast_word_meta_p(word)) {
						continue;
					}
					parser_meta_values_helper(parser, set, var, ast_word_flatten(pool, word));
				}
			}
		}
	}
}

void
parser_port_options_add_from_group(struct Parser *parser, const char *groupname)
{
	SCOPE_MEMPOOL(pool);

	struct Array *nodes = parser_lookup_variables(parser, groupname, PARSER_LOOKUP_DEFAULT, pool);
	if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
		AST_WORDS_FOREACH(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			const char *optgroupname = ast_word_flatten(pool, word);
			if (!set_contains(parser->metadata[PARSER_METADATA_OPTION_GROUPS], optgroupname)) {
				set_add(parser->metadata[PARSER_METADATA_OPTION_GROUPS], str_dup(parser->metadata_pool, optgroupname));
			}
			char *optgroupvar = str_printf(pool, "%s_%s", groupname, optgroupname);
			struct Array *nodes = parser_lookup_variables(parser, optgroupvar, PARSER_LOOKUP_DEFAULT, pool);
			if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
				AST_WORDS_FOREACH(node, word) {
					if (ast_word_meta_p(word)) {
						continue;
					}
					char *opt = ast_word_flatten(pool, word);
					if (!set_contains(parser->metadata[PARSER_METADATA_OPTIONS], opt)) {
						set_add(parser->metadata[PARSER_METADATA_OPTIONS], str_dup(parser->metadata_pool, opt));
					}
				}
			}
		}
	}
}

void
parser_port_options_add_from_var(struct Parser *parser, const char *var)
{
	SCOPE_MEMPOOL(pool);

	struct Array *nodes = parser_lookup_variables(parser, var, PARSER_LOOKUP_DEFAULT, pool);
	if (nodes) ARRAY_FOREACH(nodes, struct AST *, node) {
		AST_WORDS_FOREACH(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			char *opt = ast_word_flatten(pool, word);
			if (!set_contains(parser->metadata[PARSER_METADATA_OPTIONS], opt)) {
				set_add(parser->metadata[PARSER_METADATA_OPTIONS], str_dup(parser->metadata_pool, opt));
			}
		}
	}
}

void
parser_metadata_port_options(struct Parser *parser)
{
	SCOPE_MEMPOOL(pool);

	if (parser->metadata_valid[PARSER_METADATA_OPTIONS]) {
		return;
	}

	parser->metadata_valid[PARSER_METADATA_OPTION_DESCRIPTIONS] = true;
	parser->metadata_valid[PARSER_METADATA_OPTION_GROUPS] = true;
	parser->metadata_valid[PARSER_METADATA_OPTIONS] = true;

#define FOR_EACH_ARCH(f, var) \
	for (size_t i = 0; i < known_architectures_len; i++) { \
		char *buf = str_printf(pool, "%s_%s", var, known_architectures[i]); \
		f(parser, buf); \
	}

	parser_port_options_add_from_var(parser, "OPTIONS_DEFINE");
	FOR_EACH_ARCH(parser_port_options_add_from_var, "OPTIONS_DEFINE");

	parser_port_options_add_from_group(parser, "OPTIONS_GROUP");
	FOR_EACH_ARCH(parser_port_options_add_from_group, "OPTIONS_GROUP");

	parser_port_options_add_from_group(parser, "OPTIONS_MULTI");
	FOR_EACH_ARCH(parser_port_options_add_from_group, "OPTIONS_MULTI");

	parser_port_options_add_from_group(parser, "OPTIONS_RADIO");
	FOR_EACH_ARCH(parser_port_options_add_from_group, "OPTIONS_RADIO");

	parser_port_options_add_from_group(parser, "OPTIONS_SINGLE");
	FOR_EACH_ARCH(parser_port_options_add_from_group, "OPTIONS_SINGLE");

#undef FOR_EACH_ARCH

	struct Set *opts[] = { parser->metadata[PARSER_METADATA_OPTIONS], parser->metadata[PARSER_METADATA_OPTION_GROUPS] };
	for (size_t i = 0; i < nitems(opts); i++) {
		if (opts[i]) SET_FOREACH(opts[i], const char *, opt) {
			char *var = str_printf(pool, "%s_DESC", opt);
			if (!map_contains(parser->metadata[PARSER_METADATA_OPTION_DESCRIPTIONS], var)) {
				struct AST *node = parser_lookup_variable(parser, var, PARSER_LOOKUP_FIRST);
				if (node) {
					char *desc = ast_words_no_meta_flatten(node, parser->metadata_pool, " ");
					map_add(parser->metadata[PARSER_METADATA_OPTION_DESCRIPTIONS], str_dup(parser->metadata_pool, var), desc);
				}
			}
		}
	}
}

void
parser_metadata_alloc(struct Parser *parser)
{
	for (enum ParserMetadata meta = 0; meta <= PARSER_METADATA_USES; meta++) {
		switch (meta) {
		case PARSER_METADATA_OPTION_DESCRIPTIONS:
			parser->metadata[meta] = mempool_map(parser->metadata_pool, str_compare);
			break;
		case PARSER_METADATA_MASTERDIR:
		case PARSER_METADATA_PORTNAME:
			parser->metadata[meta] = NULL;
			break;
		default:
			parser->metadata[meta] = mempool_set(parser->metadata_pool, str_compare);
			break;
		}
	}
}

void *
parser_metadata(struct Parser *parser, enum ParserMetadata meta)
{
	if (!parser->metadata_valid[meta]) {
		switch (meta) {
		case PARSER_METADATA_CABAL_EXECUTABLES: {
			struct Set *uses = parser_metadata(parser, PARSER_METADATA_USES);
			if (set_contains(uses, "cabal")) {
				parser_meta_values(parser, "CABAL_EXECUTABLES", parser->metadata[PARSER_METADATA_CABAL_EXECUTABLES]);
				if (set_len(parser->metadata[PARSER_METADATA_CABAL_EXECUTABLES]) == 0) {
					struct AST *node = parser_lookup_variable(parser, "PORTNAME", PARSER_LOOKUP_FIRST);
					if (node) {
						char *portname = ast_words_no_meta_flatten(node, parser->metadata_pool, " ");
						if (!set_contains(parser->metadata[PARSER_METADATA_CABAL_EXECUTABLES], portname)) {
							set_add(parser->metadata[PARSER_METADATA_CABAL_EXECUTABLES], portname);
						}
					}
				}
			}
			break;
		} case PARSER_METADATA_FLAVORS: {
			parser_meta_values(parser, "FLAVORS", parser->metadata[PARSER_METADATA_FLAVORS]);
			struct Set *uses = parser_metadata(parser, PARSER_METADATA_USES);
			// XXX: Does not take into account USE_PYTHON=noflavors etc.
			for (size_t i = 0; i < static_flavors_len; i++) {
				if (set_contains(uses, (void*)static_flavors[i].uses) &&
				    !set_contains(parser->metadata[PARSER_METADATA_FLAVORS], (void*)static_flavors[i].flavor)) {
					set_add(parser->metadata[PARSER_METADATA_FLAVORS], str_dup(parser->metadata_pool, static_flavors[i].flavor));
				}
			}
			break;
		} case PARSER_METADATA_LICENSES:
			parser_meta_values(parser, "LICENSE", parser->metadata[PARSER_METADATA_LICENSES]);
			break;
		case PARSER_METADATA_MASTERDIR: {
			struct AST *node = parser_lookup_variable(parser, "MASTERDIR", PARSER_LOOKUP_FIRST | PARSER_LOOKUP_IGNORE_VARIABLES_IN_CONDITIIONALS);
			if (node) {
				if (parser->metadata[meta]) {
					mempool_release(parser->metadata_pool, parser->metadata[meta]);
				}
				parser->metadata[meta] = ast_words_no_meta_flatten(node, parser->metadata_pool, " ");
			}
			break;
		} case PARSER_METADATA_PORTNAME: {
			struct AST *node = parser_lookup_variable(parser, "PORTNAME", PARSER_LOOKUP_FIRST | PARSER_LOOKUP_IGNORE_VARIABLES_IN_CONDITIIONALS);
			if (node) {
				if (parser->metadata[meta]) {
					mempool_release(parser->metadata_pool, parser->metadata[meta]);
				}
				parser->metadata[meta] = ast_words_no_meta_flatten(node, parser->metadata_pool, " ");
			}
			break;
		} case PARSER_METADATA_SHEBANG_LANGS:
			parser_meta_values(parser, "SHEBANG_LANG", parser->metadata[PARSER_METADATA_SHEBANG_LANGS]);
			break;
		case PARSER_METADATA_OPTION_DESCRIPTIONS:
		case PARSER_METADATA_OPTION_GROUPS:
		case PARSER_METADATA_OPTIONS:
			parser_metadata_port_options(parser);
			break;
		case PARSER_METADATA_POST_PLIST_TARGETS:
			parser_meta_values(parser, "POST_PLIST", parser->metadata[PARSER_METADATA_POST_PLIST_TARGETS]);
			break;
#if PORTFMT_SUBPACKAGES
		case PARSER_METADATA_SUBPACKAGES:
			if (!set_contains(parser->metadata[PARSER_METADATA_SUBPACKAGES], "main")) {
				// There is always a main subpackage
				set_add(parser->metadata[PARSER_METADATA_SUBPACKAGES], str_dup(parser->metadata_pool, "main"));
			}
			parser_meta_values(parser, "SUBPACKAGES", parser->metadata[PARSER_METADATA_SUBPACKAGES]);
			break;
#endif
		case PARSER_METADATA_USES:
			parser_meta_values(parser, "USES", parser->metadata[PARSER_METADATA_USES]);
			break;
		}
		parser->metadata_valid[meta] = true;
	}

	return parser->metadata[meta];
}

void
parser_lookup_target_visit_target_rule_targets(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ParserLookupTargetState *this = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);
	AST_WORDS_FOREACH(node, src) {
		if (ast_word_meta_p(src)) {
			continue;
		}
		if (strcmp(ast_word_flatten(pool, src), this->name) == 0) {
			this->retval = node;
			ast_visit_stop(visit);
			return;
		}
	}
}

struct AST *
parser_lookup_target(struct Parser *parser, const char *name)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_target_rule_targets = parser_lookup_target_visit_target_rule_targets;

	struct ParserLookupTargetState state = {
		.name = name,
		.retval = NULL,
	};

	struct ASTVisit *visit = ast_visit_new(parser->ast, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);

	return state.retval;
}

void
parser_lookup_variable_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ParserLookupVariableState *this = ast_visit_context(visit);
	if (ast_word_strcmp(ast_variable_name(node), this->name) == 0) {
		array_append(this->nodes, node);
		if (this->behavior & PARSER_LOOKUP_FIRST) {
			ast_visit_stop(visit);
			return;
		}
	}
}

bool
parser_lookup_variable_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ParserLookupVariableState *this = ast_visit_context(visit);
	switch (ast_type(node)) {
	case AST_FOR:
	case AST_IF:
	case AST_INCLUDE:
		if (this->behavior & PARSER_LOOKUP_IGNORE_VARIABLES_IN_CONDITIIONALS) {
			return false;
		}
		break;
	default:
		break;
	}

	return true;
}

struct AST *
parser_lookup_variable(struct Parser *parser, const char *name, enum ParserLookupVariableBehavior behavior)
{
	SCOPE_MEMPOOL(pool);
	struct Array *nodes = parser_lookup_variables(parser, name, behavior, pool);
	if (nodes) {
		return array_get(nodes, 0);
	} else {
		return NULL;
	}
}

struct Array *
parser_lookup_variables(
	struct Parser *parser,
	const char *name,
	enum ParserLookupVariableBehavior behavior,
	struct Mempool *extpool)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_children_p = parser_lookup_variable_visit_children_p;
	visit_trait.visit_variable = parser_lookup_variable_visit_variable;

	struct ParserLookupVariableState state = {
		.nodes = array_new(),
		.behavior = behavior,
		.name = name,
	};

	struct ASTVisit *visit = ast_visit_new(parser->ast, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);
	ast_visit_free(visit);

	if (array_len(state.nodes) > 0) {
		return mempool_add(extpool, state.nodes, array_free);
	} else {
		array_free(state.nodes);
		return NULL;
	}
}

enum ParserError
parser_merge(struct Parser *parser, struct Parser *subparser, enum ParserMergeBehavior settings)
{
	SCOPE_MEMPOOL(pool);
	struct ParserEdit params = { subparser, NULL, settings };
	enum ParserError error = parser_edit(parser, NULL, edit_merge, &params);

	if (error == PARSER_ERROR_OK &&
	    parser->settings.behavior & PARSER_DEDUP_TOKENS) {
		error = parser_edit(parser, pool, refactor_dedup_tokens, NULL);
	}

	if (error == PARSER_ERROR_OK) {
		error = parser_edit(parser, pool, refactor_remove_consecutive_empty_lines, NULL);
	}

	return error;
}
