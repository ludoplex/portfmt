// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/io.h>
#include <libias/list.h>
#include <libias/mem.h>
#include <libias/mempool.h>
#include <libias/stack.h>
#include <libias/str.h>
#include <libias/trait/compare.h>

#include "ast/word.h"
#include "ast/word/expand/modifier.h"
#include "sexp.h"

struct ASTWord {
	const enum ASTWordType type;

	struct Mempool *pool;

	bool edited;
	bool expand_single;
	bool expand_parens;

	// Byte position in original file
	size_t pos;

	union {
		enum ASTWordBoolType if_type;
		char *value;
		struct List *words; // [struct ASTWord *]
	};

	struct List *modifiers; // [struct ASTWordExpandModifier *]

	// Cache for ast_word_flatten(). It's called often, so it's worth it
	// to cache the return value. This must be reset to NULL if the word
	// is edited. Use ast_word_mark_edited() for that.
	char *_flat_cache;

	struct ASTWord *(*clone)(const struct ASTWord *);
	void (*free)(struct ASTWord *);
	void (*render)(struct ASTWord *, struct File *);
	void (*sexp)(struct ASTWord *, struct SexpWriter *);
};

#define MAKE_AST_WORD(type, fun) \
	struct ASTWord *this = xmalloc(sizeof(struct ASTWord)); \
	this->clone = ast_word_clone_##fun; \
	this->free = ast_word_free_##fun; \
	this->render = ast_word_render_##fun; \
	this->sexp = ast_word_sexp_##fun; \
	this->pool = mempool_new(); \
	this->pos = pos; \
	SET_AST_WORD_TYPE(this, type);

#define SET_AST_WORD_TYPE(word, ast_word_type) \
	do { \
		enum ASTWordType type = (ast_word_type); \
		memcpy((void *)&(word)->type, &type, sizeof(enum ASTWordType)); \
	} while (false);

// Prototypes
static DECLARE_COMPARE(compare_ast_word);
static struct ASTWord *ast_word_clone_append(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_bool(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_comment(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_expand(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_string(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_whitespace(const struct ASTWord *word);
static void ast_word_free_append(struct ASTWord *word);
static void ast_word_free_bool(struct ASTWord *word);
static void ast_word_free_comment(struct ASTWord *word);
static void ast_word_free_expand(struct ASTWord *word);
static void ast_word_free_string(struct ASTWord *word);
static void ast_word_free_whitespace(struct ASTWord *word);
static void ast_word_mark_edited(struct ASTWord *word);
static void ast_word_render_append(struct ASTWord *word, struct File *f);
static void ast_word_render_bool(struct ASTWord *word, struct File *f);
static void ast_word_render_comment(struct ASTWord *word, struct File *f);
static void ast_word_render_expand(struct ASTWord *word, struct File *f);
static void ast_word_render_string(struct ASTWord *word, struct File *f);
static void ast_word_render_whitespace(struct ASTWord *word, struct File *f);
static void ast_word_sexp_append(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_bool(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_comment(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_expand(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_string(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_whitespace(struct ASTWord *word, struct SexpWriter *w);

// Constants
struct CompareTrait *ast_word_compare = &(struct CompareTrait){
	.compare = compare_ast_word,
	.compare_userdata = NULL,
};

struct ASTWord *
ast_word_new_append(const size_t pos)
{
	MAKE_AST_WORD(AST_WORD_APPEND, append);
	this->words = list_new();
	return this;
}

void
ast_word_free_append(struct ASTWord *word)
{
	LIST_FOREACH(word->words, struct ASTWord *, word) {
		ast_word_free(word);
	}
	list_free(word->words);
}

struct ASTWord *
ast_word_new_expand(const size_t pos)
{
	MAKE_AST_WORD(AST_WORD_EXPAND, expand);
	this->words = list_new();
	this->modifiers = list_new();
	return this;
}

struct ASTWord *
ast_word_new_expand_parentheses(const size_t pos)
{
	struct ASTWord *this = ast_word_new_expand(pos);
	this->expand_parens = true;
	return this;
}

struct ASTWord *
ast_word_new_expand_single(const size_t pos)
{
	struct ASTWord *this = ast_word_new_expand(pos);
	this->expand_single = true;
	return this;
}

void
ast_word_free_expand(struct ASTWord *word)
{
	LIST_FOREACH(word->words, struct ASTWord *, word) {
		ast_word_free(word);
	}
	list_free(word->words);
	LIST_FOREACH(word->modifiers, struct ASTWordExpandModifier *, modifier) {
		ast_word_expand_modifier_free(modifier);
	}
	list_free(word->modifiers);
}

struct ASTWord *
ast_word_new_bool(const enum ASTWordBoolType type, const size_t pos)
{
	MAKE_AST_WORD(AST_WORD_BOOL, bool);
	this->if_type = type;
	return this;
}

void
ast_word_free_bool(struct ASTWord *word)
{
}

struct ASTWord *
ast_word_new_string(
	const char *value,
	const size_t pos)
{
	MAKE_AST_WORD(AST_WORD_STRING, string);
	this->value = str_dup(NULL, value);
	return this;
}

void
ast_word_free_string(struct ASTWord *word)
{
	free(word->value);
}

struct ASTWord *
ast_word_new_whitespace(
	const char *value,
	const size_t pos)
{
	MAKE_AST_WORD(AST_WORD_WHITESPACE, whitespace);
	this->value = str_dup(NULL, value);
	return this;
}

void
ast_word_free_whitespace(struct ASTWord *word)
{
	free(word->value);
}

struct ASTWord *
ast_word_new_comment(const char *value, const size_t pos)
{
	MAKE_AST_WORD(AST_WORD_COMMENT, comment);
	this->value = str_dup(NULL, value);
	return this;
}

void
ast_word_free_comment(struct ASTWord *word)
{
	free(word->value);
}

void
ast_word_free(struct ASTWord *word)
{
	if (word) {
		word->free(word);
		free(word->_flat_cache);
		mempool_free(word->pool);
		free(word);
	}
}

struct ASTWord *
ast_word_clone_append(const struct ASTWord *word)
{
	struct ASTWord *dup = ast_word_new_append(
		ast_word_pos(word));

	LIST_FOREACH(word->words, struct ASTWord *, w) {
		ast_word_add_child(dup, ast_word_clone(w));
	}

	return dup;
}

struct ASTWord *
ast_word_clone_expand(const struct ASTWord *word)
{
	struct ASTWord *dup = ast_word_new_expand(
		ast_word_pos(word));

	LIST_FOREACH(word->words, struct ASTWord *, w) {
		ast_word_add_child(dup, ast_word_clone(w));
	}

	LIST_FOREACH(word->modifiers, struct ASTWordExpandModifier *, modifier) {
		list_append(
			dup->modifiers,
			ast_word_expand_modifier_clone(modifier));
	}

	dup->expand_single = word->expand_single;
	dup->expand_parens = word->expand_parens;

	return dup;
}

struct ASTWord *
ast_word_clone_bool(const struct ASTWord *word)
{
	return ast_word_new_bool(
		word->if_type,
		ast_word_pos(word));
}

struct ASTWord *
ast_word_clone_string(const struct ASTWord *word)
{
	return ast_word_new_string(
		word->value,
		ast_word_pos(word));
}

struct ASTWord *
ast_word_clone_whitespace(const struct ASTWord *word)
{
	return ast_word_new_whitespace(
		word->value,
		ast_word_pos(word));
}

struct ASTWord *
ast_word_clone_comment(const struct ASTWord *word)
{
	return ast_word_new_comment(
		word->value,
		ast_word_pos(word));
}

struct ASTWord *
ast_word_clone(const struct ASTWord *word)
{
	return word->clone(word);
}

void
ast_word_mark_edited(struct ASTWord *word)
{
	if (word->_flat_cache) {
		free(word->_flat_cache);
		word->_flat_cache = NULL;
	}
}

bool
ast_word_bool_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_BOOL;
}

bool
ast_word_string_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_STRING;
}

bool
ast_word_whitespace_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_WHITESPACE;
}

bool
ast_word_comment_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_COMMENT;
}

// Return true if WORD is a meta word, i.e., whitespace or a comment.
bool
ast_word_meta_p(const struct ASTWord *word)
{
	switch (word->type) {
	case AST_WORD_WHITESPACE:
	case AST_WORD_COMMENT:
		return true;
	default:
		return false;
	}
}

size_t
ast_word_pos(const struct ASTWord *word)
{
	return word->pos;
}

enum ASTWordType
ast_word_type(const struct ASTWord *word)
{
	return word->type;
}

void
ast_word_add_child(
	struct ASTWord *parent,
	struct ASTWord *child)
{
	panic_unless(
		parent->type == AST_WORD_APPEND || parent->type == AST_WORD_EXPAND,
		"expected AST_WORD_APPEND or AST_WORD_EXPAND but got %s",
		ASTWordType_tostring(parent->type));
	panic_if(
		parent == child,
		"cannot add parent as child");

	list_append(parent->words, child);
	ast_word_mark_edited(parent);
}

struct Array *
ast_word_children(
	struct ASTWord *word,
	struct Mempool *pool)
{
	struct Array *children = mempool_array(pool);

	switch (word->type) {
	case AST_WORD_EXPAND:
	case AST_WORD_APPEND:
		LIST_FOREACH(word->words, struct ASTWord *, word) {
			array_append(children, word);
		}
		break;
	default:
		break;
	}

	return children;
}

// Returns the number of immediate children of WORD. It does not return
// the number of children's children etc.
size_t
ast_word_children_count(struct ASTWord *word)
{
	switch (word->type) {
	case AST_WORD_EXPAND:
	case AST_WORD_APPEND:
		return list_len(word->words);
	default:
		return 1;
	}
}

// Return the number of expand modifiers in the WORD.
size_t
ast_word_expand_modifiers_count(struct ASTWord *word)
{
	switch (word->type) {
	case AST_WORD_EXPAND:
		return list_len(word->modifiers);
	default:
		return 0;
	}
}

struct Array *
ast_word_expand_modifiers(
	struct ASTWord *word,
	struct Mempool *pool)
{
	struct Array *modifiers = mempool_array(pool);

	if (word->type == AST_WORD_EXPAND) {
		LIST_FOREACH(word->modifiers, struct ASTWordExpandModifier *, modifier) {
			array_append(modifiers, modifier);
		}
	}

	return modifiers;
}

// Add MODIFIER to WORD. This function will panic if WORD is not an
// AST_WORD_EXPAND. WORD takes ownership of MODIFIER.
void
ast_word_add_expand_modifier(
	struct ASTWord *word,
	struct ASTWordExpandModifier *modifier)
{
	panic_unless(
		word->type == AST_WORD_EXPAND,
		"expected AST_WORD_EXPAND but got %s",
		ASTWordType_tostring(word->type));

	list_append(word->modifiers, modifier);
	ast_word_mark_edited(word);
}

// Returns true if WORD contains an AST_WORD_EXPAND, i.e., if its value
// can change depending on its environment when evaluated multiple
// times.
bool
ast_word_expand_p(struct ASTWord *word)
{
	switch (word->type) {
	case AST_WORD_EXPAND:
		return true;
	case AST_WORD_APPEND:
		LIST_FOREACH(word->words, struct ASTWord *, child) {
			if (ast_word_expand_p(child)) {
				return true;
			}
		}
		return false;
	case AST_WORD_BOOL:
	case AST_WORD_WHITESPACE:
	case AST_WORD_STRING:
	case AST_WORD_COMMENT:
		return false;
	}

	return false;
}

// Updates the value of an AST_WORD_COMMENT, AST_WORD_STRING, or
// AST_WORD_WHITESPACE.  This functions will panic if WORD is not of
// these types.
void
ast_word_set_value(struct ASTWord *word, const char *new_value)
{
	panic_unless(
		word->type == AST_WORD_WHITESPACE
		|| word->type == AST_WORD_STRING
		|| word->type == AST_WORD_COMMENT,
		"expected AST_WORD_COMMENT, AST_WORD_STRING, or AST_WORD_WHITESPACE but got %s",
		ASTWordType_tostring(word->type));

	ast_word_mark_edited(word);
	free(word->value);
	word->value = str_dup(NULL, new_value);
}

// Returns the read-only value of an AST_WORD_COMMENT, AST_WORD_STRING,
// or AST_WORD_WHITESPACE.  This functions will panic if WORD is not of
// these types.
//
// ast_word_flatten() will return a string representation of a word of
// any type.
//
// Use ast_word_set_value() to update it.
const char *
ast_word_value(const struct ASTWord *word)
{
	panic_unless(
		word->type == AST_WORD_WHITESPACE
		|| word->type == AST_WORD_STRING
		|| word->type == AST_WORD_COMMENT,
		"expected AST_WORD_COMMENT, AST_WORD_STRING, or AST_WORD_WHITESPACE but got %s",
		ASTWordType_tostring(word->type));

	return word->value;
}

void
ast_word_set_bool_type(
	struct ASTWord *word,
	const enum ASTWordBoolType if_type)
{
	panic_unless(
		word->type == AST_WORD_BOOL,
		"expected AST_WORD_BOOL but got %s",
		ASTWordType_tostring(word->type));

	word->if_type = if_type;
}

enum ASTWordBoolType
ast_word_bool_type(const struct ASTWord *word)
{
	panic_unless(
		word->type == AST_WORD_BOOL,
		"expected AST_WORD_BOOL but got %s",
		ASTWordType_tostring(word->type));

	return word->if_type;
}

// If WORD is an AST_WORD_APPEND with one child word, then return the
// child word and free WORD recursively. This function will always
// return a value.
struct ASTWord *
ast_word_unwrap(struct ASTWord *word)
{
	if (word->type != AST_WORD_APPEND) {
		return word;
	} else if (list_len(word->words) == 1) {
		struct ListEntry *head = list_head(word->words);
		struct ASTWord *retval = ast_word_unwrap(list_entry_value(head));
		list_entry_remove(head);
		ast_word_free(word);
		return retval;
	} else {
		return word;
	}
}

// Wrap WORD inside a new AST_WORD_APPEND. This function will always
// return a value.
struct ASTWord *
ast_word_wrap(struct ASTWord *word)
{
	struct ASTWord *wrapper = ast_word_new_append(
		ast_word_pos(word));
	ast_word_add_child(wrapper, word);
	return wrapper;
}

void
ast_word_render_append(
	struct ASTWord *word,
	struct File *f)
{
	LIST_FOREACH(word->words, struct ASTWord *, w) {
		ast_word_render(w, f);
	}
}

void
ast_word_render_expand(
	struct ASTWord *word,
	struct File *f)
{
	if (word->expand_single) {
		file_puts(f, "$");
	} else if (word->expand_parens) {
		file_puts(f, "$(");
	} else {
		file_puts(f, "${");
	}
	LIST_FOREACH(word->words, struct ASTWord *, w) {
		ast_word_render(w, f);
	}
	LIST_FOREACH(word->modifiers, struct ASTWordExpandModifier *, modifier) {
		ast_word_expand_modifier_render(modifier, f);
	}
	if (word->expand_single) {
	} else if (word->expand_parens) {
		file_puts(f, ")");
	} else {
		file_puts(f, "}");
	}
}

void
ast_word_render_bool(
	struct ASTWord *word,
	struct File *f)
{
	file_puts(f, ASTWordBoolType_bmake(word->if_type));
}

void
ast_word_render_string(
	struct ASTWord *word,
	struct File *f)
{
	file_puts(f, word->value);
}

void
ast_word_render_whitespace(
	struct ASTWord *word,
	struct File *f)
{
	file_puts(f, word->value);
}

void
ast_word_render_comment(
	struct ASTWord *word,
	struct File *f)
{
	file_puts(f, "#");
	file_puts(f, word->value);
}

void
ast_word_render(
	struct ASTWord *word,
	struct File *f)
{
	word->render(word, f);
}

void
ast_word_sexp_append(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	SCOPE_MEMPOOL(pool);
	const char *value = ast_word_flatten(pool, word);
	sexp_writer_open_tree(w, "word-append");
	sexp_writer_int64(w, "offset", ast_word_pos(word), ast_word_pos(word) + strlen(value));
	LIST_FOREACH(word->words, struct ASTWord *, child) {
		ast_word_sexp(child, w);
	}
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_expand(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	SCOPE_MEMPOOL(pool);
	const char *value = ast_word_flatten(pool, word);

	sexp_writer_open_tree(w, "word-expand");
	sexp_writer_int64(w, "offset", ast_word_pos(word), ast_word_pos(word) + strlen(value));
	LIST_FOREACH(word->words, struct ASTWord *, child) {
		ast_word_sexp(child, w);
	}
	if (list_len(word->modifiers) > 0) {
		sexp_writer_open_tree(w, "modifiers");
		LIST_FOREACH(word->modifiers, struct ASTWordExpandModifier *, modifier) {
			ast_word_expand_modifier_sexp(modifier, w);
		}
		sexp_writer_close_tree(w);
	}
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_bool(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-bool");
	const char *value = ASTWordBoolType_bmake(word->if_type);
	sexp_writer_int64(
		w,
		"offset",
		ast_word_pos(word),
		ast_word_pos(word) + strlen(value));
	sexp_writer_string(w, "value", value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_string(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word");
	sexp_writer_int64(
		w,
		"offset",
		ast_word_pos(word),
		ast_word_pos(word) + strlen(word->value));
	sexp_writer_string(w, "value", word->value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_whitespace(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-whitespace");
	sexp_writer_int64(
		w,
		"offset",
		ast_word_pos(word),
		ast_word_pos(word) + strlen(word->value));
	sexp_writer_string(w, "value", word->value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_comment(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-comment");
	sexp_writer_int64(
		w,
		"offset",
		ast_word_pos(word),
		ast_word_pos(word) + strlen(word->value));
	sexp_writer_string(w, "value", word->value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	word->sexp(word, w);
}


char *
ast_word_flatten(struct Mempool *extpool, struct ASTWord *word)
{
	if (word->_flat_cache) {
		return str_dup(extpool, word->_flat_cache);
	}

	SCOPE_MEMPOOL(pool);
	struct File *f = file_open_memstream(pool);
	ast_word_render(word, f);
	char *s = file_slurp(f, extpool, NULL);
	word->_flat_cache = str_dup(NULL, s);
	return s;
}

int
ast_word_strcmp(struct ASTWord *word, const char *s)
{
	SCOPE_MEMPOOL(pool);
	return strcmp(ast_word_flatten(pool, word), s);
}

DEFINE_COMPARE(compare_ast_word, struct ASTWord, void)
{
	SCOPE_MEMPOOL(pool);
	return strcmp(ast_word_flatten(pool, a), ast_word_flatten(pool, b));
}
