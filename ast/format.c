// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/flow.h>
#include <libias/io.h>
#include <libias/map.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/compare.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/word.h"
#include "rules.h"

struct ASTFormat {
	struct ASTFormatSettings settings;
	struct Rules *rules;

	struct Mempool *pool;
	struct Map *goalcols;
};

struct ASTFormatFindGoalcols {
	struct ASTFormat *ast_format;
	struct Array *nodes;
	int32_t moving_goalcol;
};

// Prototypes
static DECLARE_COMPARE(compare_category_makefile_subdir);
static DECLARE_COMPARE(compare_sort_variable_words);
static void ast_format_category_makefile_p_visit_include(struct ASTVisit *visit, struct AST *node);
static void ast_format_comment(struct ASTVisit *visit, struct AST *node);
static void ast_format_expr(struct ASTVisit *visit, struct AST *node);
static int32_t ast_format_expr_helper(struct ASTWordsEdit *edit, int32_t wrapcol, const char *line_cont, int32_t linelen_after_wrap, const char **line_breaks_after, const size_t line_breaks_after_len, struct ASTWord *word, const int32_t linelen);
static void ast_format_find_goalcols(struct ASTFormat *this, struct AST *node);
static void ast_format_find_goalcols_propagate(struct ASTFormatFindGoalcols *this);
static void ast_format_find_goalcols_visit_comment(struct ASTVisit *visit, struct AST *node);
static void ast_format_find_goalcols_visit_variable(struct ASTVisit *visit, struct AST *node);
static void ast_format_for(struct ASTVisit *visit, struct AST *node);
static int32_t ast_format_goalcol(struct ASTFormat *this, struct AST *node);
static void ast_format_if_branch(struct ASTVisit *visit, struct AST *node);
static void ast_format_include(struct ASTVisit *visit, struct AST *node);
static void ast_format_set_goalcol(struct ASTFormat *this, struct AST *node, int32_t goalcol);
static void ast_format_sort_category_makefile(struct AST *root);
static void ast_format_target_command(struct ASTVisit *visit, struct AST *node);
static char *ast_format_target_command_join_words(struct Mempool *extpool, struct Array *words, const char *sep);
static void ast_format_target_rule(struct ASTVisit *visit, struct AST *node);
static void ast_format_variable(struct ASTVisit *visit, struct AST *node);
static void ast_format_variable_as_list(struct ASTFormat *this, struct AST *node, struct ASTWordsEdit *edit);
static void ast_format_variable_in_category_makefile(struct ASTVisit *visit, struct AST *node);
static void ast_format_variable_sort_opt_use(struct ASTFormat *this, struct AST *node, const char *varname_str);
static void ast_format_variable_with_line_breaks(struct ASTFormat *this, struct AST *node, struct ASTWordsEdit *edit);
static bool matches_opt_use_prefix(const char *s);
static bool matches_opt_use_prefix_helper(char c);

// Constants
static const int32_t TAB_WIDTH = 8;
static const char *EXPR_LINE_CONT = " \\\n\t";
static const char *TARGET_COMMAND_LINE_CONT = " \\\n\t\t";
static const char *LINE_CONT = " \\\n";

void
ast_format_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	// comments stay like they are
}

void
ast_format_expr(
	struct ASTVisit *visit,
	struct AST *node)
{
}

int32_t
ast_format_expr_helper(
	struct ASTWordsEdit *edit,
	int32_t wrapcol,
	const char *line_cont,
	int32_t linelen_after_wrap,
	const char **line_breaks_after,
	const size_t line_breaks_after_len,
	struct ASTWord *word,
	const int32_t linelen)
{
	if (ast_word_whitespace_p(word)) {
		return linelen;
	}

	SCOPE_MEMPOOL(pool);

	wrapcol -= strlen(LINE_CONT) - strlen("\n");

	const char *word_str = ast_word_flatten(pool, word);
	const int32_t word_str_len = strlen(word_str);

	if ((linelen + word_str_len) > wrapcol) {
		bool wrap_ok = true;
		for (size_t i = 0; i < line_breaks_after_len; i++) {
			if (strcmp(word_str, line_breaks_after[i]) == 0) {
				wrap_ok = false;
				break;
			}
		}
		if (wrap_ok) {
			struct ASTWord *ws = ast_word_new_whitespace(line_cont, ast_word_pos(word));
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			return linelen_after_wrap + word_str_len;
		}
	}

	struct ASTWord *ws = ast_word_new_whitespace(" ", ast_word_pos(word));
	ast_words_edit_set_whitespace_before_word(edit, word, ws);

	return linelen + strlen(" ") + word_str_len;
}

void
ast_format_for(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);

	// TODO wrong if indent contains a \t
	int32_t linelen = strlen(ast_expr_indent(node)) + strlen(".for");

	struct AST *for_bindings = ast_get_child(node, AST_CHILD_FOR_BINDINGS);
	struct ASTWordsEdit *edit = ast_words_edit_new(for_bindings, AST_WORDS_EDIT_DEFAULT);

	AST_WORDS_FOREACH(for_bindings, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.for_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);
	}

	ast_words_edit_apply(edit);

	linelen += strlen("in");

	struct AST *for_words = ast_get_child(node, AST_CHILD_FOR_WORDS);
	edit = ast_words_edit_new(for_words, AST_WORDS_EDIT_DEFAULT);
	AST_WORDS_FOREACH(for_words, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.for_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);

		if (ast_word_comment_p(word)) {
			// Add a space before the trailing comment
			struct ASTWord *ws = ast_word_new_whitespace(" ", 0);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
		}
	}
	ast_words_edit_remove_trailing_whitespace(edit);
	ast_words_edit_apply(edit);
}

void
ast_format_if_branch(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);

	static const char *line_breaks_after[] = {
		"&&",
		"||",
		"!=",
		"==",
		"<=",
		">=",
		"<",
		">",
	};

	struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);

	enum ASTIfType type = ast_if_branch_type(node);
	// TODO wrong if indent contains a \t
	int32_t linelen = strlen(ASTIfType_human(type)) +
		strlen(ast_expr_indent(node)) +
		strlen(".");
	unless (
		type == AST_IF_ELSE
		|| ast_get_child(ast_parent(node), 0) == node)
	{
		linelen += strlen("el");
	}

	AST_WORDS_FOREACH(node, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.if_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			line_breaks_after, nitems(line_breaks_after),
			word,
			linelen);
	}

	ast_words_edit_remove_trailing_whitespace(edit);

	// Apply the edit we have so far. Afterwards we can fix the
	// whitespace of !, (, )
	ast_words_edit_apply(edit);

	edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
	AST_WORDS_FOREACH(node, word) {
		if (ast_word_comment_p(word)) {
			// Add a space before the trailing comment
			struct ASTWord *ws = ast_word_new_whitespace(" ", 0);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			continue;
		} else unless (ast_word_bool_p(word)) {
			continue;
		}
		switch (ast_word_bool_type(word)) {
		case AST_WORD_BOOL_GROUP_START:
		case AST_WORD_BOOL_NOT:
		{
			struct ASTWord *next = ast_next_word(node, word);
			if (next && ast_word_whitespace_p(next)) {
				const char *next_str = ast_word_flatten(pool, next);
				if (strcmp(next_str, EXPR_LINE_CONT) == 0) {
					ast_words_edit_set_whitespace_before_word(
						edit,
						word,
						ast_word_clone(next));
				}
				ast_words_edit_remove_whitespace_after_word(edit, word);
			}
			break;
		}
		case AST_WORD_BOOL_GROUP_END:
			ast_words_edit_remove_whitespace_before_word(edit, word);
			break;
		default:
			break;
		}
	}

	ast_words_edit_apply(edit);
}

void
ast_format_include(
	struct ASTVisit *visit,
	struct AST *node)
{
}

bool
matches_opt_use_prefix_helper(char c)
{
	return isupper((unsigned char)c) || islower((unsigned char)c) || isdigit((unsigned char)c) || c == '-' || c == '_';
}

bool
matches_opt_use_prefix(const char *s)
{
	// ^([-_[:upper:][:lower:][:digit:]]+)
	if (!matches_opt_use_prefix_helper(*s)) {
		return false;
	}
	size_t len = strlen(s);
	size_t i;
	for (i = 1; i < len && matches_opt_use_prefix_helper(s[i]); i++);

	// \+?
	if (s[i] == '+') {
		i++;
	}

	// =
	if (s[i] == '=') {
		return true;
	}

	return false;
}

void
ast_format_variable_sort_opt_use(
	struct ASTFormat *this,
	struct AST *node,
	const char *varname_str)
{
	SCOPE_MEMPOOL(pool);

	bool opt_use = false;
	char *helper = NULL;
	if (rules_parse_variable_options_helper(
			this->rules,
			varname_str,
			pool,
			NULL,
			&helper,
			NULL))
	{
		if (strcmp(helper, "USE") == 0 || strcmp(helper, "USE_OFF") == 0)  {
			opt_use = true;
		} else if (strcmp(helper, "VARS") == 0 || strcmp(helper, "VARS_OFF") == 0) {
			opt_use = false;
		} else {
			return;
		}
	} else {
		return;
	}

	AST_WORDS_FOREACH(node, word) {
		// Ignore if the (once unwrapped) prefix isn't static
		if (ast_word_string_p(word)) {
			// ok
		} else if (ast_word_children_count(word) > 0) {
			struct Array *children = ast_word_children(word, pool);
			struct ASTWord *first = array_get(children, 0);
			if (first && ast_word_string_p(first)) {
				word = first;
			} else {
				continue;
			}
		} else {
			continue;
		}

		const char *t = ast_word_flatten(pool, word);
		if (!matches_opt_use_prefix(t)) {
			continue;
		}

		char *suffix = strchr(t, '=');
		if (suffix == NULL) {
			continue;
		}
		suffix++;

		char *prefix = str_map(pool, t, suffix - t, toupper);
		enum ASTVariableModifier mod = AST_VARIABLE_MODIFIER_ASSIGN;
		if ((suffix - t) >= 1 && prefix[suffix - t - 1] == '=') {
			prefix[suffix - t - 1] = 0;
		}
		if ((suffix - t) >= 2 && prefix[suffix - t - 2] == '+') {
			mod = AST_VARIABLE_MODIFIER_APPEND;
			prefix[suffix - t - 2] = 0;
		}
		struct File *f = file_open_memstream(pool);
		if (opt_use) {
			char *var = str_printf(pool, "USE_%s", prefix);
			file_puts(f, prefix);
			file_puts(f, ASTVariableModifier_human(mod));
			struct CompareTokensData data = {
				.rules = this->rules,
				.var = var,
			};
			struct Array *values = str_split(pool, suffix, ",");
			array_sort(values, &(struct CompareTrait){rules_compare_tokens, &data});
			ARRAY_FOREACH(values, const char *, t2) {
				file_puts(f, t2);
				if (t2_index < array_len(values) - 1) {
					file_puts(f, ",");
				}
			}
		} else {
			file_puts(f, prefix);
			file_puts(f, ASTVariableModifier_human(mod));
			file_puts(f, suffix);
		}

		ast_word_set_value(word, file_slurp(f, pool, NULL));
	}
}

void
ast_format_variable_as_list(
	struct ASTFormat *this,
	struct AST *node,
	struct ASTWordsEdit *edit)
{
	SCOPE_MEMPOOL(pool);

	const char *varname = ast_word_flatten(pool, ast_variable_name(node));
	int32_t wrapcol = this->settings.variable_wrapcol;
	int32_t goalcol = ast_format_goalcol(this, node);
	if (rules_ignore_wrap_col(this->rules, varname, ast_variable_modifier(node))) {
		wrapcol = 99999999;
	} else {
		wrapcol -= goalcol;
		wrapcol -= strlen(LINE_CONT) - strlen("\n");
	}

	int32_t ntabs;
	int32_t rowlen = strlen(varname);
	if (str_endswith(varname, "+")) {
		rowlen += strlen(" ");
	}
	rowlen += strlen(ASTVariableModifier_human(ast_variable_modifier(node)));
	if (rowlen > MAX(2 * TAB_WIDTH, goalcol)) {
		ntabs = ceil((rowlen - MAX(2 * TAB_WIDTH, goalcol)) / (1.0 * TAB_WIDTH));
	} else {
		ntabs = ceil((MAX(2 * TAB_WIDTH, goalcol) - rowlen) / (1.0 * TAB_WIDTH));
	}
	const char *sep_first = str_repeat(pool, "\t", ntabs);
	rowlen += ntabs * TAB_WIDTH;
	ntabs = ceil(MAX(2 * TAB_WIDTH, goalcol) / (1.0 * TAB_WIDTH));
	const char *sep_next = str_printf(pool, "%s%s", LINE_CONT, str_repeat(pool, "\t", ntabs));

	rowlen = 0;
	bool first = true;
	AST_WORDS_FOREACH(node, word) {
		if (ast_word_whitespace_p(word)) {
			continue;
		}
		const char *word_str = ast_word_flatten(pool, word);
		int32_t word_str_len = strlen(word_str);
		if (first) {
			struct ASTWord *ws = ast_word_new_whitespace(sep_first, ast_word_pos(word));
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			rowlen += word_str_len;
			first = false;
		} else if ((rowlen + word_str_len) > wrapcol) {
			struct ASTWord *ws = ast_word_new_whitespace(sep_next, ast_word_pos(word));
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			rowlen = word_str_len;
		} else {
			rowlen += word_str_len + strlen(" ");
			struct ASTWord *ws = ast_word_new_whitespace(" ", ast_word_pos(word));
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
		}
	}

	ast_words_edit_remove_trailing_whitespace(edit);
}

void
ast_format_variable_with_line_breaks(
	struct ASTFormat *this,
	struct AST *node,
	struct ASTWordsEdit *edit)
{
	SCOPE_MEMPOOL(pool);

	const char *varname = ast_word_flatten(pool, ast_variable_name(node));
	int32_t ntabs;
	int32_t startlen = strlen(varname);
	if (str_endswith(varname, "+")) {
		startlen += strlen(" ");
	}
	startlen += strlen(ASTVariableModifier_human(ast_variable_modifier(node)));
	int32_t goalcol = ast_format_goalcol(this, node);
	if (startlen > MAX(2 * TAB_WIDTH, goalcol)) {
		ntabs = ceil((startlen - MAX(2 * TAB_WIDTH, goalcol)) / (1.0 * TAB_WIDTH));
	} else {
		ntabs = ceil((MAX(2 * TAB_WIDTH, goalcol) - startlen) / (1.0 * TAB_WIDTH));
	}
	const char *sep_first = str_repeat(pool, "\t", ntabs);
	startlen += ntabs * TAB_WIDTH;
	// For the next lines
	ntabs = ceil(MAX(2 * TAB_WIDTH, goalcol) / (1.0 * TAB_WIDTH));
	const char *sep_next = str_printf(pool, " \\\n%s", str_repeat(pool, "\t", ntabs));

	const char *sep = sep_first;
	int32_t non_whitespace_words = 0;
	AST_WORDS_FOREACH(node, word) {
		if (ast_word_whitespace_p(word)) {
			continue;
		} else if (ast_word_comment_p(word)) {
			// Keep the comment on the same line as the last word
			const char *start = " ";
			if (non_whitespace_words == 0) {
				start = sep_first;
			}
			struct ASTWord *ws = ast_word_new_whitespace(start, 0);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			continue;
		}

		struct ASTWord *ws = ast_word_new_whitespace(sep, ast_word_pos(word));
		ast_words_edit_set_whitespace_before_word(edit, word, ws);
		if (sep == sep_first) {
			sep = sep_next;
		}
		non_whitespace_words++;
	}

	ast_words_edit_remove_trailing_whitespace(edit);
}

DEFINE_COMPARE(compare_sort_variable_words, struct ASTWord, struct CompareTokensData)
{
	SCOPE_MEMPOOL(pool);
	const char *as = ast_word_flatten(pool, a);
	const char *bs = ast_word_flatten(pool, b);
	return rules_compare_tokens(&as, &bs, this);
}


void
ast_format_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);

	struct ASTWord *varname = ast_variable_name(node);
	const char *varname_str = ast_word_flatten(pool, varname);

	// Leave variables unformatted that have $\ in them
	AST_WORDS_FOREACH(node, word) {
		unless (ast_word_whitespace_p(word)) {
			continue;
		}
		const char *value = ast_word_flatten(pool, word);
		if (str_startswith(value, "$\\\n")) {
			return;
		}
	}
	if (rules_leave_unformatted_p(this->rules, varname_str)) {
		return;
	}

	if (
		!(this->settings.flags & AST_FORMAT_UNSORTED_VARIABLES)
		&& rules_should_sort_p(this->rules, varname_str, ast_variable_modifier(node)))
	{
		ast_format_variable_sort_opt_use(this, node, varname_str);

		struct CompareTokensData data = {
			.rules = this->rules,
			.var = varname_str,
		};
		struct CompareTrait compare = {
			.compare = compare_sort_variable_words,
			.compare_userdata = &data,
		};
		ast_sort_words(node, &compare);
	}

	struct ASTWordsEdit *edit = ast_words_edit_new(
		ast_get_child(node, AST_CHILD_VARIABLE_NAME),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_edit_remove_beginning_whitespace(edit);
	if (str_endswith(varname_str, "+")) {
		struct ASTWord *ws = ast_word_new_whitespace(" ", 0);
		ast_words_edit_set_whitespace_after_word(edit, varname, ws);
	} else {
		ast_words_edit_remove_trailing_whitespace(edit);
	}
	ast_words_edit_apply(edit);

	edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
	if (rules_print_with_line_breaks_p(this->rules, varname_str)) {
		ast_format_variable_with_line_breaks(this, node, edit);
	} else {
		ast_format_variable_as_list(this, node, edit);
	}

	ast_words_edit_apply(edit);
}

void
ast_format_variable_in_category_makefile(
	struct ASTVisit *visit,
	struct AST *node)
{
	// Category Makefiles have a strict layout and their own formatting
	// rules. We apply those to COMMENT and SUBDIR and format any
	// additional variable/node normally. Checking that there are no
	// additional variables should be left to a lint.

	struct ASTWord *variable_name = ast_variable_name(node);

	if (ast_word_strcmp(variable_name, "COMMENT") == 0) {
		struct ASTEdit *ast_edit = ast_edit_new(node, AST_EDIT_DEFAULT);
		ast_edit_set_variable_modifier(
			ast_edit,
			AST_VARIABLE_MODIFIER_ASSIGN);
		ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);

		// Sanitize whitespace around COMMENT
		struct AST *variable_name_node = ast_get_child(node, AST_CHILD_VARIABLE_NAME);
		struct ASTWordsEdit *edit = ast_words_edit_new(variable_name_node, AST_WORDS_EDIT_DEFAULT);
		AST_WORDS_FOREACH(variable_name_node, word) {
			if (ast_word_whitespace_p(word)) {
				ast_words_edit_remove_word(edit, word);
			} else {
				struct ASTWord *ws = ast_word_new_whitespace("    ", ast_word_pos(word));
				ast_words_edit_set_whitespace_before_word(edit, word, ws);

				ws = ast_word_new_whitespace(" ", ast_word_pos(word));
				ast_words_edit_set_whitespace_after_word(edit, word, ws);
			}
		}
		ast_words_edit_apply(edit);

		edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
		ast_words_edit_remove_beginning_whitespace(edit);
		ast_words_edit_remove_trailing_whitespace(edit);
		AST_WORDS_FOREACH(node, word) {
			unless (ast_word_whitespace_p(word)) {
				struct ASTWord *ws = ast_word_new_whitespace(" ", ast_word_pos(word));
				ast_words_edit_set_whitespace_before_word(edit, word, ws);
			}
		}
		ast_words_edit_apply(edit);
	} else if (ast_word_strcmp(variable_name, "SUBDIR") == 0) {
		// Move every word into their own SUBDIR+= node
		struct AST *sibling = node;
		AST_WORDS_FOREACH(node, word) {
			if (ast_word_whitespace_p(word)) {
				continue;
			}

			struct AST *new_node = ast_new_variable();
			// Hook up the new node
			ast_edit_add_child(ast_visit_root_edit(visit), new_node, sibling);

			// Setup SUBDIR with the right formatting
			{
				struct ASTEdit *ast_edit = ast_edit_new(new_node, AST_EDIT_DEFAULT);
				ast_edit_set_variable_modifier(
					ast_edit,
					AST_VARIABLE_MODIFIER_APPEND);
				ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);

				struct ASTWordsEdit *edit = ast_words_edit_new(
					ast_get_child(new_node, AST_CHILD_VARIABLE_NAME),
					AST_WORDS_EDIT_DEFAULT);

				struct ASTWord *subdir = ast_word_new_string("SUBDIR", 0);
				ast_words_edit_append_word(edit, subdir);

				struct ASTWord *ws = ast_word_new_whitespace("    ", 0);
				ast_words_edit_set_whitespace_before_word(edit, subdir, ws);

				ws = ast_word_new_whitespace(" ", 0);
				ast_words_edit_set_whitespace_after_word(edit, subdir, ws);

				ast_words_edit_apply(edit);
			}

			// Add word
			{
				struct ASTWordsEdit *edit = ast_words_edit_new(
					new_node,
					AST_WORDS_EDIT_DEFAULT);
				ast_words_edit_remove_all_words(edit);
				struct ASTWord *word_clone = ast_word_clone(word);
				ast_words_edit_append_word(edit, word_clone);
				struct ASTWord *ws = ast_word_new_whitespace(" ", 0);
				ast_words_edit_set_whitespace_before_word(
					edit,
					word_clone,
					ws);
				ast_words_edit_apply(edit);
			}

			sibling = new_node;
		}

		// Remove the original node
		ast_edit_remove_child(ast_visit_root_edit(visit), node);
	} else {
		return ast_format_variable(visit, node);
	}
}

char *
ast_format_target_command_join_words(
	struct Mempool *extpool,
	struct Array *words,
	const char *sep)
{
	SCOPE_MEMPOOL(pool);
	struct File *f = file_open_memstream(pool);
	bool after_first = false;
	ARRAY_FOREACH(words, struct ASTWord *, word) {
		if (after_first) {
			file_puts(f, sep);
		}
		file_puts(f, ast_word_flatten(pool, word));
		after_first = true;
	}
	return file_slurp(f, extpool, NULL);
}

void
ast_format_target_command(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);

	unless (this->settings.flags & AST_FORMAT_TARGET_COMMANDS) {
		return;
	}

	// TODO: Sanitize target command flags?

	// Normalize whitespace
	struct ASTWordsEdit *edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);
	AST_WORDS_FOREACH(node, word) {
		struct ASTWord *ws = ast_word_new_whitespace(
			" ",
			ast_word_pos(word));
		ast_words_edit_set_whitespace_after_word(edit, word, ws);
	}
	ast_words_edit_apply(edit);

	edit = ast_words_edit_new(node, AST_WORDS_EDIT_DEFAULT);

	struct Array *commands = mempool_array(pool);
	struct Array *commands_words = mempool_array(pool);
	struct Array *merge = mempool_array(pool);
	const char *command_str = NULL;
	bool wrap_after = false;
	AST_WORDS_FOREACH(node, word) {
		if (ast_word_whitespace_p(word)) {
			continue;
		}
		const char *word_str = ast_word_flatten(pool, word);

		if (command_str == NULL) {
			command_str = word_str;
		}
		if (rules_target_command_should_wrap_p(this->rules, word_str)) {
			command_str = NULL;
		}

		if (command_str &&
		    (strcmp(command_str, "${SED}") == 0 ||
		     strcmp(command_str, "${REINPLACE_CMD}") == 0)) {
			if (strcmp(word_str, "-e") == 0 || strcmp(word_str, "-i") == 0) {
				array_append(merge, word);
				wrap_after = true;
				continue;
			}
		}

		array_append(merge, word);
		array_append(commands, ast_format_target_command_join_words(pool, merge, " "));
		array_append(commands_words, merge);
		merge = mempool_array(pool);
		if (wrap_after) {
			// An empty string is abused as a "wrap line here" marker
			array_append(commands, str_dup(pool, ""));
			array_append(commands_words, mempool_array(pool));
			wrap_after = false;
		}
		array_truncate(merge);
	}
	if (array_len(merge) > 0) {
		array_append(commands, ast_format_target_command_join_words(pool, merge, " "));
		array_append(commands_words, merge);
		if (wrap_after) {
			// An empty string is abused as a "wrap line here" marker
			array_append(commands, str_dup(pool, ""));
			array_append(commands_words, mempool_array(pool));
			wrap_after = false;
		}
	}
	merge = NULL;

	// Find the places we need to wrap to the next line.
	struct Set *wraps = mempool_set(pool, id_compare);
	int32_t complexity = 0;
	size_t command_i = 0;
	int32_t linelen = TAB_WIDTH + strlen(ast_target_command_flags(node));
	ARRAY_FOREACH(commands, const char *, word) {
		if (command_str == NULL) {
			command_str = word;
			command_i = word_index;
		}
		if (rules_target_command_should_wrap_p(this->rules, word)) {
			command_str = NULL;
			command_i = 0;
		}

		for (const char *c = word; *c != 0; c++) {
			switch (*c) {
			case '`':
			case '(':
			case ')':
			case '[':
			case ']':
			case ';':
				complexity++;
				break;
			}
		}

		linelen += TAB_WIDTH + strlen(word);
		if (linelen > this->settings.target_command_wrapcol ||
		    strcmp(word, "") == 0 ||
			rules_target_command_should_wrap_p(this->rules, word) ||
		    (command_str && word_index > command_i &&
			 rules_target_command_wrap_after_each_token_p(this->rules, command_str))) {
			if (word_index + 1 < array_len(commands)) {
				char *next = array_get(commands, word_index + 1);
				if (strcmp(next, "") == 0 ||
					rules_target_command_should_wrap_p(this->rules, next)) {
					continue;
				}
			}
			linelen = 2 * TAB_WIDTH;
			set_add(wraps, (void *)(word_index + 1));
		}
	}

	if (!(this->settings.flags & AST_FORMAT_TARGET_COMMANDS) ||
	    complexity > this->settings.target_command_format_threshold) {
		return;
	}

	bool wrapped = false;
	ARRAY_FOREACH(commands, const char *, command) {
		struct Array *command_words = array_get(commands_words, command_index);
		if (wrapped) {
			struct ASTWord *first = array_get(command_words, 0);
			struct ASTWord *ws = ast_word_new_whitespace(
				TARGET_COMMAND_LINE_CONT,
				ast_word_pos(first));
			ast_words_edit_set_whitespace_before_word(
				edit,
				array_get(command_words, 0),
				ws);
		}
		wrapped = set_contains(wraps, (void *)(command_index + 1));

		if (wrapped) {
			ARRAY_FOREACH(command_words, struct ASTWord *, word) {
				struct ASTWord *ws = ast_word_new_whitespace(
					" ",
					ast_word_pos(word));
				ast_words_edit_set_whitespace_after_word(edit, word, ws);
			}
			struct ASTWord *last = array_get(
				command_words,
				array_len(command_words) - 1);
			if (last) {
				struct ASTWord *ws = ast_word_new_whitespace(
					TARGET_COMMAND_LINE_CONT,
					ast_word_pos(last));
				ast_words_edit_set_whitespace_after_word(edit, last, ws);
			}
		}
	}

	ast_words_edit_remove_beginning_whitespace(edit);
	ast_words_edit_remove_trailing_whitespace(edit);

	// if (*ast_node_comment(node)) {
	// 	// Add a space before the trailing comment
	// 	struct ASTWord *ws = ast_word_new_whitespace(" ", 0);
	// 	ast_words_edit_add_word(edit, ws, NULL);
	// }

	ast_words_edit_apply(edit);
}

void
ast_format_target_rule(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);

	int32_t linelen = 0;

	struct AST *targets = ast_get_child(node, AST_CHILD_TARGET_RULE_TARGETS);
	struct ASTWordsEdit *edit = ast_words_edit_new(targets, AST_WORDS_EDIT_DEFAULT);
	AST_WORDS_FOREACH(targets, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.target_rule_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);
	}
	ast_words_edit_remove_beginning_whitespace(edit);
	ast_words_edit_remove_trailing_whitespace(edit);
	ast_words_edit_apply(edit);

	// Separator
	linelen += strlen(":");

	struct AST *dependencies = ast_get_child(node, AST_CHILD_TARGET_RULE_DEPENDENCIES);
	edit = ast_words_edit_new(dependencies, AST_WORDS_EDIT_DEFAULT);
	AST_WORDS_FOREACH(dependencies, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.target_rule_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);

		if (ast_word_comment_p(word)) {
			// Add a space before the trailing comment
			struct ASTWord *ws = ast_word_new_whitespace(" ", 0);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
		}
	}

	ast_words_edit_remove_trailing_whitespace(edit);

	ast_words_edit_apply(edit);
}

void
ast_format_init_settings(struct ASTFormatSettings *settings)
{
	settings->flags = AST_FORMAT_DEFAULT;
	settings->if_wrapcol = 80;
	settings->for_wrapcol = 80;
	settings->target_command_format_threshold = 8;
	settings->target_command_wrapcol = 80;
	settings->target_rule_wrapcol = 80;
	settings->variable_wrapcol = 80;
}

void
ast_format(
	struct AST *node,
	struct Rules *rules,
	struct ASTFormatSettings *settings)
{
	SCOPE_MEMPOOL(pool);

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);

	visit_trait.visit_pre_comment = ast_format_comment;
	visit_trait.visit_pre_expr = ast_format_expr;
	visit_trait.visit_pre_for = ast_format_for;
	visit_trait.visit_pre_if_branch = ast_format_if_branch;
	visit_trait.visit_pre_include = ast_format_include;
	visit_trait.visit_pre_variable = ast_format_variable;
	visit_trait.visit_pre_target_command = ast_format_target_command;
	visit_trait.visit_pre_target_rule = ast_format_target_rule;

	struct ASTFormat *this = &(struct ASTFormat){
		.rules = rules,

		.pool = pool,
		.goalcols = mempool_map(pool, id_compare),
	};

	ast_format_init_settings(&this->settings);
	if (settings) {
		this->settings = *settings;
	}

	bool is_category_makefile =
		ast_root_p(node)
		&& ast_format_category_makefile_p(node);
	if (is_category_makefile) {
		visit_trait.visit_pre_variable = ast_format_variable_in_category_makefile;
	}

	ast_format_find_goalcols(this, node);

	struct ASTVisit *visit = ast_visit_new(node, &visit_trait);
	ast_visit_set_context(visit, this);
	ast_visit_run(visit);
	ast_visit_free(visit);

	if (is_category_makefile) {
		ast_format_sort_category_makefile(node);
	}
}

void
ast_format_category_makefile_p_visit_include(
	struct ASTVisit *visit,
	struct AST *node)
{
	bool *is_category = ast_visit_context(visit);
	if (
		ast_include_type(node) == AST_INCLUDE_BMAKE
		&& ast_include_system_p(node)
		&& ast_include_path(node)
		)
	{
		SCOPE_MEMPOOL(pool);
		const char *path = ast_word_flatten(
			pool,
			ast_include_path(node));
		if (strcmp(path, "<bsd.port.subdir.mk>") == 0) {
			*is_category = true;
			ast_visit_stop(visit);
			return;
		}
	}
}

bool
ast_format_category_makefile_p(struct AST *node)
{
	bool is_category = false;

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_include = ast_format_category_makefile_p_visit_include;

	struct ASTVisit *visit = ast_visit_new(node, &visit_trait);
	ast_visit_set_context(visit, &is_category);
	ast_visit_run(visit);
	ast_visit_free(visit);

	return is_category;
}

DEFINE_COMPARE(compare_category_makefile_subdir, struct AST, void)
{
	SCOPE_MEMPOOL(pool);
	return strcmp(
		ast_words_flatten(a, pool),
		ast_words_flatten(b, pool));
}

void
ast_format_sort_category_makefile(struct AST *root)
{
	SCOPE_MEMPOOL(pool);

	panic_unless(ast_root_p(root), "expected AST_ROOT");

	ssize_t start_index = -1;
	struct Array *children = mempool_array(pool);
	AST_CHILDREN_FOREACH(root, node) {
		array_append(children, node);
	}
	ssize_t end_index = -1;
	AST_CHILDREN_FOREACH(root, node) {
		if (start_index > -1) {
			if (!ast_variable_p(node) || ast_word_strcmp(ast_variable_name(node), "SUBDIR") != 0) {
				end_index = node_index;
				break;
			}
		} else if (ast_variable_p(node) && ast_word_strcmp(ast_variable_name(node), "SUBDIR") == 0) {
			start_index = node_index;
		}
	}

	struct CompareTrait cmp = {
		.compare = compare_category_makefile_subdir,
		.compare_userdata = NULL,
	};
	array_sort_slice(children, start_index, end_index, &cmp);

	struct ASTEdit *ast_edit = ast_edit_new(
		root,
		AST_EDIT_DEFAULT);
	struct Array *clones = mempool_array(pool);
	ARRAY_FOREACH(children, struct AST *, node) {
		array_append(clones, ast_clone(node));
	}
	ast_edit_remove_all_children(ast_edit);
	ARRAY_FOREACH(clones, struct AST *, node) {
		ast_edit_append_child(ast_edit, node);
	}
	ast_edit_apply(ast_edit);
}

void
ast_format_find_goalcols_propagate(struct ASTFormatFindGoalcols *this)
{
	this->moving_goalcol = MAX(2 * TAB_WIDTH, this->moving_goalcol);
	ARRAY_FOREACH(this->nodes, struct AST *, node) {
		ast_format_set_goalcol(
			this->ast_format,
			node,
			this->moving_goalcol);
	}

	this->moving_goalcol = 0;
	array_truncate(this->nodes);
}

void
ast_format_find_goalcols_visit_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormatFindGoalcols *this = ast_visit_context(visit);

	// Ignore comments in between variables and treat variables
	// after them as part of the same block, i.e., indent them the
	// same way.
	ast_format_find_goalcols_propagate(this);
}

void
ast_format_find_goalcols_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormatFindGoalcols *this = ast_visit_context(visit);
	SCOPE_MEMPOOL(pool);

	if (ast_words_no_meta_len(node) > 0) {
		const char *varname = ast_word_flatten(pool, ast_variable_name(node));
		if (rules_skip_goalcol_p(this->ast_format->rules, varname)) {
			ast_format_set_goalcol(
				this->ast_format,
				node,
				rules_indent_goalcol(varname, ast_variable_modifier(node)));
		} else {
			array_append(this->nodes, node);
			this->moving_goalcol = MAX(
				rules_indent_goalcol(varname, ast_variable_modifier(node)),
				this->moving_goalcol);
			}
	}
}

void
ast_format_find_goalcols(
	struct ASTFormat *this,
	struct AST *node)
{
	struct ASTFormatFindGoalcols state = {
		.ast_format = this,
		.nodes = array_new(),
		.moving_goalcol = 0,
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_comment = ast_format_find_goalcols_visit_comment;
	visit_trait.visit_variable = ast_format_find_goalcols_visit_variable;

	struct ASTVisit *visit = ast_visit_new(node, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);
	ast_visit_free(visit);

	ast_format_find_goalcols_propagate(&state);
	array_free(state.nodes);
}

int32_t
ast_format_goalcol(
	struct ASTFormat *this,
	struct AST *node)
{
	int32_t *goalcol = map_get(this->goalcols, node);
	if (goalcol) {
		return *goalcol;
	} else {
		return 0;
	}
}

void
ast_format_set_goalcol(
	struct ASTFormat *this,
	struct AST *node,
	int32_t goalcol)
{
	int32_t *storage = mempool_alloc(this->pool, sizeof(int32_t));
	*storage = goalcol;
	map_add(this->goalcols, node, storage);
}
