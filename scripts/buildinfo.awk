BEGIN {
	bug_tracker_url = "https://codeberg.org/tobik/portfmt/issues"
	"[ -r \"${SRCDIR}/.git\" ] && git describe --tags 2>/dev/null" | getline version
	if (version) {
		exit
	}
}

/^## / && $2  != "Unreleased" {
	version = "v" substr($2, 2, length($2) - 2)
	exit
}

END {
	buf = "// Generated file. Do not edit.\n"
	buf = buf "\n"
	buf = buf "#include \"config.h\"\n"
	buf = buf "\n"
	buf = buf "#include <stdlib.h>\n"
	buf = buf "\n"
	buf = buf "#include \"buildinfo.h\"\n"
	buf = buf "\n"
	buf = buf "const char *\n"
	buf = buf "portfmt_bug_tracker_url(void)\n"
	buf = buf "{\n"
	buf = buf "\tconst char *bug_tracker_url = getenv(\"__PORTFMT_BUG_TRACKER_URL\");\n"
	buf = buf "\tif (bug_tracker_url) {\n"
	buf = buf "\t\treturn bug_tracker_url;\n"
	buf = buf "\t} else {\n"
	buf = buf "\t\treturn \"" bug_tracker_url "\";\n"
	buf = buf "\t}\n"
	buf = buf "}\n"
	buf = buf "\n"
	buf = buf "const char *\n"
	buf = buf "portfmt_version(void)\n"
	buf = buf "{\n"
	buf = buf "\treturn \"" version "\";\n"
	buf = buf "}\n"
	printf("%s", buf)
}
